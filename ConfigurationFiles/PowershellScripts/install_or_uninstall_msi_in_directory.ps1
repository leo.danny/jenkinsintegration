param( $directory, $command, $featureFlagMode = "" )
# $directory = 'C:\test'

# Get the msi to install from the directory.
$msipackage = Get-ChildItem "$directory\*" -Include *.msi

# Determine full name or path of the package.
$msipackageFullName = $msipackage.FullName
write-host "$command $msipackageFullName"

# Determine installation name
$installationName = $msipackage.Name -ireplace ".msi", ""

# Install or uninstall M-Files package.
$args = @()
$args += "/$command"
$args += $msipackageFullName
$args += "/qn"
$args += "/l*"
$args += "`"$msipackageFullName.$command.log`""
$args += "INSTALLLEVEL=100"
#$args += "INSTALLDIR=`"C:\Program Files\M-Files\$installationName`""
$args
start-process msiexec.exe -Wait -ArgumentList $args

# If feature flag mode given, set the FF mode.
if( $featureFlagMode -ne "" ){
	# Deduce version number from package name.
	if( $installationName -imatch "\d+_\d+_\d+_\d+" ) {
		# Return underscores to dots.
		$version = $Matches[0] -ireplace "_","."

		# Set feature mode.
		Write-Host "Setting feature mode of version $version to $featureFlagMode."
		& $PSScriptRoot/set_execution_mode.ps1 -Version $version -Mode $featureFlagMode
	} else {
		# Report error.
		Write-Host "Failed to set feature mode."
		Write-Host "Could not deduce version from file name '$installationName'."
		exit 1
	}
}

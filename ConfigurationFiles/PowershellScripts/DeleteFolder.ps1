﻿param($location)

# Example parameter value
# $location = "C:\Folder"

try
{
    # Destination directory.
    $locationExists = Test-Path -Path $location

    # Verify whether the directory exists.
    if( $locationExists -eq $true )
    {
        # Remove the directory.
        Remove-Item $location -Force -Recurse

        # Writes the message in console.
        Write-Host "Execution Completed: The folder has been deleted."
    }
    else
    {
        # Writes the message in console.
        Write-Host "Execution Completed: The folder does not exists."
    }

}
catch
{
    # Write the exception in console.
    Write-Host "Execution Failed:" $_.Exception.Message
}

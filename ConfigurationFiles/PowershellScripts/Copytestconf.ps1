﻿param($sourceLocation, $destinationLocation, $Connector)

 # Example parameter value
 #$sourceLocation = "E:\BuildAgent\work\d39f3bee807c0e65\ConnectorTestAutomation\Mfiles.Connectors.TestFramework\Mfiles.Connectors.TestFramework\ConfigurationFiles"
 #$destinationLocation = "E:\BuildAgent\work\d39f3bee807c0e65\ConnectorTestAutomation\Mfiles.Connectors.TestFramework\Mfiles.Connectors.TestFramework\bin\Debug"
 #$Connector = "Network Folder"

try
{
    # Add the partial name of file to source location.
    $sourceLocation= $sourceLocation+"\"+"testconf_" + $Connector + ".json"

    Write-Host $sourceLocation
   
    # Copies the files and overwrite the files if it already exists
    #Copy-Item -Path $testconfFiles -Destination $destinationLocation'\testconf.json' -Force -ErrorAction Stop
    Copy-Item -Path $sourceLocation -Destination $destinationLocation'\testconf.json' -Force -ErrorAction Stop

    # Write the mesasage in console.
    Write-Host "Execution Completed: Copying the configuration files is successfull."
}
catch
{
    # Write the exception in console.
    Write-Host "Execution Failed:" $_.Exception.Message
}




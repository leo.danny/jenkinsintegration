﻿param($sourceLocation, $destinationLocation)

# Example parameter value
# $sourceLocation = "D:\ConfigurationFiles"
# $destinationLocation = "C:\"

try
{
    # Destination directory.
    $destinationExists = Test-Path -Path $destinationLocation"ConfigurationFiles";

    # Verify whether the destinationation directory already exists.
    if( $destinationExists -eq $true )
    {
        # Remove the directory.
        Remove-Item $destinationLocation"ConfigurationFiles" -Force -Recurse
    }

    # Copies the configuraion files and overwrite the files if it already exists.
    Copy-Item -Path $sourceLocation -Destination $destinationLocation -Recurse -Force -ErrorAction Stop

    # Writes the message in console.
    Write-Host "Execution Completed: Copying the configuration files is successfull."
}
catch
{
    # Write the exception in console.
    Write-Host "Execution Failed:" $_.Exception.Message
}




﻿param($sourceLocation, $destinationLocation)

# Example parameter value
# $sourceLocation = "D:\Reports\"
# $destinationLocation = "C:\"

try
{
    # This function moves the performance results from result loaction to the destination loaction.
    function CreateFolderAndMoveFiles
    {
        # Parameters passed.
        param([string]$sourceLocation, [string]$destinationLocation, [string]$fileExtension, [string]$destinationFolder )
        
        # Get the existence of destination directory.
        $destinationExists = Test-Path $destinationLocation$destinationFolder;

        # Creates new directory if the destination does not exists.
        if($destinationExists -eq $false)
        {
             # Creates new directory.
             New-Item -Path $destinationLocation -Name $destinationFolder -ItemType Directory

             # Moves the results from result loaction to the destination loaction.
             Move-Item $sourceLocation"*"$fileExtension -Destination $destinationLocation$destinationFolder -Force -ErrorAction Stop
        }
        else
        {
             # Destination already exists.

             # Moves the results from result loaction to the destination loaction.
             Move-Item $sourceLocation"*"$fileExtension -Destination $destinationLocation$destinationFolder -Force -ErrorAction Stop
        }
    }

    # Moves the chart created for performance results.
    CreateFolderAndMoveFiles $sourceLocation $destinationLocation 'ConnectorPerformance.Png' 'ConnectorPerformanceChart'

    #Moves the performance result created based on connector.
    CreateFolderAndMoveFiles $sourceLocation $destinationLocation 'ConnectorPerformance.xml' 'ConnectorPerformance'

    #Moves the performance result created based on Operations.
    CreateFolderAndMoveFiles $sourceLocation $destinationLocation 'OperationPerformance.xml' 'OperationPerformance'

    # Writes the message in console.
    Write-Host "Execution Completed: Copying the performance results is successfull."
}
catch
{
    # Write the exception in console.
    Write-Host "Execution Failed:" $_.Exception.Message
}




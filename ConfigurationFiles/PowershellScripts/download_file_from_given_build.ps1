﻿param( $builderString, $username, $password, $MFilesReleaseArea, $relativeFilePath, $targetFolder )

# Always fail on error.
$ErrorActionPreference = "Stop"

# Function for waiting the build to be ready. 
function WaitForFile($File) 
{
 Write-Host "Waiting for file $File"
 $attempt = 0
 $maxAttempts = 30 # 30 * 10 seconds = 5 minutes.
 while( ( ! ( Test-Path $File ) ) -AND ( $attempt -lt $maxAttempts ) ) 
 {
    # Sleep
    Start-Sleep -s 10;

    # Next attempt.
    $attempt = $attempt + 1
    
    Write-Host "Testing path $File..."
 }

 # Return true if the file exists.
 Write-Host "Testing path $File..."
 $exists = test-path $File
 Write-Host "$exists"
 return $exists
}

# Parse the build number from builder string.
"Finding the version number..."
$regex = new-object System.Text.RegularExpressions.Regex ( '\d+[.]\d+[.]\d{4}[.]\d+' )
$match = $regex.Match( $builderString )
$buildnumber = $match.Value

"Version number: $buildnumber"

# Copy the file from the build folder in release area to the target folder
New-Item -ItemType directory -Path "$targetFolder" -Force
"net use '$MFilesReleaseArea' '$password' '/user:$username'"
net use "$MFilesReleaseArea" "$password" "/user:$username"
"Net use done."

$sourceFile = "$MFilesReleaseArea\$buildnumber\$relativeFilePath"

# The name of the file to be copied
$filenameToCopy = Split-Path $sourceFile -leaf
$targetPath = "$targetFolder\$filenameToCopy"

$fileExists = WaitForFile( $sourceFile )
if( ! $fileExists )
{
    # File did not exist, try with not perfect.
    $sourceFile = "$MFilesReleaseArea\$buildnumber" + "_NOT_PERFECT\$relativeFilePath"
    $fileExists = WaitForFile( $sourceFile )
    if( ! $fileExists )
    {
        # Could not locate the file.
        exit 2
    }

}
Start-Sleep -s 30; # Wait for the possible copy operation to finish.
"Found $sourceFile. Downloading"
copy-item "$sourceFile" "$targetPath" -Force
net use "$MFilesReleaseArea" /delete 2> $null > $null
﻿using System;
using System.Linq;
using System.IO;
using System.Diagnostics;

namespace MFNUnit.Utils.Logger
{
    /// <summary>
	/// Logger class that provides methods to setup and make use of log file in the project.
	/// </summary>
    public class Logger
    {
        // Field variable of TextWriter class.
        [ThreadStatic]
        private static TextWriter _logWriter;

        // Stores the value of the current method name.
        [ThreadStatic]
        private static String _testName;

        // Stores the value of the current method name.
        [ThreadStatic]
        private static String _logFilePath;

        // Assign the enum value to write the informattion.
        public static LogMode logMode = LogMode.DEBUG;

        // Stores the current time.
        private static readonly DateTime buildStartTime = DateTime.Now;  

        /// <summary>
        /// Set and get the value of _logWriter variable.
        /// </summary>
        public static TextWriter LogWriter
        {
            get
            {
                // return the _logWriter value.
                return _logWriter;
            }
            set
            {
                // set the value _logWriter .
                _logWriter = value;
            }
        }

        /// <summary>
        /// Set and get the value of _name.
        /// </summary>
        public static String Name
        {
            get
            {
                // Return the current test method name.
                return _testName;
            }
            set
            {
                // A new instance of StackTrace class is intiated.
                StackTrace stackTrace = new StackTrace();

                // Gets the method name along with the class.
                String className = stackTrace.GetFrame(1).GetMethod().DeclaringType.ToString();

                // Stores the method name.
                _testName = ( value.Contains( className ) ? "" : className +"." ) + value;

                // Stores the path of the log file.
                _logFilePath = GetFilePath() +".log";
            }
        }



        /// <summary>
        /// Assign the value for LogWriter to add information in log file.
        /// </summary>
        /// <param name="textWriter">Variable of type TextWriter</param>
        public static void SetLogWriter( TextWriter textWriter )
        {
            // Assign the value to write the value in log file.
            LogWriter = textWriter;
        }

        /// <summary>
        /// Add information of object type in log file.
        /// </summary>
        /// <param name="writeInfo">Information of type object.</param>
        public static void Info( Object writeInfo )
        {
            // Add the information in the log file.
            if ( logMode == LogMode.INFO )
            {
                // Mode is INFO.

                // Writes the information into the log file.
                WriteToFile( writeInfo, "INFO" );
            }
        }

        /// <summary>
        /// Add information of exception type in log file.
        /// </summary>
        /// <param name="writeException">Exception.</param>
        public static void Info( Exception writeException )
        {
            // Add the exception in the log file.
            Info( "{0} \nInner Exception:\n{1} ", writeException, writeException.InnerException );
        }

        /// <summary>
        /// Add information of string type in log file.
        /// </summary>
        /// <param name="format">String format.</param>
        /// <param name="objectArguments">Arguments.</param>
        public static void Info(
                String messageFormat, 
                params Object[] objectArguments )
        {
            // Add the information in the log file.
            if ( logMode == LogMode.INFO )
            {
                // Verifies for valid arguements.
                if ( objectArguments.Length == 0 )
                    // Writes the information into the log file.
                    Info( ( Object )messageFormat );  
                else
                    // Writes the information into the log file.
                    WriteToFile( String.Format( messageFormat, objectArguments ), "INFO" );  
            }
        }

        /// <summary>
        /// Add information of object type under Debug in log file.
        /// </summary>
        /// <param name="writeError">Error in object format.</param>
        public static void Debug( Object writeError )
        {
            // Add the information in the log file.
            if ( logMode == LogMode.DEBUG || logMode == LogMode.INFO )
            {
                // Writes the information into the log file.
                WriteToFile( writeError, "DEBUG" );
            }

        }

        /// <summary>
        /// Add information of exception type under Debug in log file.
        /// </summary>
        /// <param name="writeException">Exception occured.</param>
        public static void Debug( Exception writeException )
        {
            // Add the exception in the log file.
            Debug( "{0} \nInner Exception:\n{1} ", writeException, writeException.InnerException );
        }

        /// <summary>
        /// Add information of string type under Debug in log file.
        /// </summary>
        /// <param name="messageFormat">String format.</param>
        /// <param name="objectArguments">Arguments.</param>
        public static void Debug(
                String messageFormat, 
                params Object[] objectArguments)
        {
            // Add the information in the log file.
            if ( logMode == LogMode.DEBUG || logMode == LogMode.INFO )
            {
                // Verifies for valid arguements.
                if (objectArguments.Length == 0 )
                    // Writes the information into the log file.
                    Debug( ( Object )messageFormat );  
                else
                    // Writes the information into the log file.
                    WriteToFile( String.Format( messageFormat, objectArguments), "DEBUG" ); 
            }

        }

        /// <summary>
        /// Add information of object type under Error in log file.
        /// </summary>
        /// <param name="writeError">Error in object format.</param>
        public static void Error( Object writeError )
        {
            // Writes the error information in the log file.
            WriteToFile( writeError, "ERROR" );
        }

        /// <summary>
        /// Add information of exception type under Error in log file.
        /// </summary>
        /// <param name="writeException">Exception</param>
        public static void Error( Exception writeException )
        {
            // Writes the exception in the log file.
            Error( "{0} \nInner Exception:\n{1} ", writeException, writeException.InnerException );
        }

        /// <summary>
        /// Log Debug into the log file
        /// </summary>
        /// <param name="messageFormat">String format</param>
        /// <param name="objectArguments">arguments</param>
        public static void Error(
                String messageFormat, 
                params Object[] objectArguments)
        {
            // Verifies for valid arguements.
            if (objectArguments.Length == 0 )
                // Writes the error into the log file.
                Error( ( Object ) messageFormat );  
            else
                // Writes the error into the log file.
                WriteToFile( String.Format( messageFormat, objectArguments), "ERROR" );  
        }

        /// <summary>
        /// Add the information to the log file.
        /// </summary>
        /// <param name="writeLog">Message to be added in log file.</param>
        /// <param name="logMode">Mode of the message.</param>
        private static void WriteToFile(
                Object writeLog, 
                String logMode )
        {
            // Writes the log file if there is some information.
            if ( LogWriter != null )
                LogWriter.WriteLine( writeLog );

            // Get the path of the log file.
            _logFilePath = _logFilePath ?? GetFilePath() + ".log";

            // Initializes a new instance of the StreamWriter class.
            using ( StreamWriter streamWrite = new StreamWriter( _logFilePath, true ) )
            {
                // Sets the value indicating to flush its buffer. 
                streamWrite.AutoFlush = true;

                // Writes the information in the log file.
                streamWrite.WriteLine( logMode + ": " + writeLog.ToString() );
            }

        }

         /// <summary>
        /// Get the path where TestResult information should be stored as .log file
        /// </summary>
        public static string GetFilePath()
        {
            // Verifies the value in Name.
            if ( Name == null )
                Name = "TestResult";  // Assign value to the Name.

            // Stores the filename of the log.
            String fileName = Name.Split('.').LastOrDefault();

            // Gets an array containing the characters that are not allowed in file names.
            var invalids = Path.GetInvalidFileNameChars().ToList();

            // Adds the elements of an invalids to the end of the ArrayList.
            invalids.AddRange( Path.GetInvalidPathChars() );

            // Generate a new name to save the log file.
            var newName = String.Join( "_", fileName.Split
                    ( invalids.ToArray(), StringSplitOptions.RemoveEmptyEntries ) ).TrimEnd( '.' );

            //Stores the directory to where the file will be saved
            String filePath = Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "TestResults" , 
                    buildStartTime.ToString( "MMMMdd_yyyy_HHmmss" ), Name.Replace( Name, "" ) );

            // Create the directory to place the log file.
            Directory.CreateDirectory( filePath );

            // Combines the log file name with the filepath.
            filePath = Path.Combine( filePath, newName );

            // Returns the filepath.
            return filePath;
        }

        /// <summary>
        /// Gives the current directory where log files are saved.
        /// </summary>
        /// <returns>Return the path of current directory where log file is saved</returns>
        public static string GetCurrentLogDirectory()
        {
            // Return the path of current directory where log file is saved.
            return Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "TestResults",
                    buildStartTime.ToString( "MMMMdd_yyyy_HHmmss" ) );

        }

    }

    /// <summary>
    /// Enumeration for the different information mode.
    /// </summary>
    public enum LogMode
    {
        DEBUG,
        INFO,
        ERROR
    }
}
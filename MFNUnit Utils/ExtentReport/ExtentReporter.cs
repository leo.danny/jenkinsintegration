﻿using System;
using NUnit.Framework.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using Microsoft.Win32;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.IO;

namespace MFNUnit.Utils.Logger
{
	/// <summary>
	/// Report class that provides methods to setup and make use of extent reports in the project.
	/// </summary>
	public class Report
	{
		// Field variable of ExtentReports class.
		public static ExtentReports _extentReport;

		// Initiates a collection for extent report.
		public static Dictionary<int, ExtentTest> ExtentTests = new Dictionary<int, ExtentTest>();

		// Get the path of the project.
		public static string directory = Directory.GetParent(
			NUnit.Framework.TestContext.CurrentContext.TestDirectory ).Parent.FullName;

		[ThreadStatic]
		// Field variable of ExtentTest class.   
		public static ExtentTest _testName;

		public static ExtentTest childTest;

		// Instance of the class LogInfo in initiated.
		public LogInfo loginfo = new LogInfo();

		/// <summary>
		/// Set and get the value of _extentReport field variable.
		/// </summary>
		public static ExtentReports ExtentReport
		{
			get
			{
				// return the _extentReport value.
				return _extentReport;
			}
			set
			{
				// set the value of _extentReport .
				_extentReport = value;
			}
		}

		/// <summary>
		/// Set and get the value of _testName field variable.
		/// </summary>
		public static ExtentTest ExtentTest
		{
			get
			{
				// return the _testName value.
				return _testName;
			}
			set
			{
				// set the value of _testName.
				_testName = value;
			}
		}

		/// <summary>
		/// Function to set all the properties of extent report.
		/// </summary>
		/// <param name="connectorName">Connector used.</param>
		/// <param name="mfilesVersion">M-Files Version.</param>
		/// <param name="vaultApplicationVersion">Connector Application Version.</param>
		/// <returns>Returns the extent report properties as an object.</returns> 
		public static ExtentReports SetUp( string connectorName,
										   string mfilesVersion,
										   string vaultApplicationVersion )
		{
			if( _extentReport == null )
			{
				// Location where the report will be generated.
				var reportLocation = directory + "\\Reports\\TestReport";

				// Location where the screenshots will be generated.
				var screenshotLocation = directory + "\\Reports\\TestReport\\Screenshots";

				// Verifies whether the reportLocation exists.
				bool reportFolderExists = System.IO.Directory.Exists( reportLocation );

				// Verifies whether the screenshotLocation exists.
				//bool screenshotFolderExists = System.IO.Directory.Exists( screenshotLocation ); 

				// Deletes the report folder if it exists.
				if( !reportFolderExists )
				{
					// Creates report folder.
					System.IO.Directory.CreateDirectory( reportLocation );
				}

				// Creates screenshot folder.
				//System.IO.Directory.CreateDirectory( screenshotLocation );

				// Remove all the spaces in the string.
				connectorName = connectorName.Replace( " ", "" );

				// Reprot Name
				string extentReportName = "Report-" + connectorName + "(" +
						vaultApplicationVersion + ")-M-Files(" + mfilesVersion + ")";

				// Get all the files in the folder and delete.
				string[] files = Directory.GetFiles( reportLocation );

				foreach( string file in files )
				{
					// Delete the report if it already exists.
					if( file.Contains( connectorName ) && file.Contains( vaultApplicationVersion )  )
						File.Delete( file );
				}

				// Initializes Extent HTML report.
				var htmlReporter =
					new ExtentHtmlReporter( reportLocation + "\\" + extentReportName + ".html" );

				// Assigns whether existing report has to be overwritten or appended by new report. 
				htmlReporter.AppendExisting = false;

				// Allow viewing or hiding charts on report open.
				htmlReporter.Configuration().ChartVisibilityOnOpen = true;

				// Name to be displayed in html file on opening.        
				htmlReporter.Configuration().DocumentTitle = "IML Connector Automation";

				// Report name displayed in extent report.
				htmlReporter.Configuration().ReportName = extentReportName;

				// Defines the ChartLocation. 
				htmlReporter.Configuration().ChartLocation = ChartLocation.Top;

				// Defines the Theme of extent report.
				htmlReporter.Configuration().Theme = Theme.Standard;

				// Initializes ExtentReports Class.
				_extentReport = new ExtentReports();

				// Add the machine name to extent report.
				_extentReport.AddSystemInfo( "Host Name", Environment.MachineName );

				// Add the OS of the machine to extent report.
				_extentReport.AddSystemInfo( "OS", Environment.OSVersion.ToString() );

				// Add the logged in username of the machine to extent report.
				_extentReport.AddSystemInfo( "User Name", Environment.UserName );

				// Add the domain name to extent report.
				_extentReport.AddSystemInfo( "User Domain Name", Environment.UserDomainName );

				// Add the M-Files version to extent report.
				_extentReport.AddSystemInfo( "M-Files Version", mfilesVersion );

				// Add the application version to extent report.
				_extentReport.AddSystemInfo( connectorName + " version", vaultApplicationVersion );

				// Initiates new instance of Registry class.
				RegistryKey registryKey = Registry.LocalMachine;

				// Opens the Java details available in registry key.
				RegistryKey subKey = registryKey.OpenSubKey( "SOFTWARE\\JavaSoft\\Java Runtime Environment" );

				// Add the Java details to extent report.
				//_extentReport.AddSystemInfo( "Java Version", subKey.GetValue( "CurrentVersion" ).ToString() );

				// Attach  all started tests, nodes and logs. 
				_extentReport.AttachReporter( htmlReporter );

			}

			// Return the details to be added in extent report as an object.
			return _extentReport;

		}

		/// <summary>
		/// Function to capture the screenshot for various operations.
		/// </summary>
		/// <param name="webDriver">Optional for Connetor Automation.</param> 
		/// <returns>Returns the screenshot name along with the path.</returns> 
		public static string CaptureScreenShot( IWebDriver webDriver )
		{
			try
			{
				// Initaites an instance of ITakesScreenshot class.
				ITakesScreenshot ts = ( ITakesScreenshot ) webDriver;

				// Takes the screenshot.
				Screenshot screenshot = ts.GetScreenshot();

				// Screenshot name is defined along with the path.
				string ScreenShotFileName = directory + "\\Reports\\Screenshots\\" + string.Format(
					"{0}_{1}.png", NUnit.Framework.TestContext.CurrentContext.Test.Name,
					string.Format( "{0:MM_dd_yy_HH_mm_ss}", DateTime.Now ) );

				// Save the screenshot in the defined folder.
				screenshot.SaveAsFile( ScreenShotFileName );

				// Returns the screenshot name along with the path.
				return ScreenShotFileName;

			}

			catch( Exception exception )
			{
				// Throws exception if any.
				throw exception;

			}
		}

		/// <summary>
		/// Add the exception occured to extent report.
		/// </summary>
		/// <param name="writeException">Exception occured.</param>
		/// <param name="webDriver">Optional for Connetor Automation.</param>  
		public static void LogException(
			Exception writeException,
			IWebDriver webDriver = null )
		{
			// Add the information in log file.
			Logger.Error( writeException );

			// Verifies webDriver is null, takes screenshot and add the message in extent report accordingly.
			if( webDriver != null )
			{
				try
				{
					// Path where screenshot has to be placed.
					string screenshotPath = CaptureScreenShot( webDriver );

					// Add the information to extent report along with screenshot.
					ExtentTest.Log( Status.Error, writeException,
						MediaEntityBuilder.CreateScreenCaptureFromPath( screenshotPath ).Build() );
				}

				catch( Exception exceptionOnScreenShot )
				{
					// Log the exception occur on screenshot.
					LogException(
						new Exception( "Could not Take Screen Shot due to error" + exceptionOnScreenShot.Message ) );

					// Add the information to extent report.
					ExtentTest.Log( Status.Error, writeException );
				}

			} // end if

			else
			{
				// Add the information in extent report.
				childTest.Log( Status.Error, writeException.StackTrace + "<br/>" + writeException.Message );

			} // end else
		}

		/// <summary>
		/// Add the information of passed test methods in extent report.
		/// </summary>
		/// <param name="passMessage">Message to be logged in extent report.</param>
		/// <param name="webDriver">Optional for Connetor Automation.</param>  
		public static void LogPass(
			string passMessage,
			IWebDriver webDriver = null )
		{
			// Add the information in log file.
			Logger.Debug( passMessage );

			// Verifies webDriver is null, takes screenshot and add the message in extent report accordingly.
			if( webDriver != null )
			{
				try
				{
					// Path where screenshot has to be placed.
					string screenshotPath = CaptureScreenShot( webDriver );

					// Add the information to extent report along with screenshot.
					ExtentTest.Log( Status.Pass, passMessage,
						MediaEntityBuilder.CreateScreenCaptureFromPath( screenshotPath ).Build() );
				}

				catch( Exception exceptionOnScreenShot )
				{
					// Log the exception occur on screenshot.
					ExtentTest.Log( Status.Pass, passMessage );

					// Add the information to extent report.
					LogException(
						new Exception( "Could not Take Screen Shot due to error" + exceptionOnScreenShot.Message ) );
				}

			} // end if

			else
			{
				// Add the information in extent report.
				childTest.Log( Status.Pass, passMessage );

			} // end else
		}

		/// <summary>
		/// Add the information of skipped steps or test methods.
		/// </summary>
		/// <param name="skipMessage">Message to be logged in extent report.</param>
		/// <param name="webDriver">Optional for Connetor Automation.</param>  
		public static void LogSkip(
			string skipMessage,
			IWebDriver webDriver = null )
		{
			// Add the information in log file.
			Logger.Debug( skipMessage );

			// Verifies webDriver is null, takes screenshot and add the message in extent report accordingly.
			if( webDriver != null )
			{
				try
				{
					// Path where screenshot has to be placed.
					string screenshotPath = CaptureScreenShot( webDriver );

					// Add the information to extent report along with screenshot.
					ExtentTest.Log( Status.Skip, skipMessage,
						MediaEntityBuilder.CreateScreenCaptureFromPath( screenshotPath ).Build() );
				}
				catch( Exception exceptionOnScreenShot )
				{
					// Log the exception occur on screenshot.
					ExtentTest.Log( Status.Skip, skipMessage );

					// Add the information to extent report.
					LogException(
						new Exception( "Could not Take Screen Shot due to error" + exceptionOnScreenShot.Message ) );
				}

			} // if

			else
			{
				// Add the information in extent report.
				childTest.Log( Status.Skip, skipMessage );

			} // end else
		}

		/// <summary>
		/// Add the details of the failed test methods in extent report.
		/// </summary>
		/// <param name="failedMessage">Message to be logged in extent report.</param>
		/// <param name="webDriver">Optional for Connetor Automation.</param>  
		public static void LogError(
			string failedMessage,
			IWebDriver webDriver = null )
		{
			// Add the information in log file.
			Logger.Error( failedMessage );

			// Verifies webDriver is null, takes screenshot and add the message in extent report accordingly.
			if( webDriver != null )
			{
				try
				{
					// Path where screenshot has to be placed.
					string screenshotPath = CaptureScreenShot( webDriver );

					// Add the information to extent report along with screenshot.
					ExtentTest.Log( Status.Fail, failedMessage,
						MediaEntityBuilder.CreateScreenCaptureFromPath( screenshotPath ).Build() );
				}
				catch( Exception exceptionOnScreenShot )
				{
					// Log the exception occur on screenshot.
					ExtentTest.Log( Status.Fail, failedMessage );

					// Add the information to extent report.
					LogException(
						new Exception( "Could not Take Screen Shot due to error" + exceptionOnScreenShot.Message ) );
				}

			} // end if

			else
			{
				// Add the information in extent report.
				childTest.Log( Status.Error, failedMessage );
			} // end else

		}

		/// <summary>
		/// Add the details of the failed test methods in extent report.
		/// </summary>
		/// <param name="failedMessage">Message to be logged in extent report.</param>
		/// <param name="webDriver">Optional for Connetor Automation.</param>  
		public static void LogFail(
			string failMessage,
			IWebDriver webDriver = null )
		{
			// Add the information in log file.
			Logger.Error( failMessage );

			// Verifies webDriver is null, takes screenshot and add the message in extent report accordingly.
			if( webDriver != null )
			{
				try
				{
					// Path where screenshot has to be placed.
					string screenshotPath = CaptureScreenShot( webDriver );

					// Add the information to extent report along with screenshot.
					ExtentTest.Log( Status.Fail, failMessage,
						MediaEntityBuilder.CreateScreenCaptureFromPath( screenshotPath ).Build() );
				}
				catch( Exception exceptionOnScreenShot )
				{
					// Log the exception occur on screenshot.
					ExtentTest.Log( Status.Fail, failMessage );

					// Add the information to extent report.
					LogException(
						new Exception( "Could not Take Screen Shot due to error" + exceptionOnScreenShot.Message ) );
				}

			} // end if

			else
			{
				// Add the information in extent report.
				childTest.Log( Status.Fail, failMessage );

			} // end else

		}

		/// <summary>
		/// Add information about the steps or test method to extent report.
		/// </summary>
		/// <param name="infoMessage">Message to be logged in extent report.</param>
		/// <param name="webDriver">Optional for Connetor Automation.</param>     
		public static void LogInfo(
			string infoMessage,
			IWebDriver webDriver = null )
		{
			// Add the information in log file.
			Logger.Info( infoMessage );

			// Verifies webDriver is null, takes screenshot and add the message in extent report accordingly.
			if( webDriver != null )
			{
				try
				{
					// Path where screenshot has to be placed. 
					string screenshotPath = CaptureScreenShot( webDriver );

					// Add the information to extent report along with screenshot.
					ExtentTest.Log( Status.Info, infoMessage,
						MediaEntityBuilder.CreateScreenCaptureFromPath( screenshotPath ).Build() );
				}
				catch( Exception exceptionOnScreenShot )
				{

					// Log the exception occur on screenshot.
					ExtentTest.Log( Status.Info, infoMessage );

					// Add the information to extent report.
					LogException(
						new Exception( "Could not Take Screen Shot due to error" + exceptionOnScreenShot.Message ) );
				}

			} // end if

			else
			{
				// Add the information in extent report.
				childTest.Log( Status.Info, infoMessage );

			} // end else

		}

		/// <summary>
		/// Ends the current test and add the status in extent report.
		/// </summary>
		/// <param name="testStatus">Status of the current test method.</param>
		/// <param name="TestName">Name of the current test method.</param>  
		public static void EndTest(
				Object testStatus,
				string TestName )
		{
			// Stores the instance of Status class of type enum.
			Status logstatus = Status.Pass;

			// Stores the testmethod status.
			string testMethodStatus = testStatus.ToString();

			// Switch case to assign the enum variable according to the status of the test method.
			switch( testMethodStatus.ToLower() )
			{
				case "fail":

					// The test method status is failed.
					logstatus = Status.Fail;
					break;

				case "failed":

					// The test method status is failed.
					logstatus = Status.Fail;
					break;

				case "inconclusive":

					// The test method status has warning.
					logstatus = Status.Warning;
					break;

				case "aborted":

					// The test method status has warning.
					logstatus = Status.Warning;
					break;

				case "skipped":

					// The test method has been skipped.
					logstatus = Status.Skip;
					break;

			} // end switch

			// If value of testStatus is TestStatus then test method status is assigned by TestStatus enum tupe
			if( testStatus is TestStatus )
			{
				// TestStatus enum constant.

				// Stores the instance of TestStatus class of type enum.
				TestStatus testItemStatus = ( TestStatus ) testStatus;

				// Switch case to assign the enum variable according to the status of the test method.
				switch( testItemStatus )
				{
					case TestStatus.Failed:

						// The test method status is failed.
						logstatus = Status.Fail;

						// Add the information of failed test to extent report.
						LogFail( TestName + ":Failed" );
						break;

					case TestStatus.Inconclusive:

						// The test method status has warning.
						logstatus = Status.Warning;
						break;

					case TestStatus.Skipped:

						// The test method has been skipped.
						logstatus = Status.Skip;
						break;

					default:

						// The test method has been Passed.
						logstatus = Status.Pass;
						break;

				} // end switch

			} // end if

			// If value of testStatus is UnitTestOutcome then test status is assigned by UnitTestOutcome enum type
			else if( testStatus is UnitTestOutcome )
			{
				// UnitTestOutcome enum constant.

				// Stores the instance of UnitTestOutcome class of type enum.
				UnitTestOutcome testUnitStatus = ( UnitTestOutcome ) testStatus;

				// Switch case to assign the enum variable according to the status of the test method.
				switch( testUnitStatus )
				{
					case UnitTestOutcome.Failed:

						// The test method status is failed.
						logstatus = Status.Fail;

						// Add the information of failed test to extent report.
						LogFail( TestName + ":Failed" );
						break;

					case UnitTestOutcome.Aborted:

						// The test method status has warning.
						logstatus = Status.Warning;
						break;

					case UnitTestOutcome.Inconclusive:

						// The test method has been skipped.
						logstatus = Status.Skip;
						break;

					default:

						// The test method has been Passed.
						logstatus = Status.Pass;
						break;

				} // end switch

			} // end else if

			// Writes the extent report about the status of current method.
			ExtentTest.Log( logstatus, TestName + " " + testStatus );

			// Add all the information to the extent report and end the instance.
			Flush();

		}

		/// <summary>
		/// Get the TestMethodName, Description, Category and initiates to create extent report.
		/// </summary>
		public static void StartTest()
		{
			// Stores the TestMethod name.
			string testName = NUnit.Framework.TestContext.CurrentContext.Test.Name;

			// Stores the Description available for the test method.
			string testDescription =
				NUnit.Framework.TestContext.CurrentContext.Test.Properties.Get( "Description" ).ToString();

			// Stores the Category under which the TestItem falls.
			string category =
				NUnit.Framework.TestContext.CurrentContext.Test.Properties.Get( "Category" ).ToString();

			// Start creating the information of the test method in extent report.
			ExtentTest = ExtentReport.
				CreateTest( testName, testDescription ).AssignAuthor( "M-Files" ).AssignCategory( category );
		}


		/// <summary>
		/// Get the TestMethodName, Description, Category and initiates to create extent report.
		/// </summary>
		/// <param name="connectorName">Name of the connector used.</param>
		public static void StartTest( string connectorName )
		{
			// Stores the TestMethod name extending to connecor name.
			string testName =
				NUnit.Framework.TestContext.CurrentContext.Test.Name.Split( '_' )[ 1 ] + "-" + connectorName;

			// Stores the Description available for the test method.
			string testDescription =
				NUnit.Framework.TestContext.CurrentContext.Test.Properties.Get( "Description" ).ToString();

			// Stores the Category under which the TestItem falls.
			string category =
				NUnit.Framework.TestContext.CurrentContext.Test.Properties.Get( "Category" ).ToString();

			// Start creating the information of the test method in extent report.
			ExtentTest = ExtentReport.
				CreateTest( testName, testDescription ).AssignAuthor( "M-Files" ).AssignCategory( category );
		}

		/// <summary>
		/// Create a separate node for each testdata.
		/// </summary>
		/// <param name="caseCount">TestData count.</param>
		public static void LogHeader( int caseCount )
		{
			// Create the separate node for each testdata.
			childTest = ExtentTest.CreateNode( "TestData " + caseCount.ToString() );

		}

		/// <summary>
		/// Appends the HTML file with all the ended tests.
		/// </summary>
		public static void Flush()
		{
			// Data will be wriitten to the extent report.
			ExtentReport.Flush();
		}

		/// <summary>
		/// Initiates the ExtentReport and Logger.
		/// </summary>
		/// <param name="connectorName">Connector used.</param>
		/// <param name="mfilesVersion">M-Files Version.</param>
		/// <param name="vaultApplicationVersion">Connector Application Version.</param>
		public Report( string connectorName,
					   string mfilesVersion,
					   string vaultApplicationVersion )
		{
			// Initiates an instance to log the information in .log file.
			this.loginfo.Logger =
				NLog.LogManager.GetLogger( NUnit.Framework.TestContext.CurrentContext.TestDirectory );

			// Write the TestMethod name.
			Console.WriteLine( "\r\nRunning Testcase: {0}", NUnit.Framework.TestContext.CurrentContext.Test.Name );

			// Function to declare the properties of extent report.
			SetUp( connectorName, mfilesVersion, vaultApplicationVersion );
		}

	}

	/// <summary>
	/// Calculated values used for extent report.
	/// </summary>
	public sealed class LogInfo
	{
		/// <summary>
		/// Returns the instance og the ExtentReports class.
		/// </summary>
		public static ExtentReports Extent { get; set; }

		/// <summary>
		/// Returns the folder of the directory.
		/// </summary>
		public static string Folderpath { get; set; }

		/// <summary>
		/// Returns the current test method name.
		/// </summary>
		public string TestCase { get; set; }

		/// <summary>
		/// Returns the folder of the extent report.
		/// </summary>
		public string FolderPath { get; set; }

		/// <summary>
		/// Returns the step no of the test method.
		/// </summary>
		public int StepNo { get; set; }

		/// <summary>
		/// Returns the message to be logged in extent report.
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Returns the instance og the Logger class.
		/// </summary>
		public NLog.Logger Logger { get; set; }

		/// <summary>
		/// Returns the instance og the ExtentTest class.
		/// </summary>
		public ExtentTest Test { get; set; }

		/// <summary>
		/// Returns whether screenshot has to be taken or not.
		/// </summary>
		public bool FlagToTakeScreenShotOnError { get; set; }

		/// <summary>
		/// Returns the name of the screenshot to be saved.
		/// </summary>
		public string ScreenShotFileName { get; set; }
	}

}

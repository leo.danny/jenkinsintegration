﻿using System;
using System.Data;
using System.Data.OleDb;
using MFNUnit.Utils.Logger;

namespace MFNUnit.Utils.TestData
{
    /// <summary>
    /// A class to create OleDbConnection to fetch data from TestData sheet.
    /// </summary>
    public class IO
    {



        /// <summary>
        /// This method will initiate connection to open testdata document using OleDbConnection.
        /// </summary>
        /// <param name="excelPath">Path to read excel file.</param>
        /// <returns>Returns the OleDbConnection connection.</returns>
        private static OleDbConnection CreateExcelConnection( string excelPath )
        {

            // Provides connection information as a string.
            string oleDbConnection = string.Format( 
                "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; " +
                "Jet OLEDB:Engine Type=5;Extended Properties=Excel 12.0;",
                excelPath );

            // An instance of the OleDbConnection class is initiated.
            return new OleDbConnection( oleDbConnection );

            

        }

        /// <summary>
        /// This method will read excel data.
        /// </summary>
        /// <param name="excelPath">Path to read excel file.</param>
        /// <param name="methodName">Current test method name.</param>
        /// <returns>Return test data value in Data Tables.</returns>
        public static DataTable ReadExcel(
                string excelPath,
                string methodName)
        {
            // 'using' block to dispose the object once the connection is no more needed.
            using (var excelConnection = CreateExcelConnection(excelPath))
            {

                try
                {

                    // Column name 'Sno' is assigned.
                    string columnName = "S No";

                    // Test sheet name for each case has been taken from method name.
                    string excelSheetName = methodName.Split('_')[1];

                    // Opens the excel connection to read data required for the test method.
                    excelConnection.Open();

                    // Initiating the DataTable class.
                    DataTable DTExcelresult = new DataTable();

                    // Fetches the data from excel. 
                    OleDbDataAdapter DataAdapter =
                            new OleDbDataAdapter(
                                string.Format("Select * from [{0}$] where UCase([" + columnName + "]) IS NOT NULL ",
                                excelSheetName), excelConnection);

                    // Stores the test data in a table format.
                    DataAdapter.Fill(DTExcelresult);

                    // Ends the excel connection.
                    excelConnection.Close();

                    // Return the data in table format.
                    return DTExcelresult;

                }

                catch (Exception exception)
                {

                    // Write the exception to console.
                    Console.WriteLine("Exception", exception);

                    // Return null.
                    return null;
                }

            }

        }

    } 

} 

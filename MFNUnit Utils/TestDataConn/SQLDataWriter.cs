﻿using System;
using System.Data.SqlClient;

namespace MFNUnit.Utils.TestData
{
	/// <summary>
	/// This class consists of methods to update the peformance results in SQL Server.
	/// </summary>
	public class SQLDataWriter
	{
		/// <summary>
		/// This method will perform Create/Update/Insert operations with Database/Table/Column/ColumnValue in SQL Server.
		/// </summary>
		/// <param name="dbName">Database name.</param>
		/// <param name="tableName">Table name.</param>
		/// <param name="connectorColumnName">Connector Column name.</param>
		/// <param name="connectorValue">Connector name.</param>
		/// <param name="columnName">Column name.</param>
		/// <param name="columnValue">Column value to be updated.</param>
		/// <param name="MFilesVersionColumn">Column name to store M-Files version.</param>
		/// <param name="MFilesVersionValue">M-Files Version.</param>
		/// <param name="sqlServerName">M-Files Version.</param>
		/// <param name="sqlUserName">M-Files Version.</param>
		/// <param name="sqlPassword">M-Files Version.</param>
		public static void UpdatePerformanceResultToSQL(
			string dbName,
			string tableName,
			string connectorColumnName,
			string connectorValue,
			string columnName, 
			int columnValue, 
			string MFilesVersionColumn,
			string MFilesVersionValue,
			string sqlServerName,
			string sqlUserName,
			string sqlPassword )
		{
			try
			{
				// Connection string to instantiate the connection to SQL Server.
				string connectionString = "Data Source="+ sqlServerName + ";Initial Catalog=master;User ID="+ sqlUserName + ";Password="+ sqlPassword;

				// Create an instance of SqlConnection.
				using( SqlConnection dbconnection = new SqlConnection( connectionString ) )
				{
					// Open the connection to SQL Server.
					dbconnection.Open();

					// Verify whether the DB already exists or not.
					if( !CheckDatabaseExists( dbName, dbconnection ) )
					{
						// DB does not exists already.

						// Creates a new Database in SQL Server.
						CreateDataBase( dbName, dbconnection );
					}

					// Close the connection to the SQL Server.
					dbconnection.Close();
				}

				// Connection string to instantiate the connection to Database in SQL Server.
				connectionString = "Data Source=" + sqlServerName + ";Initial Catalog="+ dbName + ";User ID=" + sqlUserName + ";Password=" + sqlPassword;

				// Create an instance of SqlConnection.
				using( SqlConnection dbconnection = new SqlConnection( connectionString ) )
				{
					// Open the connection to DB in SQL Server.
					dbconnection.Open();

					// Verify whether the table already exists in givenDB.
					if( !CheckTableExists( dbName, tableName, dbconnection ) )
					{
						// Table DB does not exists already.

						// Creates a new table for given DB in SQL Server.
						CreateTable( tableName, MFilesVersionColumn, dbconnection, connectorColumnName );
					}

					// Verify whether the current MFilesVersion has a field in givenDB.
					if( !MFilesVersionRowExists( tableName, MFilesVersionColumn, MFilesVersionValue, dbconnection, connectorColumnName, connectorValue ) )
					{
						// Row does not exists.

						// Creates new row witht the current mfiles version.
						InsertRowValue( tableName, MFilesVersionColumn, MFilesVersionValue, dbconnection, connectorColumnName, connectorValue );
					}

					// Verify whether the given column exists in the table.
					if( !CheckColumnExists( dbName, tableName, columnName, dbconnection ) )
					{
						// Column does not exists.

						// Creates new column in the table.
						CreateColumn( tableName, columnName, dbconnection );
					}

					// Update the column value;
					UpdateColumnValue( tableName, columnName, columnValue, MFilesVersionColumn, MFilesVersionValue, dbconnection, connectorColumnName, connectorValue );

					// Close the connection of DB in SQL Server.
					dbconnection.Close();
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine( "Error while updating the performance results in SQL Server" + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// This method will Verify the existence of Database in SQL Server.
		/// </summary>
		/// <param name="dbName">Database name.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		/// <returns>Return the existence of DB.</returns>
		public static bool CheckDatabaseExists( 
			string dbName, 
			SqlConnection dbconnection )
		{
			try
			{
				// Query to fetch the existence of DB.
				string sqlQuery = "SELECT * FROM master.dbo.sysdatabases WHERE name ='" + dbName + "'";

				bool dbExists = false;

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					using( SqlDataReader dbReader = sqlCmd.ExecuteReader() )
					{
						// Store the existence of DB.
						dbExists = dbReader.HasRows;
					}
				}
				// Return the existence of DB.
				return dbExists;
			}
			catch(Exception ex)
			{
				Console.WriteLine( "Error in function CheckDatabaseExists " + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// This method will Verify the existence of table in the given DB.
		/// </summary>
		/// <param name="dbName">Database name.</param>
		/// <param name="tableName">Table name.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		/// <returns>Return the existence of table.</returns>
		public static bool CheckTableExists( 
			string dbName, 
			string tableName, 
			SqlConnection dbconnection )
		{
			try
			{
				// Query to fetch the existence of table.
				string sqlQuery = "SELECT TABLE_NAME FROM " + dbName + ".information_schema.tables WHERE table_name = '" + tableName + "'";

				bool tableExists = false;

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					using( SqlDataReader dbReader = sqlCmd.ExecuteReader() )
					{
						// Store the existence of table.
						tableExists = dbReader.HasRows;
					}
				}
				// Return the existence of table.
				return tableExists;
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error in function CheckTableExists " + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// This method will Verify the existence of column in the given table.
		/// </summary>
		/// <param name="dbName">Database name.</param>
		/// <param name="tableName">Table name.</param>
		/// <param name="columnValue">Column Name.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		/// <returns>Return the existence of column.</returns>
		public static bool CheckColumnExists( 
			string dbName, 
			string tableName, 
			string columnName, 
			SqlConnection dbconnection )
		{
			try
			{
				// Query to fetch the existence of column.
				string sqlQuery = "SELECT COLUMN_NAME FROM "+ dbName + ".information_schema.columns where TABLE_NAME = '"+ tableName + "' AND COLUMN_NAME = '"+ columnName + "'";

				bool columnExists = false;

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					using( SqlDataReader dbReader = sqlCmd.ExecuteReader() )
					{
						// Store the existence of column.
						columnExists = dbReader.HasRows;
					}
				}
				// Return the existence of column.
				return columnExists;
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error in function CheckColumnExists " + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// This method will create a DB in SQL Server.
		/// </summary>
		/// <param name="dbName">Database name.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		public static void CreateDataBase( 
			string dbName, 
			SqlConnection dbconnection )
		{
			try
			{
				// Query to create a DB in SQL Server.
				string sqlQuery = "Create DATABASE "+ dbName;

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					// Execute the query.
					sqlCmd.ExecuteNonQuery();
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine( "Error in function CreateDataBase " + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// This method will create a table in given DB.
		/// </summary>
		/// <param name="tableName">Table name.</param>
		/// <param name="columnName">Column name.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		/// <param name="connectorColumnName">Connector Column name.</param>
		public static void CreateTable( 
			string tableName, 
			string columnName,
			SqlConnection dbconnection,
			string connectorColumnName)
		{
			try
			{
				// Query to create a table in given DB.
				string sqlQuery = "Create Table " + tableName +"( "+ columnName + " char(50),"+ connectorColumnName + " char(50))";

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					// Execute the query.
					sqlCmd.ExecuteNonQuery();
				}
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error in function CreateTable " + ex.Message );
			}
		}

		/// <summary>
		/// This method will create a column in given table.
		/// </summary>
		/// <param name="tableName">Table name.</param>
		/// <param name="columnName">Column name.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		public static void CreateColumn ( 
			string tableName, 
			string columnName, 
			SqlConnection dbconnection )
		{
			try
			{
				// Query to create a column in given table.
				string sqlQuery = "ALTER TABLE ["+ tableName + "] add ["+ columnName + "] real NULL";

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					// Execute the query.
					sqlCmd.ExecuteNonQuery();
				}
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error in function CreateColumn " + ex.Message );
			}
		}

		/// <summary>
		/// This method will verify whether current M-Files version has a row already.
		/// </summary>
		/// <param name="tableName">Table name.</param>
		/// <param name="MFilesVersionColumn">Column name which has M-Files version.</param>
		/// <param name="MFilesVersionValue">M-Files Version.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		/// <param name="connectorColumnName">Connector Column name.</param>
		/// <param name="connectorValue">Connector name.</param>
		/// <returns>Return the existence of row value.</returns>
		public static bool MFilesVersionRowExists( 
			string tableName,
			string MFilesVersionColumn, 
			string MFilesVersionValue,
			SqlConnection dbconnection,
			string connectorColumnName, 
			string connectorValue )
		{
			try
			{
				// Query to get the existence of a row value.
				string sqlQuery = "Select * from " + tableName + " where ("+ MFilesVersionColumn + " Like '"+ MFilesVersionValue + "' AND " + connectorColumnName + " Like '" + connectorValue + "')";

				bool primaryColumnValueExists = false;

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					using( SqlDataReader dbReader = sqlCmd.ExecuteReader() )
					{
						// Store the existence of row value.
						primaryColumnValueExists = dbReader.HasRows;
					}
				}
				// Return the existence of row value.
				return primaryColumnValueExists;
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error in function MFilesVersionRowExists " + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// This method will update the column with the given value.
		/// </summary>
		/// <param name="tableName">Table name.</param>
		/// <param name="columnName">Column name.</param>
		/// <param name="columnValue">Column Value.</param>
		/// <param name="MFilesVersionColumn">Column name which has M-Files version.</param>
		/// <param name="MFilesVersionValue">M-Files Version.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		/// <param name="connectorColumnName">Connector Column name.</param>
		/// <param name="connectorValue">Connector name.</param>
		public static void UpdateColumnValue( 
			string tableName,
			string columnName,
			int columnValue,
			string MFilesVersionColumn, 
			string MFilesVersionValue,
			SqlConnection dbconnection,
			string connectorColumnName,
			string connectorValue )
		{
			try
			{
				// Query to Update the column value.
				string sqlQuery = "Update "+ tableName + " set "+ columnName + " = '"+ columnValue + "' where "+ MFilesVersionColumn + " = '"+ MFilesVersionValue + "' AND " + connectorColumnName + " Like '" + connectorValue + "'";

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					// Execute the query.
					sqlCmd.ExecuteNonQuery();
				}
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error in function MFilesVersionRowExists " + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// This method will Insert a new row based on the M-Files Version.
		/// </summary>
		/// <param name="tableName">Table name.</param>
		/// <param name="MFilesVersionColumn">Column name which has M-Files version.</param>
		/// <param name="MFilesVersionValue">M-Files Version.</param>
		/// <param name="dbconnection">An active connection to the SQL Server.</param>
		/// <param name="connectorColumnName">Connector Column name.</param>
		/// <param name="connectorValue">Connector name.</param>
		public static void InsertRowValue(
			string tableName, 
			string MFilesVersionColumn, 
			string MFilesVersionValue, 
			SqlConnection dbconnection,
			string connectorColumnName,
			string connectorValue )
		{
			try
			{
				// Query to inser a row based on M-Files Version.
				string sqlQuery = "INSERT INTO "+ tableName + " ("+ MFilesVersionColumn + "," + connectorColumnName + ") Values ('"+ MFilesVersionValue + "', '" + connectorValue + "')" ;

				// Instantiate the SQLCommand to execute the query in SQL Server.
				using( SqlCommand sqlCmd = new SqlCommand( sqlQuery, dbconnection ) )
				{
					// Execute the query.
					sqlCmd.ExecuteNonQuery();
				}
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error in function InsertRowValue " + ex.Message );
				throw ex;
			}
		}
	}
}

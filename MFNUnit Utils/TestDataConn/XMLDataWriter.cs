﻿using System;
using System.IO;
using System.Xml;

namespace MFNUnit.Utils.TestData
{
	/// <summary>
	/// This class consists of methods to update the xml file.
	/// </summary>
	public class XMLDataWriter
	{

		/// <summary>
		/// This method will create the parent tag, child tag and update the atribute value.
		/// </summary>
		/// <param name="rootTagName">Root tag name.</param>
		/// <param name="parentTagName">Parent tag name.</param>
		/// <param name="childTagName">Child tag name.</param>
		/// <param name="xmlDoc">Instance of the xml file.</param>
		/// <param name="performanceTime">Performance time that to be updated.</param>
		/// <param name="mfilesVersion">M-Files Version.</param>
		/// <param name="versionTagName">Tag name to store M-Files Version.</param>
		public static void UpdatingPerformanceResultInXML( 
			string rootTagName, 
			string parentTagName, 
			string childTagName, 
			XmlDocument xmlDoc, 
			string performanceTime, 
			string mfilesVersion, 
			string versionTagName )
		{
			// Stores the existence of Root tag.
			XmlElement rootTagExists = ( XmlElement ) xmlDoc.SelectSingleNode( rootTagName );

			// Check the existence of the Root tag.
			if( rootTagExists == null )
			{
				// Root tag does not exsits.

				// Create the RootTag.
				XmlElement createRootTag = xmlDoc.CreateElement( rootTagName );
				xmlDoc.DocumentElement.AppendChild( createRootTag );
			}

			// URL to the Parent Tag.
			string parentTagURL = rootTagName + "/" + parentTagName;

			// Stores the instance of parent tag.
			XmlElement parentTagExists = ( XmlElement ) xmlDoc.SelectSingleNode( parentTagURL );

			//Check the existence of the Parent Tag.
			if( parentTagExists == null )
				// Create the parent tag, child tag and update the atribute value.
				CreatingNewParentAndChildTag( rootTagName, parentTagName, childTagName, performanceTime, mfilesVersion, versionTagName, xmlDoc );
			else
				// Update the atribute value of child tag.
				UpdatingParentAndChildTag( parentTagExists, xmlDoc, rootTagName, parentTagName, childTagName, mfilesVersion, versionTagName, performanceTime );

		}

		/// <summary>
		/// This method will create the parent tag, child tag and update the atribute value.
		/// </summary>
		/// <param name="rootTagName">Root tag name.</param>
		/// <param name="parentTagName">Parent tag name.</param>
		/// <param name="childTagName">Child tag name.</param>
		/// <param name="value">Value to be upadted for child tag.</param>
		/// <param name="mfilesVersion">M-Files Version.</param>
		/// <param name="versionTagName">Tag name to store M-Files Version.</param>
		/// <param name="xmlDoc">Instance of the xml file.</param>
		public static void CreatingNewParentAndChildTag( 
			string rootTagName,
			string parentTagName, 
			string childTagName, 
			string value,
			string mfilesVersion,
			string versionTagName,
			XmlDocument xmlDoc)
		{
			try
			{				
				// Create a new tag with parentTagName.
				XmlElement createParentTag = xmlDoc.CreateElement( parentTagName );
				xmlDoc.DocumentElement.AppendChild( createParentTag );

				// Create a new tag and set the build version.
				XmlElement mfVersionTag = xmlDoc.CreateElement( versionTagName );
				mfVersionTag.SetAttribute("Value", mfilesVersion );
				createParentTag.AppendChild( mfVersionTag );

				// Create a new tag with childTagName.
				XmlElement createChildTag = xmlDoc.CreateElement( childTagName );

				// Insert the attribute value with the given value.
				createChildTag.InnerText = value;

				// Append the changes below the specific parentTag.
				mfVersionTag.AppendChild( createChildTag );
			}
			catch( Exception exception )
			{
				throw exception;
			}
		}

		/// <summary>
		/// This method will update the atribute value of child tag.
		/// </summary>
		/// <param name="parentTagExists">Existence of Parent tag.</param>
		/// <param name="xmlDoc">Instance of the xml file.</param>
		/// <param name="rootTagName">Root tag name.</param>
		/// <param name="parentTagName">Child tag name.</param>
		/// <param name="childTagName">URL of the child tag.</param>
		/// <param name="mfilesVersion">M-Files Version.</param>
		/// <param name="versionTagName">Tag name to store M-Files Version.</param>
		/// <param name="value">Value to be upadted for child tag.</param>
		public static void UpdatingParentAndChildTag( 
			XmlElement parentTagExists,
			XmlDocument xmlDoc,
			string rootTagName,
			string parentTagName,
			string childTagName,
			string mfilesVersion,
			string versionTagName,
			string value )
		{
			try
			{
				// URL to the version tag.
				string versionTagURL = rootTagName + "/" + parentTagName + "/" + versionTagName;

				// Stores the instance of version tag.
				XmlNode versionTagExists = xmlDoc.SelectSingleNode( versionTagURL );

				// Instace of the version tag element. Used when tag does not exists.
				XmlElement mfVersionTag;

				// Check whether the version tag already exists.
				if( versionTagExists == null )
				{
					// Version tag does not exists.

					// Create the version tag and set the build version.
					mfVersionTag = xmlDoc.CreateElement( versionTagName );
					mfVersionTag.SetAttribute( "Value", mfilesVersion );
					parentTagExists.AppendChild( mfVersionTag );

					// Create new child tag.
					XmlElement createChildTag = xmlDoc.CreateElement( childTagName );

					// Insert the attribute value with the given value.
					createChildTag.InnerText = value;

					// Append the changes below the specific parent tag.
					mfVersionTag.AppendChild( createChildTag );

				}
				else
				{
					//	Version tag already exists.

					// Instance of the version tag URL.
					XmlElement updateMFVersion = ( XmlElement ) xmlDoc.SelectSingleNode( versionTagURL );
					
					// Update the version tag value.
					updateMFVersion.SetAttribute( "Value", mfilesVersion ); // Set to new value.

					//  URL to the child tag.
					string childTagURL = versionTagURL + "/" + childTagName;

					// Stores the existence of child tag.
					XmlNode childTagExists = xmlDoc.SelectSingleNode( childTagURL );
					
					// Verify the existence of child tag.
					if( childTagExists == null )
					{
						// Child tag does not exists.

						// Create new child tag.
						XmlElement createChildTag = xmlDoc.CreateElement( childTagName );

						// Insert the attribute value with the given value.
						createChildTag.InnerText = value;

						// Append the changes below the specific parent tag.
						versionTagExists.AppendChild( createChildTag );
					}
					else
					{
						// Child tag exists.

						// Stores the existence of child tag.
						XmlNode updateChildTag =
							xmlDoc.SelectSingleNode( childTagURL );

						// Insert the attribute value with the given value.
						updateChildTag.InnerText = value;
					}
				}
			}
			catch( Exception exception )
			{
				throw exception;
			}

		}
	}
}
﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Runtime.CompilerServices;
using System.Data;
using System.IO;
using System.Xml;

namespace MFNUnit.Utils.TestData
{
    /// <summary>
    /// Customizing TestCaseSourceAttribute definitions.
    /// </summary>
    /// <param name="AttributeTargets.Method">Defines that the customized attribute can be used by a method.</param>
    /// <param name="AllowMultiple">Indicates whether more than one instance of the attribute can be specified.</param>
    [AttributeUsage(
            AttributeTargets.Method,
            AllowMultiple = false)]
    public class TestCaseSourceXML : TestCaseSourceAttribute
    {
        /// <summary>
        /// Component to fetch the test data, by referring to to the sourcename. 
        /// </summary>
        /// <param name="sourceType">Type of the method name where the definitions are done.</param>
        /// <param name="sourceName">Method name where the definitions are done.</param>
        /// <param name="methodName">Test method name.</param>
        public TestCaseSourceXML(
            Type sourceType,
            string sourceName,
            [CallerMemberName]string methodName = null
            ) : base(sourceType, sourceName, new object[] { methodName })
        {
        }

    }

    /// <summary>
    /// Customizing TestCaseSourceEx definitions.
    /// </summary>
    /// <param name="AttributeTargets.Method">Defines that the customized attribute can be used by a method.</param>
    /// <param name="AllowMultiple">Indicates whether more than one instance of the attribute can be specified.</param>
    [AttributeUsage(
            AttributeTargets.Method,
            AllowMultiple = false)]
    public class FactoryTestCaseSourceXML : TestCaseSourceXML
    {
        /// <summary>
        /// Component to fetch the test data, by referring to to the methodname. 
        /// </summary>
        /// <param name="methodName">Test method name.</param>
        public FactoryTestCaseSourceXML([CallerMemberName] string methodName = null)
            : base(typeof(MyDataSources), "FactoryMethod", methodName)
        {
        }
    }

    /// <summary>
    /// A class to create OleDbConnection to fetch data and return the value in DataRow type.
    /// </summary>
    public class MyDataSources
    {
        // Get the path of the project.
        public static string directoryPath = Directory.GetParent(NUnit.Framework.TestContext.CurrentContext.TestDirectory).Parent.FullName;

        /// <summary>
        /// Fetch the data from xml connection and return it as DataRow type.
        /// </summary>
        /// <param name="methodName">Name of the current test method.</param>
        /// <returns>Returns the data in DataRow type.</returns>
        public static IEnumerable<object> FactoryMethod(string methodName)
        {
            // Stores the testdata path.
            string xmlLocation = directoryPath + "\\Resources\\" + methodName.Split('_')[0] + ".xml";
            DataTable dtXMLresult = XMLValues.GetDataFromXML(xmlLocation, methodName);

            // Verify the existence of test data file and read the data and stores the data in table format.
            if (!string.IsNullOrWhiteSpace(xmlLocation))
            {
                // TestData file exists.

                // Get the test data in table format and store it in DataRow type.
                for (int rowNo = 0; rowNo < dtXMLresult.Rows.Count; rowNo++)
                {
                    // Represents a row of data in a DataTable.
                    System.Data.DataRow resultRows = dtXMLresult.Rows[rowNo];

                    // Return the tesdata in DataRow type.
                    yield return new Tuple<System.Data.DataRow>(resultRows);

                } // end for

            } // end if           

        }

    }


    /// <summary>
    /// A class to get the value of respective column.
    /// </summary>
    public static class XMLValues
    {

        /// <summary>
        /// To fetch the value of a particular column.
        /// </summary>
        /// <param name="dataRow">All the values in the current row.</param>
        /// <param name="columnName">Column name for which value is needed.</param>
        /// <returns>Return the value of the respective column.</returns>
        public static string GetValueForColumn(
                this DataRow dataRow,
                string columnName)
        {
            // String to store the column value.
            string columnValue = null;

            try
            {
                // Verifies whether the column name is available and assign the column value.
                if (dataRow.Table.Columns.Contains(columnName))
                    columnValue = dataRow.Field<string>(columnName);

                // Return column value.
                return columnValue;

            }

            catch (Exception exception)
            {
                // Write the exception to console.
                Console.WriteLine("Exception", exception);

                // Throws exception.
                throw exception;

            }

        }

		/// <summary>
		/// This method will store the testdata in DataTable.
		/// </summary>
		/// <param name="xmlPath">Path of the XML file.</param>
		/// <param name="methodName">Current test method name.</param>
		/// <returns>The testdata in DataTable.</returns>
		public static DataTable GetDataFromXML(string xmlPath, 
			string methodName)
        {
			// Loop count.
			int count = 0;

			// Number of testdata.
			int testDataCount;

			// Stores the testname.
			string testDataName = methodName.Replace( methodName.Split( '_' )[0] + "_", "");
            
			// Create a DataTable.  
            DataTable dataXML = new DataTable();

            // Object to store the testdata in XML format.  
            XmlDocument xmlDoc = new XmlDocument();
            XmlNodeList xmlMethodNodes, xmlChildNodes;
			
			// Read the testdata.
            FileStream fileContent = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);

            // Loads the XML content as XML.
            xmlDoc.Load( fileContent );

            // Selects the testData which have the current method name
            xmlMethodNodes = xmlDoc.SelectNodes("/tests/test[@Name=\"" + testDataName + "\"]/testData");

			// Stores the test data in XML file as DataTable format. 
            for ( count = 0; count <= xmlMethodNodes.Count - 1; count++ )
            {
				// Stores the number of fields used.
                xmlChildNodes = xmlMethodNodes[ count ].ChildNodes;

				// Stores the data of each row.
                foreach (XmlNode node in xmlChildNodes)
                    if (!dataXML.Columns.Contains(node.Name))
                        dataXML.Columns.Add(new DataColumn(node.Name, typeof(string)));

				// Number of testdata.
				testDataCount = 0;

				// Adds the row in the data set to add the values
				dataXML.Rows.Add();

                // Execute a loop over the columns.  
                foreach (XmlNode node in xmlChildNodes)
                {
					// Stores the tag values.
                    dataXML.Rows[dataXML.Rows.Count - 1][ testDataCount ] = node.InnerText;
					testDataCount++;
                }
            }

			// Return the testdata as DataTable.
            return dataXML;
        }

    }

}



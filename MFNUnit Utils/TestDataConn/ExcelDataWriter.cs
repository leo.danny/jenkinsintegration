﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFNUnit.Utils.TestData
{
    public class ExcelDataWriter
    {

        /// <summary>
        /// This method will create a new instance of oledbconnection.
        /// </summary>
        /// <param name="excelPath">Path of the excel file.</param>
        /// <returns>The instance of the oledb connection.</returns>
        public OleDbConnection CreateExcelConnection(string excelPath)
        {

            // Provides connection information as a string.
            string oleDbConnection = string.Format(
                "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; " +
                "Jet OLEDB:Engine Type=5;Extended Properties=Excel 12.0 Xml;",
                excelPath);

            // An instance of the OleDbConnection class is initiated and returned.
            return new OleDbConnection(oleDbConnection);
        }

        /// <summary>
        /// This method will create a new instance of oledbconnection.
        /// </summary>
        /// <param name="excelConnection">An active oledb connection.</param>
        /// <param name="sheetName">Worksheet name.</param>
        /// <param name="testName">Current test name.</param>
        /// <param name="columnToBeUpdated">Colum which has to be updated.</param>
        /// <param name="valueToBeUpdated">Value which has to be updated in the respective column.</param>
        public void ExecuteQueryToUpdate(
                OleDbConnection excelConnection,
                string sheetName,
                string testName,
                string columnToBeUpdated,
                string valueToBeUpdated)
        {
            // Varible to iterate through the row.
            int count;

            // Fetches the data from excel. 
            OleDbDataAdapter DataAdapter = new OleDbDataAdapter(string.Format(
                "Select Test from [{0}$] where Test='{1}'", sheetName, testName), excelConnection);

            // Stores the test data in a table format.
            DataTable DTExcelresult = new DataTable();
            DataAdapter.Fill(DTExcelresult);

            // Loop to find whether the test name exists in the document.
            for (count = 0; count < DTExcelresult.Rows.Count; count++)
            {
                // Stores the respective test name from table.
                string resultName = DTExcelresult.Rows[count].ItemArray[0].ToString();

                // Compare with current test name.
                if (resultName == testName)
                    break;
            }

            if (count < DTExcelresult.Rows.Count)
            {
                // Test name already exists in the document.

                // Create new instance of the OleDbCommand.
                OleDbCommand updateCommand = new OleDbCommand();

                // Query to update the value in the document.
                string updateValueQuery = "Update [" + sheetName + "$] set [" + columnToBeUpdated + "] = '"
                    + valueToBeUpdated + "' where Test= '" + testName + "'";

                // Sets the OleDbConnection.
                updateCommand.Connection = excelConnection;

                // Set the query string.
                updateCommand.CommandText = updateValueQuery;

                //Execute the query.
                updateCommand.ExecuteNonQuery();
            }
            else
            {
                // Test name doesnot exists in the document.

                // Query to insert the value in the document.
                var insertValueQuery = "Insert Into [" + sheetName + "$] " +
                    "([Test],[" + columnToBeUpdated + "]) Values (@testName, @executionTime)";

                // Create new instance of the OleDbCommand.
                using (var insertCommand = new OleDbCommand(insertValueQuery, excelConnection))
                {
                    // Command to insert the value for each column.
                    insertCommand.Parameters.AddWithValue("@testName", testName);
                    insertCommand.Parameters.AddWithValue("@executionTime", valueToBeUpdated);

                    // Execute the query.
                    insertCommand.ExecuteNonQuery();
                }
            }
        }
    }
}

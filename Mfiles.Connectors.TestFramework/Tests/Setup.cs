﻿using System;
using Mfiles.Connectors.TestFramework.Configuration;
using Mfiles.Connectors.TestFramework.TestRunSetup;
using MFilesAPI;
using NUnit.Framework;
using System.Security.Principal;
using MFNUnit.Utils.Logger;
using System.DirectoryServices;
using System.Web.Security;

namespace Mfiles.Connectors.TestFramework.Tests
{
	/// <summary>
	/// Class for test setup.
	/// </summary>
	[SetUpFixture]
	internal class Setup
	{
		/// <summary>
		/// The vault Id.
		/// </summary>
		public static Guid VaultId;

		/// <summary>
		/// M-Files server application.
		/// </summary>
		private MFilesServerApplication _mfServer;

		/// <summary>
		/// Name of the vault to connect to.
		/// </summary>
		public static string vaultName;

		/// <summary>
		/// Name of the connector.
		/// </summary>
		public static string connectorName;

		/// <summary>
		/// Stores the M-Files Version as a string.
		/// </summary>
		public static string mFilesVersion;
		
		/// <summary>
		/// Name of the new windows user to add.
		/// </summary>
		public static string addNewWindowsUser;

		/// <summary>
		/// Password used for the new windows user.
		/// </summary>
		public static string newWindowsUserPassWord;

		/// <summary>
		/// Destroy the vault.
		/// </summary>
		public void DestroyVault()
		{
			// Get the Guid of the vault.
			string vaultGuid = this._mfServer.GetOnlineVaults().GetVaultByName( vaultName ).GUID;

			// Destroy the vault.
			this._mfServer.VaultManagementOperations.DestroyVault( vaultGuid );
		}

		[OneTimeSetUp]
		public void OneTimeSetUp()
		{
			// Load test configuration.
			TestConfiguration testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();
			
			// Set the vault name.
			vaultName = testConfiguration.ConnectorInstallationConfiguration.NameOfVaultToConnectTo;

			//Stores the connector name.
			connectorName = testConfiguration.ConnectorInstallationConfiguration.ConnectorName;

			// Add the LoginAccounts to the server.
			this.UsersForServer( testConfiguration );

			// Adds the new windows user to the server login accounts.
			if( connectorName == "Network Folder" && testConfiguration.Users.AdminUser.ExternalCredentials.Username == "" )
			{
				// Network Folder connector.

				// Stores the new windows user name.
				addNewWindowsUser = "nunit_tester";

				// Login account name including the domain name.
				string loginAccountName = Environment.MachineName + "\\" + addNewWindowsUser;

				// Assign the password to the new windows user.
				newWindowsUserPassWord = this.AssignAndGetWindowsUser( addNewWindowsUser );

				// Add the new windows user to the server login accounts.
				this.AddLoginAccount( addNewWindowsUser, newWindowsUserPassWord, Environment.MachineName,
					true, MFLicenseType.MFLicenseTypeNamedUserLicense, true );
			}

			// Connect the user to the M-Files server.
			this._mfServer = this.ConnectToServer( testConfiguration );

			// Create or Restore the vault based on the configuration.
			if( testConfiguration.ConnectorInstallationConfiguration.CreateNewVault.Equals( "Yes" ) )
			{
				// Create new vault.
				this.CreateNewVault( testConfiguration );
			}
			else if( testConfiguration.ConnectorInstallationConfiguration.VaultRestoreRequired.Equals( "Yes" ) )
			{
				//Restore the vault from backup.
				this.RestoreVault( testConfiguration );
			}

			// Add users from configuration and windows user to the vault.
			this.UsersForVault( testConfiguration );

			// Adds the new windows user to the vault users.
			if( connectorName == "Network Folder" && testConfiguration.Users.AdminUser.ExternalCredentials.Username == "" )
			{
				// Network Folder connector.

				// Login account name including the domain name.
				string loginAccountName = Environment.MachineName + "\\" + addNewWindowsUser;

				// Adding new windows user to the vault.
				this.AddUserToVault( loginAccountName, true );

				// Assigning the external crendentials with new windows user credentials.
				testConfiguration.Users.AdminUser.ExternalCredentials.Username = loginAccountName;
				testConfiguration.Users.AdminUser.ExternalCredentials.Password = newWindowsUserPassWord;
			}

			// Initialize and install connector.
			Guid vaultId = this.InstallConnector( testConfiguration );

			// Authenticate the users of the vault if connector authentication required is set to 'Yes' in configuration
			if( testConfiguration.ConnectorInstallationConfiguration.ConnectorAuthenticationRequired.Equals( "Yes" ) )
				AuthenticateUsers( vaultId, testConfiguration, _mfServer );

			//// Verify the structure of the test repository.
			//var verifier = new StructureVerifier(testConfiguration, vaultId);
			//verifier.VerifyTestStructure();

			// Save the vault id to the static field.
			Setup.VaultId = vaultId;

			// Add the vault connection.
			CreateVaultConnection();

			// Get the version of vault application.
			string vaultApplicationVersion =
				getVaultApplicationLicense( VaultId, testConfiguration.ConnectorInstallationConfiguration );

			// Object to get the mfiles version installed in the machine.
			MFilesVersion mfilesVersion = this._mfServer.GetServerVersion();

			// Stores the m-files version as a string.
			mFilesVersion = mfilesVersion.Display;
			
			// Initiates the Report class for logging the information in extent report.
			Report Report = new Report( connectorName, mfilesVersion.Display, vaultApplicationVersion );
		}

		[OneTimeTearDown]
		public void TearDown()
		{
			// Destroy the vault.
			this.DestroyVault();

			// Generate the graphical view of performance test results.
			GeneratingPerformanceResults.CreatePerformanceTestChartView();
		}

		/// <summary>
		/// Connects to the server.
		/// </summary>
		/// <param name="testConfiguration">The configuration for the test framework.</param>
		/// <returns>The server where the tests will be run.</returns>
		private MFilesServerApplication ConnectToServer( TestConfiguration testConfiguration )
		{
			// Collect needed configurations.
			ServerConfiguration serverConfiguration = testConfiguration.ServerConfiguration;
			UserCredentialConfiguration mfilesAdminCredentials = testConfiguration.Users.AdminUser.MFilesCredentials;

			// Collect info for logging.
			string fullServerAddress =
				$"{serverConfiguration.ServerNetworkAddress}:{serverConfiguration.ServerEndpoint}";

			// Connect to the server.
			Console.WriteLine( $"Connecting to the server at {fullServerAddress}" );
			var mfServer = new MFilesServerApplication();
			mfServer.ConnectAdministrative(
				AuthType: MFAuthType.MFAuthTypeSpecificMFilesUser,
				UserName: mfilesAdminCredentials.Username,
				Password: mfilesAdminCredentials.Password,
				NetworkAddress: serverConfiguration.ServerNetworkAddress,
				Endpoint: serverConfiguration.ServerEndpoint );

			// Configuring web acccess.
			mfServer.ServerManagementOperations.ConfigureWebAccessToDefaultWebSite();

			// Return the server.
			Console.WriteLine( $"Succesfully connected to the server at {fullServerAddress}" );
			return mfServer;
		}

		/// <summary>
		/// Install the connector.
		/// </summary>
		/// <param name="testConfiguration">The configuration for the test framework.</param>
		/// <returns>The id of the vault where the connector is installed.</returns>
		private Guid InstallConnector( TestConfiguration testConfiguration )
		{

			// Initialize the installer.
			Console.WriteLine( $"Installing the connector '{testConfiguration.ConnectorInstallationConfiguration.ConnectorName}' to {vaultName}." );
			var connectorInstaller = new ConnectorInstaller(
				testConfiguration.ConnectorInstallationConfiguration,
				testConfiguration.Users.AdminUser.ExternalCredentials,
				this._mfServer );

			// Install the connector.
			Guid vaultId = connectorInstaller.InstallConnector( testConfiguration );

			// Return the id of the connector.
			Console.WriteLine( $"Successfully installed the connector '{testConfiguration.ConnectorInstallationConfiguration.ConnectorName}' to {vaultName}." );
			return vaultId;
		}

		/// <summary>
		/// Authenticates the users used in the tests. 
		/// </summary>
		/// <remarks>If using multiple users in any tests make sure they are authenticated here.</remarks>
		/// <param name="vaultId">The id of the vault where the connector is installed.</param>
		/// <param name="testConfiguration">The configuration for the test framework.</param>
		public static void AuthenticateUsers(
			Guid vaultId,
			TestConfiguration testConfiguration,
			MFilesServerApplication mfServer )
		{
			// Create the authenticator.
			Console.WriteLine( "Authenticating users." );
			var authenticator = new Authenticator( mfServer );
			
			// Authenticating using common user.
			authenticator.AuthenticateSpecialUsers(
				vaultId,
				testConfiguration.Users.AdminUser.ExternalCredentials,
				MFExtensionAuthenticationSpecialUserType.MFExtensionAuthenticationSpecialUserTypeCommon );

			// Authenticating using indexed user. (STC 27-01-2018)
			authenticator.AuthenticateSpecialUsers(
				vaultId,
				testConfiguration.Users.User1.ExternalCredentials,
				MFExtensionAuthenticationSpecialUserType.MFExtensionAuthenticationSpecialUserTypeIndexer );

			// Authenticating using permission retreiver.
			authenticator.AuthenticateSpecialUsers(
				vaultId,
				testConfiguration.Users.User2.ExternalCredentials,
				MFExtensionAuthenticationSpecialUserType.MFExtensionAuthenticationSpecialUserTypePermissions );

			// Authenticate all standard users.
			authenticator.AuthenticateAllStandardUsers( vaultId, testConfiguration.Users );
			Console.WriteLine( "Successfully authenticated users." );
		}

		/// <summary>
		/// Restore the vault.
		/// </summary>
		private void RestoreVault( TestConfiguration testConfig )
		{
			// Try to restore the vault.
			Console.WriteLine( $"Restoring vault from {testConfig.ConnectorInstallationConfiguration.VaultBackupFilePath}." );
			try
			{
				// Set the vault properties.
				var vaultProperties = new VaultProperties();
				vaultProperties.DisplayName = vaultName;
				vaultProperties.MainDataFolder =
					$@"{testConfig.ConnectorInstallationConfiguration.MFilesVaultFolder}\{vaultName}";
				vaultProperties.VaultGUID = Guid.NewGuid().ToString();

				// Set the restore job.
				var restoreJob = new RestoreJob();
				restoreJob.BackupFileFull = testConfig.ConnectorInstallationConfiguration.VaultBackupFilePath;
				restoreJob.OverwriteExistingFiles = true;
				restoreJob.VaultProperties = vaultProperties;

				// See if the vault already exists.
				if( this._mfServer.GetOnlineVaults().GetVaultIndexByName( vaultName ) != -1 )
				{
					// If the vault already exists, then destroy it.
					this._mfServer.VaultManagementOperations.DestroyVault(
						this._mfServer.GetOnlineVaults().GetVaultByName( vaultName ).GUID );
				}

				// Restore the vault.
				this._mfServer.VaultManagementOperations.RestoreVault( restoreJob );
			}
			catch( Exception ex )
			{
				throw new Exception( "Failed to restore the vault.", ex );
			}
			Console.WriteLine( "Successfully restored vault." );
		}


		/// <summary>
		/// Create new document vault.
		/// </summary>
		/// <param name="testConfiguration">The configuration for the test framework.</param>
		private void CreateNewVault( TestConfiguration testConfiguration )
		{
			try
			{
				TestUsersConfiguration mfilesAdminCredentials = testConfiguration.Users;
				// Set the vault properties.
				var vaultProperties = new VaultProperties();
				vaultProperties.DisplayName = vaultName;
				vaultProperties.MainDataFolder = "C:\\Program Files\\M-Files\\Server Vaults\\" + vaultName;

				// See if the vault already exists.
				if( this._mfServer.GetOnlineVaults().GetVaultIndexByName( vaultName ) != -1 )
				{
					// If the vault already exists, then destroy it.
					this._mfServer.VaultManagementOperations.DestroyVault(
						this._mfServer.GetOnlineVaults().GetVaultByName( vaultName ).GUID );
				}

				// Create new vault.
				this._mfServer.VaultManagementOperations.CreateNewVault( vaultProperties );
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw new Exception( "Failed to create new vault.", ex );
			}
			Console.WriteLine( "Successfully created new vault." );
		}

		/// <summary>
		/// Add the user to the vault.
		/// </summary>
		/// <param name="testConfiguration">The configuration for the test framework.</param>
		private void UsersForVault( TestConfiguration testConfiguration )
		{
			try
			{
				// Get the object for windows user.
				WindowsIdentity windowsID = WindowsIdentity.GetCurrent();

				// Stores the current windows user name.
				string currentWindowsUser = windowsID.Name;

				// Adding current windows user to the vault.
				AddUserToVault( currentWindowsUser, true );

				// Adding m-files user to the vault.
				testConfiguration.Users.AllStandardUserConfigurations.ForEach( userstest =>
						 AddUserToVault( userstest.MFilesCredentials.Username, false ) );

				// Adding m-files admin user to the vault.
				AddUserToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username, true );

			}
			catch( Exception ex )
			{
				// Throws exception.
				throw new Exception( "Failed to add the users to new vault.", ex );
			}
			Console.WriteLine( "Successfully added the users to new vault." );
		}

		/// <summary>
		/// Add users to the vault with given permissions.
		/// </summary>
		/// <param name="userName">User to be added.</param>
		/// <param name="windowsUser">User is Admin/Normal user.</param>
		private void AddUserToVault( string userName,
				bool windowsUser )
		{
			try
			{
				// Object to store the login session of the vault.
				Vault userLoginSession;

				// Stores the existence of the user account.
				int accountExist = 0;

				// Object to store the properties of the user account.
				UserAccount userProperties = new UserAccount();

				// Assigns the permission for the user.
				if( windowsUser )
					userProperties.VaultRoles = MFUserAccountVaultRole.MFUserAccountVaultRoleFullControl;  // Admin user.
				else
					userProperties.VaultRoles = MFUserAccountVaultRole.MFUserAccountVaultRoleDefaultRoles;  // Normal user.

				// Properties of the user account.
				userProperties.LoginName = userName;
				userProperties.InternalUser = true;
				userProperties.Enabled = true;

				// See if the vault exists.
				if( this._mfServer.GetOnlineVaults().GetVaultIndexByName( vaultName ) != -1 )
				{
					// Login to the vault.
					userLoginSession = this._mfServer.LogInToVaultAdministrative(
						  this._mfServer.GetOnlineVaults().GetVaultByName( vaultName ).GUID );

					// Get the user account in the vault.
					UserAccounts userAccounts = userLoginSession.UserOperations.GetUserAccounts();

					// Verify the existence of the user account.
					foreach( UserAccount account in userAccounts )
					{
						// Modify the user account properties if it already exists.
						if( account.LoginName == userName )
						{
							// Set the user Id to the user properties.
							userProperties.ID = account.ID;

							// Modify the user account properties.
							userLoginSession.UserOperations.ModifyUserAccount( userProperties );

							// Increments the variable.
							accountExist++;
						}
					}

					// Create new user account in vault if it doesnot exists.
					if( accountExist == 0 )
						userLoginSession.UserOperations.AddUserAccount( userProperties );
				}
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		public void InstallCOnnLicense()
		{
			MFApplicationLicenseStatus tt = new MFApplicationLicenseStatus();

			//License
		}

		/// <summary>
		/// Add users to the server with given permissions.
		/// </summary>
		/// <param name="testConfiguration">The configuration for the test framework.</param>
		private void UsersForServer( TestConfiguration testConfiguration )
		{
			try
			{
				// Boolean to store the user is admin or not.
				bool AdminUser = true;

				// Adding m-files user to the server.
				testConfiguration.Users.AllStandardUserConfigurations.ForEach( userstest =>
						 AddLoginAccount( userstest.MFilesCredentials.Username, userstest.MFilesCredentials.Password, Environment.UserDomainName,
						 !AdminUser, MFLicenseType.MFLicenseTypeConcurrentUserLicense, false ) );


				// Adding Admin user to the server.
				AddLoginAccount( testConfiguration.Users.AdminUser.MFilesCredentials.Username, testConfiguration.Users.AdminUser.MFilesCredentials.Password,
					Environment.UserDomainName, AdminUser, MFLicenseType.MFLicenseTypeConcurrentUserLicense, false );

			}
			catch( Exception ex )
			{
				// Throws exception.
				throw new Exception( "Failed to add the users to server.", ex );
			}
			Console.WriteLine( "Successfully added the users to server." );
		}

		/// <summary>
		/// Assign the permissions and role of the user and add to the server.
		/// </summary>
		/// <param name="userName">Username to add.</param>
		/// <param name="passWord">Password for the user account.</param>
		/// <param name="domainName">Domain of the user.</param>
		/// <param name="AdminUser">User is Admin/Normal user.</param>
		/// <param name="licenseType">License type which should be assigned.</param>
		/// <param name="windowsUser">User is Windows/MFiles user.</param>
		private void AddLoginAccount( string userName,
				string passWord,
				string domainName,
				bool AdminUser,
				MFLicenseType licenseType,
				bool windowsUser )
		{
			try
			{
				// Existence of the account.
				bool accountExists = false;

				// Stores the instance of the MFilesServerApplication.
				var serverConnection = new MFilesServerApplication();

				// Object to store the login account information.
				LoginAccount userInformation = new LoginAccount();

				// Verify the user needs admin role.
				if( AdminUser )
					// Admin User.
					userInformation.ServerRoles = MFLoginServerRole.MFLoginServerRoleSystemAdministrator;
				else
					// Normal User.
					userInformation.ServerRoles = MFLoginServerRole.MFLoginServerRoleNone;

				// Verify the user needs admin role.
				if( windowsUser )
				{
					// Windows User.
					userInformation.DomainName = domainName;
					userInformation.AccountType = MFLoginAccountType.MFLoginAccountTypeWindows;
				}
				else
					// M-Files User.
					userInformation.AccountType = MFLoginAccountType.MFLoginAccountTypeMFiles;

				// Collects the information about the users.
				userInformation.UserName = userName;
				userInformation.LicenseType = licenseType;
				userInformation.Enabled = true;

				// Get all the existing login accounts from the server.
				LoginAccounts loginAccounts = this.GetAllLoginAccounts( serverConnection );

				// Verify whether the account already exists or not.
				foreach( LoginAccount acount in loginAccounts )
				{
					// Compares the user name.
					if( acount.UserName.Equals( userName ) )
					{						
						// Set the flag if the user exists.
						accountExists = true;
						break;
					}
				}

				// Add or update the login account based on the existence.
				if( accountExists )
				{
					// Account exists.

					// Modify the user roles and assign the password.
					serverConnection.LoginAccountOperations.ModifyLoginAccount( userInformation );

					// If the user is m-files user then update the password.
					if( !windowsUser )
						serverConnection.LoginAccountOperations.UpdateLoginPassword(
							userName, passWord );
				}
				else
				{
					// Account does not exists.

					if( windowsUser )
						// Add new windows login account.
						serverConnection.LoginAccountOperations.AddLoginAccount( userInformation );
					else
						// Add new m-files login account.
						serverConnection.LoginAccountOperations.AddLoginAccount( userInformation, passWord );
				}

			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Get all the existing login account from the server.
		/// </summary>
		/// <param name="serverConnection">Instance of the server connection.</param>
		/// <param name="userConfiguration">The configuration of the users.</param>
		/// <returns>The available login accounts.</returns>
		private LoginAccounts GetAllLoginAccounts( MFilesServerApplication serverConnection )
		{
			try
			{
				// Connects to the server.
				serverConnection.ConnectAdministrative();

				// Get all the available login accounts from the server.
				LoginAccounts loginAccounts = serverConnection.LoginAccountOperations.GetLoginAccounts();

				// Return the login accounts.
				return loginAccounts;
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Assign new password to the windows user.
		/// </summary>
		/// <param name="addNewWindowsUser">User name to be added in system.</param>
		/// <returns>The password of newly added windows user.</returns>
		private string AssignAndGetWindowsUser( string addNewWindowsUser )
		{
			try
			{
				// Generate a password for the new user.
				var passWord = Membership.GeneratePassword( 16, 4 );

				// Fetch all the users and usergroups in machine.
				DirectoryEntry systemUsers = new DirectoryEntry( "WinNT://" + Environment.MachineName + ",computer" );

				// Create new object to store the new user details.
				DirectoryEntry newUser = systemUsers.Children.Find( addNewWindowsUser );

				// Set the password for the new user.
				newUser.Invoke( "SetPassword", new object[] { passWord } );

				// Save the user details.
				newUser.CommitChanges();

				Console.WriteLine( "New windows user has been added: " + addNewWindowsUser );

				// return the passWord assigned for new user.
				return passWord;
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error while adding new windows user " + ex.Message );
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Add the vault connection.
		/// </summary>
		public void CreateVaultConnection()
		{
			try
			{
				// Get the vault on server.
				VaultOnServer getVault = this._mfServer.GetOnlineVaults().GetVaultByName( vaultName );

				// Create new instance to set the properties of vault connection.
				VaultConnection vaultconnection = new VaultConnection();

				// Set the proeprties of the vault connection.
				vaultconnection.AuthType = MFAuthType.MFAuthTypeLoggedOnWindowsUser;
				vaultconnection.Name = vaultName;
				vaultconnection.ServerVaultGUID = getVault.GUID;
				vaultconnection.ServerVaultName = vaultName;
				vaultconnection.UserSpecific = false;
				vaultconnection.Endpoint = "MFServerCommon_F5EE352D-6A03-4866-9988-C69CEA2C39BF";
				vaultconnection.ProtocolSequence = "ncalrpc";

				// Get the available vault connections.
				MFilesClientApplication mfClient = new MFilesClientApplication();
				VaultConnections getVaultConnections = mfClient.GetVaultConnections();

				// Verify whether same vault connection is available or not.
				foreach( VaultConnection vaultConn in getVaultConnections )
				{
					// If available removes the vault connection.
					if( vaultConn.Name == vaultName )
						mfClient.RemoveVaultConnection( vaultName, false );
				}

				// Add vault connection.
				mfClient.AddVaultConnection( vaultconnection );
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Error while aading the vault connection " + ex.Message );
			}
		}

		/// <summary>
		/// Fetch the vault application license.
		/// </summary>
		/// <param name="vaultId">Vault GUID.</param>
		/// <param name="testConfiguration">Test configuration.</param>
		/// <returns>The version of the vault application.</returns>
		public string getVaultApplicationLicense( Guid vaultId, 
			ConnectorInstallationConfiguration connectorConfiguration )
		{
			try
			{
				// The GUID has to be in curly brackets. Otherwise it is seen as an invalid GUID by the server.
				string compatibleVaultGuid = "{" + vaultId + "}";

				// Log in to the vault as an administrator.
				Vault vaultConnection = this._mfServer.LogInToVaultAdministrative( compatibleVaultGuid );

				// Get the application version.
				CustomApplication application =
					vaultConnection.CustomApplicationManagementOperations.GetCustomApplication(
							connectorConfiguration.ApplicationGuid );

				// Return the application version.
				return application.Version;
			}
			catch(Exception ex)
			{
				Console.WriteLine( "Exception while fetching the connector version " + ex.Message );
				throw ex;
			}
		}
	}
}
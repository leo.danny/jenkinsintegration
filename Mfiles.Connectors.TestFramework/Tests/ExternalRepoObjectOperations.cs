﻿using Mfiles.Connectors.TestFramework.Configuration;
using MFaaP.MFWSClient;
using NUnit.Framework;
using MFNUnit.Utils.Logger;
using System;
using MFNUnit.Utils.TestData;
using System.Data;
using System.Collections.Generic;

namespace Mfiles.Connectors.TestFramework.Tests
{
	[TestFixture]
	[Order( 1 )]
	class ExternalRepoObjectOperations : TestBaseClass
	{
		/// <summary>
		/// Represents an object version.
		/// </summary>
		protected ObjVer externalObjVer;

		/// <summary>
		/// Stores the path of the object.
		/// </summary>
		private string objectPath = null;
		
		/// <summary>
		/// Variable to store testdata count and print in extent report.
		/// </summary>
		public int testDataCount;

		/// <summary>
		/// Instance of the MFilesApiHelperMethods class.
		/// </summary>
		public MFilesApiHelperMethods mfilesApi;

		/// <summary>
		/// Loggedin instance on MFClient.
		/// </summary>
		MFilesAPI.Vault mfClientVault;

		/// <summary>
		/// Load test configuration.
		/// </summary>
		TestConfiguration testConfiguration;

		[ SetUp]
		public void SetUp()
		{
			// Store testdata count and print in extent report
			testDataCount = 1;

			// Starts logging the extent report.
			Report.StartTest( Setup.connectorName );

			// Load test configuration.
			testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();
			
			// Creates new object reference for the class MFilesApiHelperMethods.
			mfilesApi = new MFilesApiHelperMethods();

			// Login using specified user.
			mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
				testConfiguration.Users.User2.MFilesCredentials.Password );
		}

		[TearDown]
		public void TearDown()
		{
			// Add the status and information into the extent report.
			Report.Flush();
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroy the promoted object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DestroyPromotedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Expected error message.
					string expectedMessage =
							"No object with the name " + objectName + " exists.";

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Login using admin user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectName );

					// Delete the object.
					mfilesApi.vaultConnection.ObjectOperations.DestroyObject( objVersionAndProperties.ObjVer.ObjID, true, -1 );

					try
					{
						// Get the object.
						this.GetObjectVersion( objectName, objectPath );
					}
					catch( Exception exception )
					{
						//Verifies the error thrown is expected.
						if( exception.Message == expectedMessage )
						{
							Report.LogPass( "Test Passed: " +
								"Promoted Object " + objectName + " has been destroyed from repository." );
							continue;
						}
						else
							throw exception;
					}

					// Fails the testcase if object is not deleted.
					Assert.Fail( "Test Failed: Promoted Object " + objectName + " is not destroyed from external repository." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroy the unmanaged object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DestroyUnmanagedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Expected error message.
					string expectedMessage =
							"No object with the name " + objectName + " exists.";

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object " + objectVersion.Title );

					// Get the object details.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Delete the object.
					mfilesApi.vaultConnection.ObjectOperations.DestroyObject( mfilesObjID, true, -1 );

					try
					{
						// Get the object.
						this.GetObjectVersion( objectName, objectPath );
					}
					catch( Exception exception )
					{
						//Verifies the error thrown is expected.
						if( exception.Message == expectedMessage )
						{
							Report.LogPass( "Test Passed: " +
								"Unmanaged Object " + objectName + " has been destroyed from repository." );
							continue;
						}
						else
							throw exception;
					}

					// Fails the testcase if object is not deleted.
					Assert.Fail( "Test Failed: Unmanaged Object " + objectName + " is not destroyed from external repository." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Add comment to a promoted object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_AddCommentToPromotedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					string commentValue = returnTuple.Item1.GetValueForColumn( "Comment" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectVersion.Title );

					// Add the comment to the promoted object.
					objVersionAndProperties =
						mfilesApi.SetCommentToExternalObject(
							mfilesApi, objVersionAndProperties.ObjVer, MFilesAPI.MFDataType.MFDatatypeMultiLineText, commentValue );

					Report.LogInfo( "Step 3: Added the comment " + commentValue + " to the promoted object " + objectVersion.Title );

					// Get the comment for the latest version.
					MFilesAPI.VersionComment versionComment =
						mfilesApi.vaultConnection.ObjectPropertyOperations.GetVersionComment( objVersionAndProperties.ObjVer );

					// Verify whether the comment has been added to the promoted object.
					if( versionComment.VersionComment.Value.DisplayValue == commentValue )
						Report.LogPass( "Test Passed: Comment can be added to the promoted object." );
					else
						Assert.Fail( "Test Failed: Error while adding comments to promoted object" );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Add comment to unmanaged object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_AddCommentToUnmanagedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					string commentValue = returnTuple.Item1.GetValueForColumn( "Comment" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Expected error message.
					string expectedMessage =
							"This operation is not available for external repository objects that do not have M-Files metadata";

					// Get the object version.
					MFilesAPI.ObjVer objVer = mfilesApi.GetLatestObjVer( externalObjVer );

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object " + objectVersion.Title );

					try
					{
						// Add the comment to unmanaged object.
						MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
							mfilesApi.SetCommentToExternalObject(
								mfilesApi, objVer, MFilesAPI.MFDataType.MFDatatypeMultiLineText, commentValue );
					}
					catch( Exception exception )
					{
						// Verifies the exception message.
						if( exception.Message.Contains( expectedMessage ) )
						{
							Report.LogPass( "Test Passed: Comments cannot be added to the unmanaged object." );
							continue;
						}
						else
							Report.LogError( "The error message has changed as " + exception.Message );
					}

					// Fails the test case if comments can be added to unmanaged object.
					Assert.Fail( "Test Failed: Comments can be added to the unmanaged object." );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Perform rollback operation to a promoted object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_RollbackWithPromotedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					string commentValue = returnTuple.Item1.GetValueForColumn( "Comment" );

					// Expected error message.
					string expectedMessage =
							"This operation is not allowed for documents that use external linked files";

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectVersion.Title );

					// Add the comment to the promoted object.
					objVersionAndProperties =
						mfilesApi.SetCommentToExternalObject(
							mfilesApi, objVersionAndProperties.ObjVer, MFilesAPI.MFDataType.MFDatatypeMultiLineText, commentValue );

					Report.LogInfo( "Step 3: Added the comment " + commentValue + " to the promoted object " + objectVersion.Title );

					// Verify whether the comment has been added to the promoted object.
					if( objVersionAndProperties.ObjVer.Version == 2 )
						Report.LogInfo( "Step 4: Comment has been added to the Promoted object." );

					try
					{
						// Rollback to the older version.
						mfilesApi.vaultConnection.ObjectOperations.Rollback( objVersionAndProperties.ObjVer.ObjID, 1 );
					}
					catch( Exception exception )
					{
						// Verify the error message.
						if( exception.Message.Contains( expectedMessage ) )
						{
							Report.LogPass( "Test Passed: Rollback operation cannot be performed for external objects." );
							continue;
						}
						else
							Report.LogError( "The error message has changed as " + exception.Message );
					}

					// Fails the test case if rollback can be done for external objects.
					Assert.Fail( "Test Failed: Rollback operation is successfull for promoted object" );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Add a property to the metadatcard of promoted object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_AddPropertyToPromotedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					string propertyValue = returnTuple.Item1.GetValueForColumn( "PropertyValue" );

					int propertyDefID = Int32.Parse( returnTuple.Item1.GetValueForColumn( "PropertyDefID" ) );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectVersion.Title );

					// Check out the promoted object.
					MFilesAPI.ObjectVersion mfilesApiObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );

					// Add the property to the promoted object.
					objVersionAndProperties =
						mfilesApi.SetPropertyToExternalObject(
							mfilesApi, mfilesApiObjVersion.ObjVer, MFilesAPI.MFDataType.MFDatatypeText, propertyDefID, propertyValue );

					// Check In the promoted object.
					mfilesApiObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( objVersionAndProperties.ObjVer );

					Report.LogInfo( "Step 3: Added a new property to the metadata of promoted object." );

					// Get all the properties of the object.
					MFilesAPI.PropertyValues propertyCollection =
						mfilesApi.vaultConnection.ObjectPropertyOperations.GetProperties( mfilesApiObjVersion.ObjVer, true );

					// Fetch the specified property from the proeprty collection.
					MFilesAPI.PropertyValue propertyDef = propertyCollection.SearchForProperty( propertyDefID );

					// Verify whether the Property has been added to the promoted object.
					if( propertyDef.Value.Value == propertyValue )
						Report.LogPass( "Test Passed: Property is added to the metadatacard of promoted object." );
					else
						Assert.Fail( "Test Failed: Error while adding property to the metadatacard of promoted object." );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Add a property to the metadatcard of Unmanaged object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_AddPropertyToUnmanagedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					string propertyValue = returnTuple.Item1.GetValueForColumn( "PropertyValue" );

					int propertyDefID = Int32.Parse( returnTuple.Item1.GetValueForColumn( "PropertyDefID" ) );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Expected message.
					string expectedMessage = "not";

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Get the object.
					MFilesAPI.ObjVer objVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Check out the promoted object.
					MFilesAPI.ObjectVersion mfilesApiObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVer.ObjID );

					try
					{
						// Add the property to the promoted object.
						MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
							mfilesApi.SetPropertyToExternalObject(
								mfilesApi, mfilesApiObjVersion.ObjVer, MFilesAPI.MFDataType.MFDatatypeText, propertyDefID, propertyValue );

						// Check In the promoted object.
						mfilesApiObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.CheckIn( objVersionAndProperties.ObjVer );
					}
					catch( Exception exception )
					{
						// Check In the object.
						mfilesApiObjVersion = mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjVersion.ObjVer );

						// Verify the error message.
						if( exception.Message.ToLower().Contains( expectedMessage ) && !mfilesApiObjVersion.ObjectCheckedOut )
						{
							Report.LogPass( "Test Passed: Properties cannot be added to metadata of unmamaged object." );
							continue;
						}
						else
							Report.LogError( "The error message has changed as " + exception.Message );
					}

					// Fails the test case if properties can be added to unmanaged object.
					Assert.Fail( "Test Failed: Properties can be added to metadata of unmamaged object." );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroy multiple objects." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DestroyMultipleObjects()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					int count = 0;
					string[] objectNames = new string[ 2 ];

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the objects in the subfolder.
					FolderContentItems folderItems = this.GetSubfolderContents( objectPath );

					// Object to store details of all objects.
					MFilesAPI.ObjIDs mfilesObjIDs = new MFilesAPI.ObjIDs();

					// Login using admin user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Stores the ObjIDs of multiple objects.
					foreach( FolderContentItem item in folderItems.Items )
					{
						if( !item.ObjectVersion.ObjectCheckedOut )
						{
							// Get the object ID.
							MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( item.ObjectVersion.ObjVer );

							// Stores the object name.
							objectNames[ mfilesObjIDs.Count ] = item.ObjectVersion.Files[ 0 ].EscapedName;

							// Stores the object details.
							mfilesObjIDs.Add( mfilesObjIDs.Count, mfilesObjID );

							// Verifies the number of selected object.
							if( mfilesObjIDs.Count >= 2 )
								break;
						}
					}

					Report.LogInfo( "Step 1: Fetched the object version of multiple objects." );

					// Destory the selected objects.
					mfilesApi.vaultConnection.ObjectOperations.DestroyObjects( mfilesObjIDs );

					Report.LogInfo( "Step 2: Destroyed multiple objects." );

					// Verify the availability of deleted objects.
					foreach( string objectName in objectNames )
					{
						try
						{
							// Get the object.
							this.GetObjectVersion( objectName, objectPath );
						}
						catch( Exception exception )
						{
							//Verifies the error thrown is expected.
							if( exception.Message == "No object with the name " + objectName + " exists." )
								count++;
							else
								throw exception;
						}
					}

					// Verifies the objects are deleted from mfiles.
					if( count == objectNames.Length )
						Report.LogPass( "Test Passed: Multiple objects can be destroyed from external repository." );
					else
						Report.LogFail( "Test Failed: Multiple objects cannot be destroyed from external repository." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Marking promoted object for offline availability." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_MarkPromotedObjectForOffline()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectVersion.Title );

					// Login to MFClient.
					MFClientApiHelperMethods mfClient = new MFClientApiHelperMethods();
					mfClientVault = mfClient.LoginToMFClient( MFilesAPI.MFAuthType.MFAuthTypeSpecificMFilesUser );

					// Get the properties of the view.
					MFilesAPI.FolderDefs viewProperties =
						mfClient.GetViewProperties( MFilesAPI.MFBuiltInView.MFBuiltInViewOfflineMarkedForOfflineAvailability );

					// Set the promoted object for offline availability.
					mfClientVault.ObjectOperations.SetOfflineAvailability( objVersionAndProperties.ObjVer.ObjID, true );

					// Verify the object is marked for offline availability.
					if( mfClientVault.ObjectOperations.GetOfflineAvailability( objVersionAndProperties.ObjVer.ObjID ) )
						Report.LogPass( "Test Passed: Promoted object can be set for offline availability." );
					else
						Assert.Fail( "Test Failed: Promoted object cannot be set for offline availability." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Logout from MFClient.
					mfClientVault.LogOutWithDialogs( IntPtr.Zero );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Marking unmanaged object for offline availability." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_MarkUnManagedObjectForOffline()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					string expectedMessage =
						"This operation is not available for external repository objects that do not have M-Files metadata";

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjID objID =
						mfilesApi.GetObjID( externalObjVer );

					// Login to MFClient.
					MFClientApiHelperMethods mfClient = new MFClientApiHelperMethods();
					mfClientVault = mfClient.LoginToMFClient( MFilesAPI.MFAuthType.MFAuthTypeSpecificMFilesUser );

					try
					{
						// Set the object for offline availability.
						mfClientVault.ObjectOperations.SetOfflineAvailability( objID, true );
					}
					catch( Exception exception )
					{

						// Verify the error message.
						if( exception.Message.Contains( expectedMessage ) )
						{
							Report.LogPass( "Test Passed: Unmamaged object cannot be set for offline availability." );
							continue;
						}
						else
							Report.LogError( "The error message has changed as " + exception.Message );
					}

					// Fails the test case if unmanaged object can be set for offline.
					Assert.Fail( "Test Failed: Unmamaged object can be set for offline availability." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Logout from MFClient.
					mfClientVault.LogOutWithDialogs( IntPtr.Zero  );
				}
			}
		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeDocumentum" ), Category( "ExcludeOneDriveForBusiness" )]
		[Description( "Renaming the object with special characters #%*:>?/." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_RenamingWithSpecialCharacters()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					string expectedMessage = "character";

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					string newName =
						objectVersion.Title + returnTuple.Item1.GetValueForColumn( "SpecialChar" );

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Object to store the details of the file.
					MFilesAPI.FileVer fileVersion = mfilesApi.GetFileVer( objectVersion );

					// Get the object ID.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( mfilesObjID );

					// Checkout the object.
					MFilesAPI.ObjectVersion mfilesObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					Report.LogInfo( "Step 2: Checked out the external object " + objectVersion.Title );

					try
					{
						// Change the name of the object.
						mfilesObjVersion = mfilesApi.vaultConnection.ObjectFileOperations.RenameFile
							( mfilesObjVersion.ObjVer, fileVersion, newName, objectVersion.Files[ 0 ].Extension );

						// Try to check in the object.
						mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVersion.ObjVer );
					}
					catch( Exception exception )
					{
						// Undo checkout to revert the changes.
						mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjVersion.ObjVer );

						// Verify the error message.
						if( exception.Message.Contains( expectedMessage ) )
						{
							Report.LogPass(
								"Test Passed: Special characters #%*:<>?/ should not a part of object title " + exception.Message );
							continue;
						}
						else
							Assert.Fail( "Test Failed: The error message has changed as " + exception.Message );
					}

					// Verifies the objects are renamed.
					if( mfilesObjVersion.Title == newName && !Setup.connectorName.Contains( "Sharepoint" ) )
						Report.LogPass(
							"Test Passed: Special characters #%*:<>?/ can be added in the title of the object " + mfilesObjVersion.Title );
					else
						Assert.Fail( "Test Failed: Special characters #%*:<>?/ cannot be added in the title of the object." );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Converting unmanaged SFD object to MFD." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_ConvertUnmanagedSFDToMFD()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					string expectedMessage = "not";

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Get the MFilesApi ObjVer of the selected object.
					MFilesAPI.ObjVer mfilesObjVer =  mfilesApi.GetLatestObjVer( externalObjVer );

					// Checkout the unmanaged object.
					MFilesAPI.ObjectVersion  mfilesObjectVersion = 
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjVer.ObjID );

					Report.LogInfo( "Step 2: Checked out the selected unmanaged object " + mfilesObjectVersion.Title );

					try
					{
						// Convert the SFD to MFD object.
						mfilesApi.vaultConnection.ObjectOperations.SetSingleFileObject( mfilesObjectVersion.ObjVer, false );

						// Checkin the object.
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjectVersion.ObjVer );
					}
					catch(Exception ex)
					{
						// Undocheckout the checked in object.
						mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjectVersion.ObjVer );

						if( ex.Message.ToLower().Contains( expectedMessage.ToLower() ) )
						{
							Report.LogPass( "Test Passed: Unmanaged SFD object cannot be converted to MFD document" );
							continue;
						}
						else
							Assert.Fail( "Test Failed: The error message has changed as " + ex.Message );
					}

					// Fails the test case if unmanaged SFD object is converted to MFD.
					Assert.Fail( "Test Failed: Unmanaged SFD object is converted to MFD document" );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" ), Category( "Bug" )]
		[Description( "Converting Promoted SFD object to MFD." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_ConvertPromotedSFDToMFD()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					string expectedMessage = "not found";

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the selected unmanaged object " + objectVersion.Title);

					// Checkout the promoted object.
					MFilesAPI.ObjectVersion mfilesObjectVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );

					try
					{
						// Convert the SFD to MFD object.
						mfilesApi.vaultConnection.ObjectOperations.SetSingleFileObject( mfilesObjectVersion.ObjVer, false );

						// Checkin the object.
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjectVersion.ObjVer );
					}
					catch( Exception ex )
					{
						// Undocheckout the checked in object.
						mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjectVersion.ObjVer );

						if( ex.Message.ToLower().Contains( expectedMessage ) )
						{
							Report.LogPass( "Test Passed: Promoted SFD object cannot be converted to MFD document" );
							continue;
						}
						else
							Assert.Fail( "Test Failed: The error message has changed as " + ex.Message );
					}

					// Fails the test case if unmanaged SFD object is converted to MFD.
					Assert.Fail( "Test Failed: Promoted SFD object is converted to MFD document" );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Promoting external object using read only user." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_PromoteObjectUsingReadOnlyUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the user credentials to be created.
					UserCredentialConfiguration userCredentials = new UserCredentialConfiguration();
					userCredentials.Username = returnTuple.Item1.GetValueForColumn( "User" );
					userCredentials.Password = returnTuple.Item1.GetValueForColumn( "Password" );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Create/Update login account.
					mfilesApi.AddLoginAccount( userCredentials, false, MFilesAPI.MFLicenseType.MFLicenseTypeReadOnlyLicense );

					// Below commented code will add the user to the vault if empty vault is created. 
					mfilesApi.AddUserToVault( returnTuple.Item1.GetValueForColumn( "User" ), false );

					// Login using specified user.
					mfilesApi.LoginToVault( userCredentials.Username,
						userCredentials.Password );

					Report.LogInfo( "Step 2: Logged in with user having readonly license." );
					
					try
					{
						// Promote the object using read only user.
						MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
							mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );
						
					}
					catch( Exception ex )
					{
						// Verifies the error message.
						if( ex.Message.ToLower().Contains( "not" ) )
						{
							Report.LogPass( "Test Passed: User with read only user cannot promote the unmanaged object." + ex.Message );
							continue;
						}
						else
							Assert.Fail( "Test Failed: The error message has changed as " + ex.Message );
					}

					// Fails the test case if User with read only user can promote the unmanaged object.
					Assert.Fail( "Test Failed: User with read only user can promote the unmanaged object." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}

				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Marking Promoted/Unmanaged object for offline availability." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_MarkExternalObjectForArchiving()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties;
					MFilesAPI.ObjID objId;

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					if( returnTuple.Item1.GetValueForColumn( "Promote" ) == "Yes" )
					{
						// Promote the object.
						objVersionAndProperties =
							mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

						objId = objVersionAndProperties.ObjVer.ObjID;

						Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectVersion.Title );
					}
					else
						objId = mfilesApi.GetObjID( externalObjVer );

					try
					{
						objVersionAndProperties = mfilesApi.vaultConnection.ObjectPropertyOperations.MarkForArchiving( objId );
					}
					catch(Exception ex)
					{
						// Verifies the exception message.
						if( ex.Message.ToLower().Contains( returnTuple.Item1.GetValueForColumn( "Message" ) ) )
						{
							Report.LogPass( "Test Passed: External object cannot be marked for archiving." );
							continue;
						}
						else
							Report.LogError( "The error message has changed as " + ex.Message );
					}

					// Fails the test case if marked for archiving can be done for external object.
					Assert.Fail( "Test Failed: External object can be marked for archiving." );


				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Demote the object when it is in checked out state." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DemotingExternalObjectWhenCheckedOut()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the version of the external object." );

					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the external object " + objectName );

					// Verifies whether the object is promoted.
					if( !objVersionAndProperties.VersionData.Class.ToString().Equals( returnTuple.Item1.GetValueForColumn( "ClassID" ) ) )
						Assert.Fail( "Test Failed.Promoting the object is failed." );

					// Checkout the promoted object.
					MFilesAPI.ObjectVersion mfilesObjVersion = 
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );

					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					try
					{
						// Destroy the metadatacard of the promoted object.
						mfilesApi.DemoteObjects( externalObjVer );

						// Checkin the object.
						mfilesObjVersion = mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVersion.ObjVer );
					}
					catch(Exception ex)
					{
						mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjVersion.ObjVer );

						// Verifies the exception message.
						if( ex.Message.ToLower().Contains( returnTuple.Item1.GetValueForColumn( "Message" ) ) )
						{
							Report.LogPass( "Test Passed: Demoting cannot be done for promoted object when it is in checked out state." );
							continue;
						}
						else
							Report.LogError( "The error message has changed as " + ex.Message );
					}

					// Fails the test case if checked out object can be demoted.
					Assert.Fail( "Test Failed: Demoting can be done for promoted object when it is in checked out state." );
				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Verify the repository name in external objects metadata." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_VerifyingRepositoryPropertyOfExternalObject()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the root repository name.
					string repositoryName = testConfiguration.RepositoryName;

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object full name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the version of the external object." );

					// Promote the objects based on the testdata.
					if( returnTuple.Item1.GetValueForColumn( "Promote" ) == "Yes" )
					{
						// Promote the object.
						MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
							mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

						objectVersion = this.GetObjectVersion( objectName, objectPath );

						Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectVersion.Title );
					}
					
					// Verifies the Repository property value.
					if( objectVersion.BaseProperties.Exists(x => x.PropertyDef == 102) && 
						objectVersion.BaseProperties.Exists( x => x.TypedValue.DisplayValue.Equals( repositoryName ) ) )
						Report.LogPass( "Test Passed. Repository name in metadata of external object is " + 
							objectVersion.BaseProperties[0].TypedValue.DisplayValue );
					else
						Assert.Fail( "Test Failed. Repository name in metadata of external object is " +
							objectVersion.BaseProperties[ 0 ].TypedValue.DisplayValue );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Adding multiple properties to the metadata of promoted object." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_AddingMultiplePropertiesToPromotedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectVersion.Title );

					// Check out the promoted object.
					MFilesAPI.ObjectVersion mfilesApiObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );
					
					// Add the property to the promoted object.
					objVersionAndProperties =
						mfilesApi.SetPropertyToExternalObject(
							mfilesApi, mfilesApiObjVersion.ObjVer, MFilesAPI.MFDataType.MFDatatypeText,
							Int32.Parse( returnTuple.Item1.GetValueForColumn( "PropertyDefID1" ) ),
							returnTuple.Item1.GetValueForColumn( "PropertyValue1" ) );

					// Add the property to the promoted object.
					objVersionAndProperties =
						mfilesApi.SetPropertyToExternalObject(
							mfilesApi, mfilesApiObjVersion.ObjVer, MFilesAPI.MFDataType.MFDatatypeBoolean,
							Int32.Parse( returnTuple.Item1.GetValueForColumn( "PropertyDefID2" ) ), "true" );

					//// Add the property to the promoted object.
					//objVersionAndProperties =
					//	mfilesApi.SetPropertyToExternalObject(
					//		mfilesApi, mfilesApiObjVersion.ObjVer, MFilesAPI.MFDataType.MFDatatypeLookup,
					//		Int32.Parse( returnTuple.Item1.GetValueForColumn( "PropertyDefID2" ) ),
					//		returnTuple.Item1.GetValueForColumn( "PropertyValue2" ) );
					
					// Check In the promoted object.
					mfilesApiObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( objVersionAndProperties.ObjVer );

					Report.LogInfo( "Step 3: Added multiple properties to the metadata of promoted object." );

					// Get all the properties of the object.
					MFilesAPI.PropertyValues propertyCollection =
						mfilesApi.vaultConnection.ObjectPropertyOperations.GetProperties( mfilesApiObjVersion.ObjVer, true );

					// Fetch the specified property from the property collection.
					MFilesAPI.PropertyValue propertyDef1 = propertyCollection.SearchForProperty( 
										Int32.Parse( returnTuple.Item1.GetValueForColumn( "PropertyDefID1" ) ) );

					// Fetch the specified property from the property collection.
					MFilesAPI.PropertyValue propertyDef2 = propertyCollection.SearchForProperty(
										Int32.Parse( returnTuple.Item1.GetValueForColumn( "PropertyDefID2" ) ) );

					// Verify whether the Property has been added to the promoted object.
					if( propertyDef1.Value.Value == returnTuple.Item1.GetValueForColumn( "PropertyValue1" ) &&
						propertyDef2.Value.Value == true )
						Report.LogPass( "Test Passed: Multiple proerties is added to the metadatacard of promoted object." );
					else
						Assert.Fail( "Test Failed: Error while adding multiple properties to the metadatacard of promoted object." );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroying metadata of multiple promoted objects at the same time." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DemoteMultipleObjects()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					int count = 0;

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Stores the ObjIDs.
					MFilesAPI.ObjIDs promotedObjIDs = new MFilesAPI.ObjIDs();

					for( int index=0; index <3; index++ )
					{
						// Get the object.
						ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
						externalObjVer = objectVersion.ObjVer;
						
						// Promote the object.
						MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
							mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

						if( objVersionAndProperties.VersionData.Class == Int32.Parse( returnTuple.Item1.GetValueForColumn( "ClassID" )) )
							// Stores the ObjIDs.
							promotedObjIDs.Add( index, objVersionAndProperties.ObjVer.ObjID );
					}

					Report.LogInfo( "Step 1: Promoted more than one object and stores the ObjID of each object.");

					// Demote multiple objects at once.
					MFilesAPI.ObjectVersionAndPropertiesOfMultipleObjects demotedObjects =
						mfilesApi.vaultConnection.ExternalObjectOperations.DemoteObjects( promotedObjIDs );
					
					// Verify whether the metadta of selected objects are destroyed.
					foreach( MFilesAPI.ObjectVersionAndProperties demotedObject in demotedObjects )
					{
						// Verify the class of the object and increments the counter.
						if( demotedObject.VersionData.Class == -107 )
							count++;
					}

					if( count == 3 )
						Report.LogPass( "Test Passed: Multiple objects can be demoted at once." );
					else
						Assert.Fail( "Test Failed: Error while demoting multiple objects at once." );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Demoting an external object with standard user." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DemoteObjectWithStandardUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
					
					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );
					
					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectName );

					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					try
					{
						// Demote the object.
						mfilesApi.DemoteObjects( externalObjVer );
					}
					catch( Exception exception )
					{
						// Verifies the error thrown is as expected.
						if( exception.Message.Contains( returnTuple.Item1.GetValueForColumn( "ErrorMessage" ) ) )
							Report.LogInfo( "Demoting the object with standard user throws errror " + exception.Message );
						else
							Report.LogError("Error message is different while demoting the object with standard user " + exception.Message );
					}

					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );

					// Verifies whether the object has been demoted or not.
					if( objectVersion.Class != -107 )
						Report.LogPass( "Test Passed: Demoting the object " + objectName + " with standard user is not possible." );
					else
						// Fails the testcase if object is demoted.
						Assert.Fail( "Test Failed: Demoting Object " + objectName + " with standard user is possible." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroying unmanaged object with standard user." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DestroyUnmanagedObjectWithStandardUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Get the mfiles ObjID.
					MFilesAPI.ObjID mfilesApiObjID =  mfilesApi.GetObjID( externalObjVer );

					try
					{
						// Destroy the object.
						mfilesApi.vaultConnection.ObjectOperations.DestroyObject( mfilesApiObjID, true, -1 );
					}
					catch( Exception exception )
					{
						// Verifies the error thrown is as expected.
						if( exception.Message.Contains( returnTuple.Item1.GetValueForColumn( "ErrorMessage" ) ) )
							Report.LogInfo( "Destroying unmanaged object with standard user throws errror " + exception.Message );
						else
							Report.LogError( "Error message is different while destroying unmanaged object with standard user " + exception.Message );
					}

					// Get the object.
					ObjectVersion objectVersionAfterDestroying = GetObjectVersion( objectName, objectPath );

					// Verifies whether the object has been destroyed or not.
					if( objectVersion.EscapedTitleWithID == objectVersionAfterDestroying.EscapedTitleWithID )
						Report.LogPass( "Test Passed: Destroying unmanaged object " + objectName + " with standard user is not possible." );
					else
						// Fails the testcase if object is destroyed.
						Assert.Fail( "Test Failed: Destroying unmanaged Object " + objectName + " with standard user is possible." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroying promoted object with standard user." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DestroyPromotedObjectWithStandardUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectName );

					try
					{
						// Destroy the object.
						mfilesApi.vaultConnection.ObjectOperations.DestroyObject( objVersionAndProperties.ObjVer.ObjID, true, -1 );
					}
					catch( Exception exception )
					{
						// Verifies the error thrown is as expected.
						if( exception.Message.Contains( returnTuple.Item1.GetValueForColumn( "ErrorMessage" ) ) )
							Report.LogInfo( "Destroying promoted object with standard user throws errror " + exception.Message );
						else
							Report.LogError( "Error message is different while destroying promoted object with standard user " + exception.Message );
					}

					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );

					// Verifies whether the object has been destroyed or not.
					if( objectVersion != null )
						Report.LogPass( "Test Passed: Destroying promoted object " + objectName + " with standard user is not possible." );
					else
						// Fails the testcase if object is destroyed.
						Assert.Fail( "Test Failed: Destroying promoted Object " + objectName + " with standard user is possible." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Editing object which was promoted by another user." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_EditObjectPromotedByOtherUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					string commentValue = returnTuple.Item1.GetValueForColumn( "Comment" );

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Login using admin user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: With "+ testConfiguration.Users.AdminUser.MFilesCredentials.Username 
									+ " promoted the unmanaged object " + objectName );

					// Login using standard user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );

					// Add the comment to the promoted object.
					objVersionAndProperties =
						mfilesApi.SetCommentToExternalObject(
							mfilesApi, objVersionAndProperties.ObjVer, MFilesAPI.MFDataType.MFDatatypeMultiLineText, commentValue );

					Report.LogInfo( "Step 3: With "+ testConfiguration.Users.User2.MFilesCredentials.Username + " added the comment " 
									+ commentValue + " to the promoted object " + objectVersion.Title );

					// Get the comment for the latest version.
					MFilesAPI.VersionComment versionComment =
						mfilesApi.vaultConnection.ObjectPropertyOperations.GetVersionComment( objVersionAndProperties.ObjVer );

					// Verify whether the comment has been added to the promoted object.
					if( versionComment.VersionComment.Value.DisplayValue == commentValue )
						Report.LogPass( "Test Passed: Editing the object which was promoted by another user is successful." );
					else
						Assert.Fail( "Test Failed:  Editing the object which was promoted by another user is failed." );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroying metadatacard of promoted object using ReadOnly User." )]
		[Order( 1 )]
		public void ExternalRepoObjectOperations_DemotingPromotedObjectUsingReadOnlyUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectName );

					// Stores the user credentials to be created.
					UserCredentialConfiguration userCredentials = new UserCredentialConfiguration();
					userCredentials.Username = returnTuple.Item1.GetValueForColumn( "User" );
					userCredentials.Password = returnTuple.Item1.GetValueForColumn( "Password" );

					// Create/Update login account.
					mfilesApi.AddLoginAccount( userCredentials, false, MFilesAPI.MFLicenseType.MFLicenseTypeReadOnlyLicense );

					// Below code will add the user to the vault if empty vault is created. 
					mfilesApi.AddUserToVault( returnTuple.Item1.GetValueForColumn( "User" ), false );

					// Login using specified user.
					mfilesApi.LoginToVault( userCredentials.Username,
						userCredentials.Password );

					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 2: Logged in with user having readonly license." );

					try
					{
						// Destroy the metadatacard of the promoted object.
						mfilesApi.DemoteObjects( externalObjVer );

					}
					catch( Exception ex )
					{
						// Verifies the error message.
						if( ex.Message.ToLower().Contains( returnTuple.Item1.GetValueForColumn( "ErrorMessage" ).ToLower() ) )
						{
							Report.LogPass( "Test Passed: User with read only user cannot promote the unmanaged object." + ex.Message );
							continue;
						}
						else
							Assert.Fail( "Test Failed: The error message has changed as " + ex.Message );
					}

					// Fails the test case if User with read only user can promote the unmanaged object.
					Assert.Fail( "Test Failed: User with read only user can promote the unmanaged object." );
				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}
	}
}

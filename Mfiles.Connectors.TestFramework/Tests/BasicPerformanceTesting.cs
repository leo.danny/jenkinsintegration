﻿using Mfiles.Connectors.TestFramework.Configuration;
using MFNUnit.Utils.TestData;
using NUnit.Framework;
using System;
using MFaaP.MFWSClient;
using System.Data;
using System.Diagnostics;

namespace Mfiles.Connectors.TestFramework.Tests 
{
	[TestFixture]
	[Order( 1 )]
	internal class BasicPerformanceTesting : TestBaseClass
    {
        /// <summary>
        /// Represents an object version.
        /// </summary>
        protected ObjVer externalObjVer;

		/// <summary>
		/// Variable of the StopWatch class.
		/// </summary>
		private Stopwatch stopWatch;

		/// <summary>
		/// Variable to store the test data.
		/// </summary>
		private Tuple<DataRow> returnTuple;

		/// <summary>
		/// Represents the object path.
		/// </summary>
		private string objectPath;
		
		/// <summary>
		/// Instance of the MFilesApiHelperMethods class.
		/// </summary>
		public MFilesApiHelperMethods mfilesApi;

		/// <summary>
		/// Stores test configuration..
		/// </summary>
		TestConfiguration testConfiguration;

		[ SetUp]
        public void SetUp()
        {
            // Assigns null before the test begins.
            stopWatch = null;

			// Load test configuration.
			testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

			// Creates new object reference for the class MFilesApiHelperMethods.
			mfilesApi = new MFilesApiHelperMethods();

			// Login using specified user.
			mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
				testConfiguration.Users.User2.MFilesCredentials.Password );

			// Get the testdata for the current test.
			foreach (var data in MyDataSources.FactoryMethod(TestContext.CurrentContext.Test.MethodName.ToString()))
                returnTuple = (Tuple<DataRow>) data;     
        }

        [TearDown]
        public void TearDown()
        {
			// Update the execution time in XML file.
			GeneratingPerformanceResults.updateXMLDocument( Setup.connectorName, stopWatch.ElapsedMilliseconds.ToString() );
			
			// Update the execution time in SQL Server.
			//GeneratingPerformanceResults.updateSQLServer( Setup.connectorName, stopWatch.ElapsedMilliseconds.ToString() );			
        }

		[Test]
		[Category( "Performance" )]
		[Description( "Perform Checkout operation on unmanaged object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_CheckOut_UMO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Get the object details.
				MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

				// Start the time to log the performance of checkout operation on unmanaged object.
				stopWatch = Stopwatch.StartNew();

				// Checkout the unmanaged object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

				// Stop the time.
				stopWatch.Stop();

				// Checkin the object.
				mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjectVersion.ObjVer );
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Perform Checkout operation on promoted object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_CheckOut_PO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

				// Start the time to log the performance of checkout operation on promoted object.
				stopWatch = Stopwatch.StartNew();

				// Checkout the promoted object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );

				// Stop the time.
				stopWatch.Stop();

				// Checkin the object.
				mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjectVersion.ObjVer );
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Perform Checkin operation on promoted object." )]
		[Order( 2 )]
		public void BasicPerformanceTesting_CheckIn_PO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

				// Checkout the object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );

				// Start the time to log the performance of checkin operation on promoted object.
				stopWatch = Stopwatch.StartNew();

				// CheckIn the promoted object.
				mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjectVersion.ObjVer );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Perform Checkin operation on unmanaged object." )]
		[Order( 2 )]
		public void BasicPerformanceTesting_CheckIn_UMO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Get the object details.
				MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

				// Checkout the object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjVer.ObjID );

				// Start the time to log the performance of checkin operation on unmanaged object.
				stopWatch = Stopwatch.StartNew();

				// CheckIn the unmanaged object.
				mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjectVersion.ObjVer );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Promote the unmanaged object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_PromoteObject()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Start the time to log the performance of PromoteObjects operation.
				stopWatch = Stopwatch.StartNew();

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties = 
					mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Destroy the metadata of external object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_DemoteObject()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Login using specified user.
				mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
					testConfiguration.Users.AdminUser.MFilesCredentials.Password );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;
				
				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

				MFilesAPI.ObjIDs objIDs = new MFilesAPI.ObjIDs();
				objIDs.Add(-1, objVersionAndProperties.ObjVer.ObjID );

				// Start the time to log the performance of DemoteObjects operation.
				stopWatch = Stopwatch.StartNew();

				// Destroy the metadatacard of the promoted object.
				mfilesApi.vaultConnection.ExternalObjectOperations.DemoteObjects( objIDs );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
			finally
			{
				// Login using specified user.
				mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
					testConfiguration.Users.User2.MFilesCredentials.Password );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Perform UndoCheckout operation on unmanaged object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_UndoCheckOut_UMO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Get the object details.
				MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

				// Checkout the object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

				// Start the time to log the performance of UndoCheckout operation on unmanaged object.
				stopWatch = Stopwatch.StartNew();

				// UndoCheckout for unmanaged object.
				mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesApiObjectVersion.ObjVer );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Perform UndoCheckout operation on promoted object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_UndoCheckOut_PO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

				// Checkout the object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );

				// Start the time to log the performance of UndoCheckout operation on promoted object.
				stopWatch = Stopwatch.StartNew();

				// UndoCheckout for promoted object.
				mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesApiObjectVersion.ObjVer );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Delete unmanaged object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_Delete_UMO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Get the object details.
				MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

				// Start the time to log the performance of DeleteObject operation on unmanaged object.
				stopWatch = Stopwatch.StartNew();

				// Delete unmanaged object.
				mfilesApi.vaultConnection.ObjectOperations.DeleteObject( mfilesObjID );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Delete the promoted object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_Delete_PO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

				// Start the time to log the performance of DeleteObject operation on promoted object.
				stopWatch = Stopwatch.StartNew();

				// Delete the promoted object.
				mfilesApi.vaultConnection.ObjectOperations.DeleteObject( objVersionAndProperties.ObjVer.ObjID );

				// Stop the time.
				stopWatch.Stop();
			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Rename unmanaged object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_Rename_UMO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// The new name for the object.
				var newName = "Performance" + System.DateTime.Now.Millisecond;

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Object to store the details of the file.
				MFilesAPI.FileVer fileVersion = mfilesApi.GetFileVer( objectVersion );

				// Get the object details.
				MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

				// Checkout the object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

				// Start the time to log the performance of RenameFile operation on unmanaged object.
				stopWatch = Stopwatch.StartNew();

				// Rename unmanaged object.
				mfilesApiObjectVersion = mfilesApi.vaultConnection.ObjectFileOperations.RenameFile
										 ( mfilesApiObjectVersion.ObjVer, fileVersion, 
										 newName, objectVersion.Files[0].Extension );

				// CheckIn the renamed unmanaged object.
				mfilesApiObjectVersion = mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjectVersion.ObjVer );

				// Stop the time.
				stopWatch.Stop();

			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Rename promoted object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_Rename_PO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// The new name for the object.
				var newName = "Performance" + System.DateTime.Now.Millisecond;

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );

				// Stores the object name.
				string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( objectVersion.ObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

				// Get the object.
				objectVersion = this.GetObjectVersion( objectName, objectPath );
				
				// Object to store the details of the file.
				MFilesAPI.FileVer fileVersion = mfilesApi.GetFileVer( objectVersion );
				
				// Checkout the object.
				MFilesAPI.ObjectVersion mfilesApiObjectVersion =
					mfilesApi.vaultConnection.ObjectOperations.CheckOut( objVersionAndProperties.ObjVer.ObjID );

				// Stores the version of the checked out object.
				fileVersion.Version = mfilesApiObjectVersion.CheckedOutVersion;

				// Start the time to log the performance of RenameFile operation on promoted object.
				stopWatch = Stopwatch.StartNew();

				// Rename promoted object.
				mfilesApiObjectVersion = mfilesApi.vaultConnection.ObjectFileOperations.RenameFile
										 ( mfilesApiObjectVersion.ObjVer, fileVersion,
										 newName, objectVersion.Files[ 0 ].Extension );

				// CheckIn the renamed promoted object.
				mfilesApiObjectVersion = mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjectVersion.ObjVer );

				// Stop the time.
				stopWatch.Stop();

			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Destroy unmanaged object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_Destroy_UMO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Login using specified user.
				mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
					testConfiguration.Users.AdminUser.MFilesCredentials.Password );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Get the object details.
				MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

				// Start the time to log the performance of DestroyObject operation on unmanaged object.
				stopWatch = Stopwatch.StartNew();

				// Destroy unmanaged object.
				mfilesApi.vaultConnection.ObjectOperations.DestroyObject( mfilesObjID, true, -1 );

				// Stop the time.
				stopWatch.Stop();

			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
			finally
			{
				// Login using specified user.
				mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
					testConfiguration.Users.User2.MFilesCredentials.Password );
			}
		}

		[Test]
		[Category( "Performance" )]
		[Description( "Destroy promoted object." )]
		[Order( 1 )]
		public void BasicPerformanceTesting_Destroy_PO()
		{
			try
			{
				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Login using specified user.
				mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
					testConfiguration.Users.AdminUser.MFilesCredentials.Password );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );				

				// Start the time to log the performance of DestroyObject operation on promoted object.
				stopWatch = Stopwatch.StartNew();

				// Destroy promoted object.
				mfilesApi.vaultConnection.ObjectOperations.DestroyObject( objVersionAndProperties.ObjVer.ObjID, true, -1 );

				// Stop the time.
				stopWatch.Stop();

			}
			catch( Exception exception )
			{
				Assert.Fail( exception.Message );
			}
			finally
			{
				// Login using specified user.
				mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
					testConfiguration.Users.User2.MFilesCredentials.Password );
			}
		}
	}
}

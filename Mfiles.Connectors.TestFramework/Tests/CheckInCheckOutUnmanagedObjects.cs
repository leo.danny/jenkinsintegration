﻿using Mfiles.Connectors.TestFramework.Configuration;
using MFaaP.MFWSClient;
using NUnit.Framework;
using MFNUnit.Utils.Logger;
using System;
using MFNUnit.Utils.TestData;
using System.Data;
using System.Collections.Generic;

namespace Mfiles.Connectors.TestFramework.Tests
{
	[TestFixture]
	[Order( 1 )]
	class CheckInCheckOutUnmanagedObjects : TestBaseClass
	{
		/// <summary>
		/// Represents an object version.
		/// </summary>
		protected ObjVer externalObjVer;

		/// <summary>
		/// Stores the path of the object.
		/// </summary>
		private string objectPath = null;

		/// <summary>
		/// Variable to store testdata count and print in extent report.
		/// </summary>
		public int testDataCount;

		/// <summary>
		/// Instance of the MFilesApiHelperMethods class.
		/// </summary>
		public MFilesApiHelperMethods mfilesApi;

		/// <summary>
		/// Load test configuration.
		/// </summary>
		TestConfiguration testConfiguration;

		[ SetUp]
		public void SetUp()
		{
			// Store testdata count and print in extent report
			testDataCount = 1;

			// Starts logging the extent report.
			Report.StartTest( Setup.connectorName );

			// Load test configuration.
			testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

			// Creates new object reference for the class MFilesApiHelperMethods.
			mfilesApi = new MFilesApiHelperMethods();

			// Login using specified user.
			mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
				testConfiguration.Users.User2.MFilesCredentials.Password );
		}

		[TearDown]
		public void TearDown()
		{
			// Add the status and information into the extent report.
			Report.Flush();
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Try to checkout the object that was checked out by another user" )]
		[Order( 1 )]
		public void CheckInCheckOutUnmanagedObjects_CannotCheckOutByAnotherUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{

					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
					
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
									testConfiguration.Users.User2.MFilesCredentials.Password );

					// Get the object ID.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Checkout the object.
					MFilesAPI.ObjectVersion mfilesApiObjectVersion =
							mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					Report.LogInfo( "Step 1: " +
						"The object is checked out by the user " + mfilesApiObjectVersion.CheckedOutToUserName );

					// Try to check out the object which was checked out by another user.
					try
					{
						// Login using specified user.
						mfilesApi.LoginToVault( testConfiguration.Users.User1.MFilesCredentials.Username,
									testConfiguration.Users.User1.MFilesCredentials.Password );

						// Checkin the object by another user.
						mfilesApiObjectVersion =
								mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );
					}

					catch( Exception exception )
					{
						// Throws exception if the error message is not as expected.
						if( !exception.Message.Contains( returnTuple.Item1.GetValueForColumn( "ErrorMessage" ) ) )
							throw exception;
					}

					// Get the object.
					objectVersion = this.GetObjectVersion( objectName, objectPath );
					
					// Verifies the checked out user name.
					if( objectVersion.CheckedOutToUserName.Equals( testConfiguration.Users.User2.MFilesCredentials.Username ) )
						Report.LogPass( "Test Passed. Cannot check out the object which was checked " +
							"out by another user. Checked Out to " + mfilesApiObjectVersion.CheckedOutToUserName );
					else
						Assert.Fail( "Test Failed. Object checked out by other user can be checked out and the checked out " +
							"status of the object " + objectName + " is " + mfilesApiObjectVersion.ObjectCheckedOut );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
									testConfiguration.Users.User2.MFilesCredentials.Password );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// CheckIn the object if it is in checked out state.
					if( mfilesObjVersion.ObjectCheckedOut )
						mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjVer );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Checking out of multiple objects." )]
		[Order( 1 )]
		public void CheckInCheckOutUnmanagedObjects_CheckOutMultipleObjects()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Get the number of objects to checkout.
					int NumOfObjects = Int32.Parse( returnTuple.Item1.GetValueForColumn( "ObjCount" ) );

					// Loop increment variable.
					int count;

					// Stores the object.
					ObjectVersion objectVersion;

					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Object to store details of all objects.
					MFilesAPI.ObjIDs mfilesObjIDs = new MFilesAPI.ObjIDs();

					// Get the object.
					for( count = 1; count <= NumOfObjects; count++ )
					{
						// Get the object.
						objectVersion =
							this.GetObjectVersion( returnTuple.Item1.GetValueForColumn( "ObjectName" + count ), objectPath );
						this.externalObjVer = objectVersion.ObjVer;

						// Get the object ID.
						MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

						// Stores the object details.
						mfilesObjIDs.Add( count - 1, mfilesObjID );
					}

					Report.LogInfo( "Step 1: Stored the details of multiple objects." );
					
					// Checkout multiple objects.
					MFilesAPI.ObjectVersions mfilesApiObjectVersions =
							mfilesApi.vaultConnection.ObjectOperations.CheckOutMultipleObjects( mfilesObjIDs );

					Report.LogInfo( "Step 2: Checked Out multiple objects." );
					
					// Verify the objects are in checked out mode.
					for( count = 1; count <= 3; count++ )
					{
						// Get the object.
						objectVersion =
							this.GetObjectVersion( returnTuple.Item1.GetValueForColumn( "ObjectName" + count ), objectPath );

						// Verify the status of the object.
						if( !objectVersion.ObjectCheckedOut )
							break;
					}

					// If all the objects are checked out.
					if( count > 3 )
						Report.LogPass( "Test Passed. Multiple objects were checked out at same time." );
					else
						Assert.Fail( "Test Failed. Multiple objects were not checked out at same time." );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Checking in of multiple objects." )]
		[Order( 2 )]
		public void CheckInCheckOutUnmanagedObjects_CheckInMultipleObjects()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Get the number of objects to checkout.
					int NumOfObjects = Int32.Parse( returnTuple.Item1.GetValueForColumn( "ObjCount" ) );

					// Loop increment variable.
					int count;

					// Stores the object.
					ObjectVersion objectVersion;

					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Object to store details of all objects.
					MFilesAPI.ObjVers mfilesObjVers = new MFilesAPI.ObjVers();
					
					// Get the object.
					for( count = 1; count <= NumOfObjects; count++ )
					{
						// Get the object.
						objectVersion =
							this.GetObjectVersion( returnTuple.Item1.GetValueForColumn( "ObjectName" + count ), objectPath );
						this.externalObjVer = objectVersion.ObjVer;

						// Get the object version.
						MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

						// Stores the object details.
						mfilesObjVers.Add( count - 1, mfilesObjVer );
					}

					Report.LogInfo( "Step 1: Stored the details of multiple objects." );

					// Checkin multiple objects.
					MFilesAPI.ObjectVersions mfilesApiObjectVersions =
							mfilesApi.vaultConnection.ObjectOperations.CheckInMultipleObjects( mfilesObjVers );

					Report.LogInfo( "Step 2: Checked In multiple objects." );
					
					// Verify the objects are checked in mode.
					for( count = 1; count <= 3; count++ )
					{
						// Get the object.
						objectVersion =
							this.GetObjectVersion( returnTuple.Item1.GetValueForColumn( "ObjectName" + count ), objectPath );

						// Verify the status of the object.
						if( objectVersion.ObjectCheckedOut )
							break;
					}

					// If all the objects are checked in.
					if( count > 3 )
						Report.LogPass( "Test Passed. Multiple objects were checked out at same time." );
					else
						Assert.Fail( "Test Failed. Multiple objects were not checked out at same time." );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" ), Category( "Bug" )]
		[Description( "Verify whether the objects are updated in CheckedOutToMe view based on the obejct status." )]
		[Order( 1 )]
		public void CheckInCheckOutUnmanagedObjects_CheckedOutToMeView()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the properties of CheckedOutToMe view.
					MFilesAPI.View checkedOut =
						mfilesApi.vaultConnection.ViewOperations.GetBuiltInView(
							MFilesAPI.MFBuiltInView.MFBuiltInViewCheckedOutToCurrentUser );

					// Stores the folder location of CheckedOutToMe view.
					MFilesAPI.FolderDefs viewFolderPath = new MFilesAPI.FolderDefs();
					MFilesAPI.FolderDef viewFolder = new MFilesAPI.FolderDef();
					viewFolder.SetView( checkedOut.ID );
					viewFolderPath.Add( -1, viewFolder );

					// Get the count of CheckedOutToMe folder.
					MFilesAPI.FolderContentItems countBeforeAdding =
						mfilesApi.vaultConnection.ViewOperations.GetFolderContents( viewFolderPath );

					Report.LogInfo( "Step 1: Number of objects in CheckedOutToMe view is " + countBeforeAdding.Count );
					
					// Get the object.
					ObjectVersion objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object id and object type.
					MFilesAPI.ObjID objId = mfilesApi.GetObjID( externalObjVer );

					// Checkout the object.
					MFilesAPI.ObjectVersion mfilesObjectVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( objId );
					
					// Get the contents of count of CheckedOutToMe view.
					MFilesAPI.FolderContentItems countAfterAdding =
						mfilesApi.vaultConnection.ViewOperations.GetFolderContents( viewFolderPath );

					// Verifies the object has been added to CheckedOutToMe view.
					if( countBeforeAdding.Count.Equals( countAfterAdding.Count ) )
						Assert.Fail( "Test Failed: Checked Out Unmanaged object " + objectName +
							"is not added to CheckedOutToMe view." );

					Report.LogInfo( "Step 2: Checked Out Unmanaged object " + objectName + " is added to CheckedOutToMe view." );
					
					// CheckIn the object.
					mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjectVersion.ObjVer );

					// Get the contents of CheckedOutToMe view.
					MFilesAPI.FolderContentItems countAfterRemoving =
						mfilesApi.vaultConnection.ViewOperations.GetFolderContents( viewFolderPath );
					
					// Verifies the object has been added to CheckedOutToMe view.
					if( countBeforeAdding.Count.Equals( countAfterRemoving.Count ) )
						Report.LogPass( "Test Passed: Checked In Unmanaged object " + objectName +
							"is not available in CheckedOutToMe view." );					
					else
						Assert.Fail( "Test Failed: Checked In Unmanaged object " + objectName +
							"is available in CheckedOutToMe view." );
					
				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" ), Category( "Bug" )]
		[Description( "Promote an unmanaged object when it was checked out." )]
		[Order( 3 )]
		public void CheckInCheckOutUnmanagedObjects_PromotingUnmanagedObjectWhenCheckedOut()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the version of the external object." );
					
					// Get the ObjID.
					MFilesAPI.ObjID objId = mfilesApi.GetObjID( externalObjVer );

					// Checkout the object.
					MFilesAPI.ObjectVersion mfilesObjectVersion = 
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( objId );

					// Promote the object.
					mfilesApi.PromoteObjects( mfilesObjectVersion.ObjVer,
						returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					// Checkin the object.
					mfilesObjectVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjectVersion.ObjVer );

					Report.LogInfo( "Step 2: Promoted the external object " + objectName );

					// Get the objectVersion.
					objectVersion = GetObjectVersion( objectName, objectPath );
					
					// Verifies whether the object is promoted.
					if( objectVersion.Class.ToString().Equals( returnTuple.Item1.GetValueForColumn( "ClassID" ) ) )
						Report.LogPass( "Test Passed. Promoting the unmanaged object when checked out is successfull. " +
							"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );
					else
						Assert.Fail( "Test Failed.Promoting the unmanaged object when checked out got failed. " +
							"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// CheckIn the object if it is in checked out state.
					if( mfilesObjVersion.ObjectCheckedOut )
						mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjVer );
				}

			}

		}

	}
}

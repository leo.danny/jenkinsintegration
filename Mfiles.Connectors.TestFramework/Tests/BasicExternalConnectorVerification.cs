﻿using Mfiles.Connectors.TestFramework.Configuration;
using MFaaP.MFWSClient;
using NUnit.Framework;
using MFNUnit.Utils.Logger;
using System;
using MFNUnit.Utils.TestData;
using System.Data;
using System.Collections.Generic;
using System.Threading;

namespace Mfiles.Connectors.TestFramework.Tests
{
	[TestFixture]
	[Order( 1 )]
	class BasicExternalConnectorVerification : TestBaseClass
	{
		/// <summary>
		/// Represents an object version.
		/// </summary>
		protected ObjVer externalObjVer;

		/// <summary>
		/// Get the object.
		/// </summary>
		private ObjectVersion[] extObjVersion;

		/// <summary>
		/// Existence of the object.
		/// </summary>
		private int fileExistence;

		/// <summary>
		/// Stores the path of the object.
		/// </summary>
		private string objectPath = null;

		/// <summary>
		/// Variable to store testdata count and print in extent report.
		/// </summary>
		public int testDataCount;

		/// <summary>
		/// Load the test configuration.
		/// </summary>
		TestConfiguration testConfiguration;

		/// <summary>
		/// Instance of the MFilesApiHelperMethods class.
		/// </summary>
		public MFilesApiHelperMethods mfilesApi;

		[SetUp]
		public void SetUp()
		{
			// Store testdata count and print in extent report
			testDataCount = 1;

			// Starts logging the extent report.
			Report.StartTest( Setup.connectorName );

			// Load test configuration.
			testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

			// Creates new object reference for the class MFilesApiHelperMethods.
			mfilesApi = new MFilesApiHelperMethods();

			// Login using specified user.
			mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
				testConfiguration.Users.User2.MFilesCredentials.Password );
		}

		[TearDown]
		public void TearDown()
		{
			// Add the status and information into the extent report.
			Report.Flush();
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Perform check in operation  and verfify the status." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_ObjectIsCheckedIn()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
					
					// Get the object details.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( mfilesObjID );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// Ensure that the object is checked in.
					if( mfilesObjVersion.ObjectCheckedOut )
						Assert.Fail( "Object is not checked in" );

					// Checkout the object.
					mfilesObjVersion = mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					// CheckIn the object.
					mfilesObjVersion = mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVersion.ObjVer );

					// Stores the status of the object
					bool objectStatus = mfilesObjVersion.ObjectCheckedOut;

					// Assert
					// Verifies whether the object is Checked In or not.
					if( !objectStatus )
						Report.LogPass( "Test Passed. Check in operation is successfull " +
							"and the status of object " + objectName + " is, CheckedOut:" + objectStatus );
					else
						Assert.Fail( "Test Failed. Check in operation fails and the " +
							"status of object " + objectName + " is, CheckedOut:" + objectStatus );
				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}

				finally
				{
					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// CheckIn the object if it is in checked out state.
					if( mfilesObjVersion.ObjectCheckedOut )
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVer );
				}
			}

		}

		[Test]
		[Category( "Smoke" ), Category( "Bug" )]
		[Description( "Try to checkin the object that was checked out by another user" )]
		[Order( 2 )]
		public void BasicExternalConnectorVerification_CannotCheckInByAnotherUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{

					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User1.MFilesCredentials.Username,
							testConfiguration.Users.User1.MFilesCredentials.Password );

					// Get the object ID.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Checkout the object.
					MFilesAPI.ObjectVersion mfilesApiObjectVersion =
							mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					Report.LogInfo( "Step 1: " +
						"The object is checked out by the user " + mfilesApiObjectVersion.CheckedOutToUserName );

					// Try to check in the object which was checked out by another user.
					try
					{
						// Login using specified user.
						mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
								testConfiguration.Users.User2.MFilesCredentials.Password );
						
						// Checkin the object by another user.
						mfilesApiObjectVersion =
								mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesApiObjectVersion.ObjVer );
					}

					catch( Exception exception )
					{
						// Throws exception if the error message is not as expected.
						if( !exception.Message.Contains( "Not found" ) )
							throw exception;
					}

					// Get the object details.
					objectVersion = this.GetObjectVersion( objectName, objectPath );
					
					// Verifies whether the object is Checked In or not.
					if( objectVersion.CheckedOutToUserName.Equals( testConfiguration.Users.User1.MFilesCredentials.Username ) )
						Report.LogPass( "Test Passed. Cannot check in the object which was checked " +
							"out by another user. Checked Out to " + mfilesApiObjectVersion.CheckedOutToUserName );
					else
						Assert.Fail( "Test Failed. Object checked out by other user can be checked in and the checked out " +
							"status of the object " + objectName + " is " + mfilesApiObjectVersion.ObjectCheckedOut );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
							testConfiguration.Users.User2.MFilesCredentials.Password );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// CheckIn the object if it is in checked out state.
					if( mfilesObjVersion.ObjectCheckedOut )
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVer );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Perform checkout operation and verfify the status" )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_ObjectIsCheckedOut()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
						
					// Get the object details.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( mfilesObjID );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// Ensure that the object is checked in.
					if( mfilesObjVersion.ObjectCheckedOut )
						Assert.Fail( "Object is not checked in" );

					// Checkout the object.
					mfilesObjVersion = mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					// Stores the status of the object
					bool objectStatus = mfilesObjVersion.ObjectCheckedOut;
					
					// Verifies whether the object is checked out or not.
					if( mfilesObjVersion.ObjectCheckedOut )
						Report.LogPass( "Test Passed. Object is checked out successfully " +
							"and the status of object " + objectName + " is CheckedOut:" + objectStatus );
					else
						Assert.Fail( "Test Failed. " +
							"Object is not checked out and the status of object " + objectName + " is CheckedOut:" + objectStatus );
				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

				finally
				{
					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// CheckIn the object if it is in checked out state.
					if( mfilesObjVersion.ObjectCheckedOut )
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVer );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Try to checkout the object using a read only user." )]
		[Order( 2 )]
		public void BasicExternalConnectorVerification_CheckoutByReadOnlyUser()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the user credentials to be created.
					UserCredentialConfiguration userCredentials = new UserCredentialConfiguration();
					userCredentials.Username = returnTuple.Item1.GetValueForColumn( "User" );
					userCredentials.Password = returnTuple.Item1.GetValueForColumn( "Password" );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the version of the external object." );

					// Create/Update login account.
					mfilesApi.AddLoginAccount( userCredentials, false, MFilesAPI.MFLicenseType.MFLicenseTypeReadOnlyLicense );
					
					// Below commented code will add the user to the vault if empty vault is created. 
					mfilesApi.AddUserToVault( returnTuple.Item1.GetValueForColumn( "User" ), false );

					// Login using specified user.
					mfilesApi.LoginToVault( userCredentials.Username,
						userCredentials.Password );

					Report.LogInfo( "Step 2: Logged in with user having readonly license." );
					
					// Get the object identifier.
					MFilesAPI.ObjID objID = mfilesApi.GetObjID( externalObjVer );

					try
					{
						// Try to checkout the object.
						mfilesApi.CheckOutObject( objID );
					}
					catch( Exception exception )
					{
						// Verifies the exception message
						if( exception.Message.Contains( returnTuple.Item1.GetValueForColumn( "Message" ) ) )
							Report.LogInfo( "Exception occures while trying to checkout using read only user." );
						else
							throw exception;
					}

					// Get the object.
					objectVersion =
						this.GetObjectVersion( returnTuple.Item1.GetValueForColumn( "ObjectName" ), objectPath );
					
					// Verifies whether the object is checked out or not.
					if( !objectVersion.ObjectCheckedOut )
						Report.LogPass( "Test Passed. Object is not checked out by read only user and the " +
							"Check Out status of object " + objectName + " is " + objectVersion.ObjectCheckedOut );
					else
						Assert.Fail( "Test Failed. Object is checked out by read only user and the " +
							"Check Out status of object " + objectName + " is " + objectVersion.ObjectCheckedOut );
				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Delete the unmanaged object and verify the status." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_DeleteUnmanagedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object if its not present in the root.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
					
					//Stores the object name.
					string objectTitle = objectVersion.Title;

					// Expected error message.
					string expectedMessage = "No object with the name " + objectName + " exists.";
					
					// Delete the object.
					this.Client.ObjectOperations.DeleteObject( externalObjVer );

					// Verifies the existence of object.
					try
					{
						// Get the object.
						this.GetObjectVersion( objectName, objectPath );
					}
					catch( Exception exception )
					{

						//Verifies the error thrown is expected.
						if( exception.Message == expectedMessage )
						{
							Report.LogPass( "Test Passed: " +
								"Object " + objectName + " has been deleted from repository. " + exception.Message );
							continue;
						}
						else
							throw exception;
					}

					// Fails the testcase if object is not deleted.
					Assert.Fail( "Test Failed: Object " + objectName + " is not deleted from external object. " );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Delete the promoted object and verify the status." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_DeletePromotedObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Expected error message.
					string expectedMessage =
							"No object with the name " + objectName + " exists.";

					Report.LogInfo( "Step 1: Fetched the object version of an unmanaged object." );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the unmanaged object " + objectName );

					// Delete the object.
					mfilesApi.vaultConnection.ObjectOperations.DeleteObject( objVersionAndProperties.ObjVer.ObjID );

					try
					{
						// Get the object.
						this.GetObjectVersion( objectName, objectPath );
					}
					catch( Exception exception )
					{
						//Verifies the error thrown is expected.
						if( exception.Message == expectedMessage )
						{
							Report.LogPass( "Test Passed: " +
								"Promoted Object " + objectName + " has been deleted from repository." );
							continue;
						}
						else
							throw exception;
					}

					// Fails the testcase if object is not deleted.
					Assert.Fail( "Test Failed: Promoted Object " + objectName + " is not deleted from external repository." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Delete the object and verify whether undeletion can be performed." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_UndeleteExternalObject()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object if its not present in the root.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					int index = 1;

					Report.LogInfo( "Step 1: Logged in with the user " + returnTuple.Item1.GetValueForColumn( "User" ) );

					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Stores the object id and object type.
					MFilesAPI.ObjID mfilesObjId = mfilesApi.GetObjID( externalObjVer );

					// Delete the object.
					mfilesApi.vaultConnection.ObjectOperations.DeleteObject( mfilesObjId );
					
					Report.LogInfo( "Step 2: External object " + objectName + " was deleted from the vault." );
					
					// Prepare search condition.
					MFilesAPI.SearchCondition searchCondition = new MFilesAPI.SearchCondition();
					searchCondition.Expression.SetStatusValueExpression( MFilesAPI.MFStatusType.MFStatusTypeDeleted, null );
					searchCondition.TypedValue.SetValue( MFilesAPI.MFDataType.MFDatatypeBoolean, true );
					searchCondition.ConditionType = MFilesAPI.MFConditionType.MFConditionTypeEqual;

					// Perform search for deleted objects.
					MFilesAPI.ObjectSearchResults searchResults =
						mfilesApi.vaultConnection.ObjectSearchOperations.SearchForObjectsByCondition( searchCondition, true );

					Report.LogInfo( "Step 3: Searched for the Deleted objects in the vault." );
					
					// Verify the deleted external object is available in deleted search results.
					for( ; index <= searchResults.Count; index++ )
					{
						// Verify the returned object is deleted external object.
						if( searchResults[ index ].Title.Contains( objectVersion.Title ) )
							break;
					}

					// Verify the object is not available in deleted search view.
					if( index > searchResults.Count )
						Report.LogPass( "Test Passed:  " +
							"External object " + objectName + " is not available in deleted search view." );
					else
					{
						Report.LogError( "Deleted external object " + objectName +
							" can be viewed in deleted object search view." );

						// Try to undelete the object.
						mfilesApi.vaultConnection.ObjectOperations.UndeleteObject( mfilesObjId );

						// Fails the testcase.
						Assert.Fail( "Test Failed: " +
							"External object " + objectName + " can be undeleted from m-files." );
					}


				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Download the object and verfify the status" )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_TheDownloadIsNotEmpty()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Download the object.
					byte[] downloadedObject = this.Client.ObjectFileOperations.DownloadFile( objectVersion );
					
					// Verifies whether the object is downloaded or not.
					if( downloadedObject.Length != 0 )
						Report.LogPass( "Test Passed. Downloading the object " + objectName + " is successful." );
					else
						Assert.Fail( "Test Failed. Downloading the object " + objectName + " failed." );
				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( " Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Promote an unmanaged object by changing the class value." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_PromotingUnmanagedObject()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the object.
					ObjectVersion objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the version of the external object." );
					
					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties = 
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the external object " + objectName );

					// Get the objectVersion.
					objectVersion = GetObjectVersion( objectName, objectPath );
					
					// Verifies whether the object is promoted.
					if( objectVersion.Class.ToString().Equals( returnTuple.Item1.GetValueForColumn( "ClassID" ) ) )
						Report.LogPass( "Test Passed. Promoting the unmanaged object is successfull. " +
							"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );
					else
						Assert.Fail( "Test Failed.Promoting the unmanaged object got failed. " +
							"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroy the metadata of a promoted object." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_DestroyMetadata()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					Report.LogInfo( "Step 1: Fetched the version of the external object." );

					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties = 
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					Report.LogInfo( "Step 2: Promoted the external object " + objectName );
					
					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Verifies whether the object is promoted.
					if( !objectVersion.Class.ToString().Equals( returnTuple.Item1.GetValueForColumn( "ClassID" ) ) )
						Assert.Fail( "Test Failed.Promoting the object is failed." );

					// Destroy the metadatacard of the promoted object.
					mfilesApi.DemoteObjects( externalObjVer );

					Report.LogInfo( "Step 3: Destroyed the metadata of promoted object " + objectName );

					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );
					
					// Verifies whether the object is Demoted.
					if( objectVersion.Class.Equals( -107 ) &&
							objectVersion.IsInternal == false )
						Report.LogPass( "Test Passed.Demoting the promoted object is successfull. " +
							"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );
					else
						Assert.Fail( "Test Failed.Demoting the promoted object was failed. " +
							"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
				finally
				{
					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}
			}

		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeSharepointServer2016" ), Category( "ExcludeOneDriveForBusiness" )]
		[Description( "Covert a promoted object into PDF." )]  
		[Order( 1 )]
		public void BasicExternalConnectorVerification_ConvertToPdf()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
					
					Report.LogInfo( "Step 1: Fetched the version of the external object." );
					
					// Promote the object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties = 
						mfilesApi.PromoteObjects( externalObjVer, returnTuple.Item1.GetValueForColumn( "ClassID" ) );

					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Verifies whether the object is promoted.
					if( !objectVersion.Class.ToString().Equals( returnTuple.Item1.GetValueForColumn( "ClassID" ) ) )
						Assert.Fail( "Test Failed.Promoting the object is failed." );

					Report.LogInfo( "Step 2: Promoted the external object " + objectName );
					
					// Stores the file ID of the object.
					int fileID = objectVersion.Files.ToArray()[ 0 ].ID;

					// Covert the document into PDF.
					mfilesApi.ConvertToPDF( externalObjVer, fileID );

					Report.LogInfo( "Step 3: Promoted object " + objectName + " is converted into PDF format." );

					// Get the object.
					objectVersion = GetObjectVersion( objectVersion.Title + "." + "pdf", objectPath );
					
					// Verifies the object has been converted into pdf or not.
					if( objectVersion != null )
						Report.LogPass( "Test Passed.File Converted to PDF. " +
							"EscapedTitleWithID:" + objectVersion.EscapedTitleWithID );
					else
						Assert.Fail( "Test Failed.Error while converting into PDF." );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Try to edit the permissions of external objects." )]
		[Order( 2 )]
		public void BasicExternalConnectorVerification_EditPermissions()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Variable to store the status of permission.
					bool permissionNotChanged;

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the object
					ObjectVersion objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					Report.LogInfo( "Step 1: Fetched the version of the external object." );

					// Get the object permission.
					MFilesAPI.ObjectVersionPermissions previousPermission =
						mfilesApi.GetObjectPermission( externalObjVer );

					try
					{
						// Try to edit the permission of the external object.
						mfilesApi.EditPermissions( externalObjVer, 1 );
					}
					catch( Exception exception )
					{
						// Verifies the exception message.
						if( exception.Message.Contains( "Not found" ) )
						{
							// Expected message.
							Report.LogInfo( "Permissions of the external object is not editable." );
						}
					}

					Report.LogInfo( "Step 2: Edit Permission of the external object has be done." );
					
					// Get the object.
					objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Get the object permission.
					MFilesAPI.ObjectVersionPermissions currentPermission =
										mfilesApi.GetObjectPermission( externalObjVer );

					Report.LogInfo( "Step 3: Permission of the external object after editing is stored." );
					
					// Verify the permission of object has NACL or Custom.
					if( previousPermission.NamedACL.Name == null && previousPermission.CustomACL == true )
						permissionNotChanged = currentPermission.CustomACL;
					else
						permissionNotChanged =
							previousPermission.NamedACL.Name.ToString().Equals(
								currentPermission.NamedACL.Name.ToString() );

					// Verifies the permission has been modified or not.
					if( permissionNotChanged )
						Report.LogPass( "Test Passed.The permission of external object is not changed." );
					else
						Assert.Fail( "Test Failed.The permission of external object is changed." );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Adding and removing a promoted object from favorites." )]
		[Order( 3 )]
		public void BasicExternalConnectorVerification_AddAndRemoveFavorites()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the properties of Favorites view.
					MFilesAPI.View fav =
						mfilesApi.vaultConnection.ViewOperations.GetBuiltInView(
							MFilesAPI.MFBuiltInView.MFBuiltInViewFavorites );

					// Stores the folder location of Favorites view.
					MFilesAPI.FolderDefs viewFolderPath = new MFilesAPI.FolderDefs();
					MFilesAPI.FolderDef viewFolder = new MFilesAPI.FolderDef();
					viewFolder.SetView( fav.ID );
					viewFolderPath.Add( -1, viewFolder );

					// Get the contents of Favorites folder before adding the object as favorites.
					MFilesAPI.FolderContentItems countBeforeAdding =
						mfilesApi.vaultConnection.ViewOperations.GetFolderContents( viewFolderPath );

					Report.LogInfo( "Step 1: Number of objects in favorites view before " +
								"adding a promoted object is " + countBeforeAdding.Count );
					
					// Get the object.
					ObjectVersion objectVersion = GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Get the objID.
					MFilesAPI.ObjID objId = new MFilesAPI.ObjID();
					objId = mfilesApi.GetObjID( externalObjVer );

					// Add the promoted object to favorites.
					mfilesApi.vaultConnection.ObjectOperations.AddFavorite( objId );

					// Get the contents of Favorites view.
					MFilesAPI.FolderContentItems countAfterAdding =
						mfilesApi.vaultConnection.ViewOperations.GetFolderContents( viewFolderPath );

					// Verifies the object has been added to favorites view.
					if( countBeforeAdding.Count.Equals( countAfterAdding.Count ) )
						Assert.Fail( "Test Failed: Promoted object " + objectName +
							" is not added to favorite view." );

					Report.LogInfo( "Step 2: Added the promoted object " + objectName + " to favorites view." );

					// Remove the prmoted object from favorites.
					mfilesApi.vaultConnection.ObjectOperations.RemoveFavorite( objId );

					// Get the contents of Favorites view.
					MFilesAPI.FolderContentItems countAfterRemoving =
						mfilesApi.vaultConnection.ViewOperations.GetFolderContents( viewFolderPath );
					
					// Verify the object has been removed from favorites view.
					if( countBeforeAdding.Count.Equals( countAfterRemoving.Count ) )
						Report.LogPass( "Test Passed: Adding & removing promoted object " + objectName +
							" from favorite is successfull." );
					else
						Assert.Fail( "Test Failed: Removing promoted object " + objectName +
							" from favorites is not successfull." );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}


		[Test]
		[Category( "Smoke" )]
		[Description( "Adding an unmanaged object to favorites." )]
		[Order( 3 )]
		public void BasicExternalConnectorVerification_AddUnmanagedObjectToFavorites()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
					
					// Stores the object id and object type.
					ObjID objId = new ObjID();
					objId = this.GetObjID( externalObjVer );
					
					Report.LogInfo( "Step 1: Fetched the version of the external object." );
					
					try
					{
						// Try to add the unmanaged object as favorites.
						ExtendedObjectVersion extObjectVersion =
							this.Client.ObjectOperations.AddToFavorites( objId );
					}
					catch( Exception exception )
					{
						// Verify the exception message.
						if( exception.InnerException.Message.Contains( "Not found" ) )
						{
							// Expected exception message.
							Report.LogPass( "Test Passed: Adding the unmanaged object " + objectName +
								" thorws expected error " + exception.InnerException.Message );
							continue;
						}
					}

					// Fails when no error occurs while adding an unmanaged object as favorites.
					Report.LogFail( "Test Failed: Adding the unmanaged object " + objectName +
						" does not throws an error." );
				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Verify the CheckOut status of an object" )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_CheckoutStatusIsNotNull()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Get the object details.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Get latest object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( mfilesObjID );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// Stores the status of the object
					bool objectStatus = mfilesObjVersion.ObjectCheckedOut;
					
					// Verifies whether the object is checked out or not.
					if( !objectStatus )
						Report.LogPass( "Test Passed. " +
							"Status of the object " + objectName + " is, CheckedOut:" + objectStatus );
					else
						Assert.Fail( "Test Failed. " +
							"Status of the object " + objectName + " is, CheckedOut:" + objectStatus );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Verify the existence of an object." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_DeletedStatusIsNotNull()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Get the objId.
					var objId = new ObjID( externalObjVer );
					
					// Get the status of the object.
					bool? deletedStatus = this.Client.ObjectOperations.GetDeletedStatus( objId );
					
					// Verifies whether the object is deleted or not.
					if( !deletedStatus.Equals( null ) )
						Report.LogPass( "Test Passed. " + objectName +
							" is available in the external repository and the DeletedStatus is " + deletedStatus );
					else
						Assert.Fail( "Test Failed.  " + objectName +
							" does not exists in the repository and the DeletedStatus is " + deletedStatus );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Verify the contents of subfolders." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_WhenFolderIsNotRoot()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Stores the path of the object.
					string objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the sub folder contents.
					FolderContentItems folderContentItems = this.GetSubfolderContents( objectPath );
					
					// Verifies whether the object is checked out or not.
					if( !folderContentItems.Items.Equals( null ) )
						Report.LogPass( "Test Passed. " +
							"Sub folder has objects and the count is " + folderContentItems.Items.Count );
					else
						Report.LogFail( "Test Failed. " +
							"Sub folder has no objects and the count is " + folderContentItems.Items.Count );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Verify the contents of root folder." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_WhenFolderIsRoot()
		{
			try
			{
				// Create new node in extent report for separate test data.
				Report.LogHeader( testDataCount );

				// Get the root folder contents.
				FolderContentItems rootFolderContentItems = this.GetRootFolderContents();
				
				// Verifies whether the object is checked out or not.
				if( !rootFolderContentItems.Items.Equals( null ) )
					Report.LogPass( "Test Passed. " +
						"Root folder has objects and the count is " + rootFolderContentItems.Items.Count );
				else
					Assert.Fail( "Test Failed. " +
						"The root folder is empty and the count returned is " + rootFolderContentItems.Items.Count );

			}

			catch( Exception exception )
			{
				Console.WriteLine( exception );

				// Verifies the exception message.
				if( exception.Message.Contains( "Test Failed" ) )
					Report.LogFail( exception.Message );
				else
					Report.LogException( exception );
				
				Assert.Fail( exception.Message );
			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Get the properties of the object." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_ThePropertiesAreNotEmpty()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Get the properties of the object.
					MFilesAPI.PropertyValues propertyValues =
							mfilesApi.vaultConnection.ObjectPropertyOperations.GetProperties( mfilesObjVer );
					
					// Verifies whether the object id downloaded or not.
					if( propertyValues.Count > 0 )
						Report.LogPass( "Test Passed. Properties of the object " + objectName + " is not empty." );
					else
						Assert.Fail( "Test Failed. Properties of the object " + objectName + " is not valid." );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}

		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeDocumentum" )]
		[Description( "History Functionality" )]
		[Order( 3 )]
		public void BasicExternalConnectorVerification_CheckHistory()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				// Stores the path of the object.
				string[] objectPath = new string[] { };

				try
				{
					// Stores the object name.
					var objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );

					// Fetch required Test data. 
					var filePath = returnTuple.Item1.GetValueForColumn( "Path" );
					var userName = returnTuple.Item1.GetValueForColumn( "User" );
					var passWord = returnTuple.Item1.GetValueForColumn( "Password" );
					var objectClassID = returnTuple.Item1.GetValueForColumn( "ClassID" );
					var objectNewName = returnTuple.Item1.GetValueForColumn( "ObjNewName" );
					var objectNewNameExt = returnTuple.Item1.GetValueForColumn( "ObjectNewName" );
					
					// Get the object version.
					ObjectVersion objectVersion = GetObjectVersion( objectName, filePath );
					ObjVer externalObjVer = objectVersion.ObjVer;

					// Login using specified user
					mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
						testConfiguration.Users.AdminUser.MFilesCredentials.Password );

					// PreCondition: To Promote the External object.
					MFilesAPI.ObjectVersionAndProperties objVersionAndProperties = 
						mfilesApi.PromoteObjects( externalObjVer, objectClassID );

					// Get the object version after promoting.
					objectVersion = GetObjectVersion( objectName, filePath );
					externalObjVer = objectVersion.ObjVer;

					// Verify the object is promoted.
					if( objectVersion.Class.ToString().Equals( objectClassID ) )
						Report.LogInfo( "Object is Promoted successfully" );
					else
						Assert.Fail( "Promoting the object is failed." );
					
					// Rename the object for getting more history.
					var objectRename = this.Client.ObjectOperations.RenameObject( objectVersion, objectNewNameExt );
					if( !objectRename.Title.Equals( objectName ) )
						Report.LogInfo( "The Object " + objectName + " Renamed Successfully" );
					else
						Assert.Fail( "The Object " + objectName + " does not Renamed Successfully" );

					// Verify whether the object history is available or not.
					if( mfilesApi.GetObjectHistoryCount( externalObjVer ) >= 1 )
						Report.LogInfo( "Test Passed. Properties of the object is not empty" );
					else
						Assert.Fail( "Test Failed. Properties of the object is not valid" );

					// Get the object version after Renaming the object.
					objectVersion = GetObjectVersion( objectNewNameExt, filePath );
					externalObjVer = objectVersion.ObjVer;
					
					// Get all the property values from the metadata card.
					PropertyValue[] propertyValues = this.Client.ObjectPropertyOperations.GetProperties( externalObjVer );
					
					// Verify Whether the property values are available.
					if( propertyValues.Length != 0 )
						Report.LogInfo( "Test Passed. Properties of the object is not empty" );
					else
						Assert.Fail( "Test Failed. Properties of the object is not valid" );

					bool IsReturn = false;  // Default Return False

					// Verify whether the modified date in metadata card is equal to the modified date of the object.
					for( int Index = 0; Index <= propertyValues.Length; Index++ )
					{
						// Get the last modified date in metadata card.
						var lastModifiedDateInCard = propertyValues[ Index ].TypedValue.DisplayValue.ToString();

						// Get the modified date of the object.
						var getObjectModifiedDate = objectVersion.LastModifiedDisplayValue.ToString();

						// Verify both date are equal.
						if( lastModifiedDateInCard == getObjectModifiedDate )
						{
							IsReturn = true;
							break;
						}
					}

					// Return Pass if both modified date and object date are equal.
					if( IsReturn )
						Report.LogPass( "Test Passed. Properties of the object is not empty" );
					else
						Assert.Fail( "Test Failed. Properties of the object is not valid" );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception.Message );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );

					Assert.Fail( exception.Message );
				}
				finally
				{
					ObjectVersion objectVersion = GetObjectVersion(
						returnTuple.Item1.GetValueForColumn( "ObjectNewName" ), 
						returnTuple.Item1.GetValueForColumn( "Path" ) );
					ObjVer externalObjVer = objectVersion.ObjVer;

					// Reverting the object name.
					this.Client.ObjectOperations.RenameObject(
						objectVersion, returnTuple.Item1.GetValueForColumn( "ObjectName" ) );

					// Demote the Object.
					mfilesApi.DemoteObjects( externalObjVer );

					// Login using specified user.
					mfilesApi.LoginToVault( testConfiguration.Users.User2.MFilesCredentials.Username,
						testConfiguration.Users.User2.MFilesCredentials.Password );
				}

			}

		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Renaming the external object." )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_TitleIsChangedAfterRenaming()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;
										
					// File Extension.
					string fileExtension = objectVersion.Files[ 0 ].Extension;

					// The new name for the object.
					var newName = "TestObject" + System.DateTime.Now.Millisecond.ToString();
					
					// Object to store the details of the file.
					MFilesAPI.FileVer fileVersion = mfilesApi.GetFileVer( objectVersion );

					// Get the object ID.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( mfilesObjID );

					// Checkout the object.
					MFilesAPI.ObjectVersion mfilesObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					// Change the name of the object.
					mfilesObjVersion = mfilesApi.vaultConnection.ObjectFileOperations.
						RenameFile( mfilesObjVersion.ObjVer, fileVersion, newName, fileExtension );

					// Check in the object.
					mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVersion.ObjVer );

					// Get the object version and the title of the renamed object.
					FolderContentItems subFolderContents = this.GetSubfolderContents( objectPath );

					// Get the object.
					objectVersion = this.GetObjectFromFolderContents( newName + "." + fileExtension, subFolderContents );

					// Verifies whether renaming the object is successfull or not.
					if( objectVersion.Title.Contains( newName ) )
						Report.LogPass( "Test Passed. " +
							"Renaming the object is successful and the new named object is " + objectVersion.Title );
					else
						Assert.Fail( "Test Failed. " +
							"Renaming the object is not successful and the name is " + objectVersion.Title );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}

		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeDocumentum" ), Category( "BugOneDriveForBusiness" ), Category( "ExcludeSharepointServer2016" ), Category( "ExcludeFileNetP8" )]
		[Description( "Renaming the external object as same name of another existence object." )]
		[Order( 2 )]
		public void BasicExternalConnectorVerification_RenamingWithInvalidChar()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// File Extension.
					string fileExtension = objectName.Split( '.' )[ 1 ];

					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the object version and the current name of the object.
					ObjectVersion objectVersion = this.GetObjectVersion( objectName, objectPath );
					externalObjVer = objectVersion.ObjVer;

					// The new name for the object.
					var newName = returnTuple.Item1.GetValueForColumn( "ObjectNewName" );
					
					// Object to store the details of the file.
					MFilesAPI.FileVer fileVersion = mfilesApi.GetFileVer( objectVersion );

					// Get the object ID.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( mfilesObjID );

					// Checkout the object.
					MFilesAPI.ObjectVersion mfilesObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					Report.LogInfo( "Step 1: Checked out the external object " + objectName );

					// Try to rename the object with invalid characters.
					try
					{
						// Change the name of the object.
						mfilesObjVersion = mfilesApi.vaultConnection.ObjectFileOperations.RenameFile
							( mfilesObjVersion.ObjVer, fileVersion,
							newName.Split( '.' )[ 0 ], fileExtension );

						// Try to check in the object.
						mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVersion.ObjVer );
					}
					catch( Exception excpetion )
					{
						// Verifies the exception message.
						if( excpetion.Message.Contains( returnTuple.Item1.GetValueForColumn( "Message" ) ) )
						{
							// Expected message.

							// Log the info in report.
							Report.LogInfo( "Renaming an object with the same name of another object " +
								"throws an error. Exception Message: " + excpetion.Message );

							// Undo checkout to revert the changes.
							mfilesObjVersion =
								mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjVersion.ObjVer );
						}
						else
						{
							// Undo checkout to revert the changes.
							mfilesObjVersion =
								mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjVersion.ObjVer );

							// Throws exception.
							throw excpetion;
						}
					}

					Report.LogInfo( "Step 2: Renaming the object with invalid name has been performed." );
					
					// Get the object version and the title of the renamed object.
					FolderContentItems subFolderContents = this.GetSubfolderContents( objectPath );

					// Get the object.
					objectVersion = this.GetObjectFromFolderContents( objectName, subFolderContents );
					
					// Verifies whether renaming the object is successfull or not.
					if( objectVersion != null )
						Report.LogPass( "Test Passed. Renaming the object same as another " +
							"file fails and the name is stil " + objectVersion.Title );
					else
						Assert.Fail( "Test Failed. Renaming the object same as another " +
							"file succeds and the name is " + objectVersion.Title );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

			}

		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeAllExceptNFC" )]
		[Description( "Search by metadata of unmanaged object." )]
		[Order( 3 )]
		public void BasicExternalConnectorVerification_UnmanagedObjectMetadata()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( NUnit.Framework.TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{

				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Stores the number of objects in search results.
					fileExistence = 0;
					
					int count = 0;

					// Waits for the indexing to happen.
					while( fileExistence <= 0 && count < 120 )
					{
						// Stores all the objects returned in the search operation.
						extObjVersion = this.Client.ObjectSearchOperations.SearchForObjectsByString( objectName );

						if( extObjVersion.Length != 0 )
						{
							// Verifies whether the expected object is present in the search list.
							foreach( var testc in extObjVersion )
							{
								// Verifies the properties of the object.
								if( testc.Class.Equals( -107 ) && testc.Title.ToString().Contains(
									objectName.Split( '.' )[ 0 ] ) && testc.EscapedTitleWithID.Contains( objectName.Split( '.' )[ 1 ] ) &&
									testc.IsInternal == false )
								{
									fileExistence++;  // Increments the value if the object is found.

								}
							}
							count++;
						}
						else
						{
							Thread.Sleep( 10000 );
							count++;
						}
					}

					Report.LogInfo( "Step 1: Search by metadatacard operation for unmanaged object was performed." );
					
					// Verifies the variable count.
					if( fileExistence > 0 )
						Report.LogPass( "Test Passed: " +
							"Search with metadata operation succeds for unmanaged object " + objectName );
					else
						Assert.Fail( "Test Failed: " +
							"Search with metadata operation fails for unmanaged object " + objectName );


				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeAllExceptNFC" )]
		[Description( "Search by metadata of managed object." )]
		[Order( 3 )]
		public void BasicExternalConnectorVerification_ManagedObjectMetadata()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( NUnit.Framework.TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{

				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Stores the number of objects in search results.
					fileExistence = 0;

					int count = 0;

					// Waits for the indexing to happen.
					while( fileExistence <= 0 && count < 120 )
					{
						// Stores all the objects returned in the searcg operation.
						extObjVersion =
							this.Client.ObjectSearchOperations.SearchForObjectsByString( objectName );

						if( extObjVersion.Length != 0 )
						{
							// Verifies whether the expected object is present in the search list.
							foreach( var testc in extObjVersion )
							{
								// Verifies the properties of the object.
								if( testc.Class.ToString().Equals( returnTuple.Item1.GetValueForColumn( "ClassID" ) ) &&
									testc.Title.Contains( objectName.Split( '.' )[ 0 ] ) &&
									testc.EscapedTitleWithID.Contains( objectName.Split( '.' )[ 1 ] ) )
								{
									fileExistence++;  // Increments the value if the object is found.
								}
							}
							count++;
						}
						else
						{
							Thread.Sleep( 10000 );
							count++;
						}
					}

					Report.LogInfo( "Step 1: Search by metadatacard operation for managed object was performed." );
					
					// Verifies the variable count.
					if( fileExistence > 0 )
						Report.LogPass( "Test Passed: " +
							"Search with metadata operation succeds for managed objects " + objectName );
					else
						Assert.Fail( "Test Failed:" +
							"Search with metadata operation fails for managed objects " + objectName );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeAllExceptNFC" )]
		[Description( "Search by filecontents of unmanaged object." )]
		[Order( 3 )]
		public void BasicExternalConnectorVerification_UnmanagedObjectFileContents()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( NUnit.Framework.TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{

				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Stores the number of objects in search results.
					fileExistence = 0;

					int count = 0;

					// Waits for the indexing to happen.
					while( fileExistence <= 0 && count < 120 )
					{
						// Stores all the objects returned in the search operation.
						extObjVersion = this.Client.ObjectSearchOperations.SearchForObjectsByString(
							returnTuple.Item1.GetValueForColumn( "SearchKeyWord" ) );

						if( extObjVersion.Length != 0 )
						{
							// Verifies whether the expected object is present in the search list.
							foreach( var testc in extObjVersion )
							{
								// Verifies the properties of the object.
								if( testc.Class.Equals( -107 ) &&
									testc.Title.ToString().Contains( objectName.Split( '.' )[ 0 ] ) &&
									testc.EscapedTitleWithID.Contains( objectName.Split( '.' )[ 1 ] ) &&
									testc.IsInternal == false )
								{
									fileExistence++;  // Increments the value if the object is found.
								}
							}
							count++;
						}
						else
						{
							Thread.Sleep( 10000 );
							count++;
						}
					}

					Report.LogInfo( "Step 1: Search by file contents operation for unmanaged object was performed." );
					
					// Verifies the variable count.
					if( fileExistence > 0 )
						Report.LogPass( "Test Passed: " +
							"Search with filecontents operation succeds for unmanaged objects " + objectName );
					else
						Assert.Fail( "Test Failed: " +
							"Search with filecontents operation fails for unmanaged objects " + objectName );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" ), Category( "ExcludeAllExceptNFC" )]
		[Description( "Search by filecontents of managed object." )]
		[Order( 3 )]
		public void BasicExternalConnectorVerification_ManagedObjectFileContents()
		{

			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( NUnit.Framework.TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{

				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the object name.
					string objectName = returnTuple.Item1.GetValueForColumn( "ObjectName" );

					// Stores the number of objects in search results.
					fileExistence = 0;
					
					int count = 0;

					// Waits for the indexing to happen.
					while( fileExistence <= 0 && count < 120 )
					{
						// Stores all the objects returned in the search operation.
						extObjVersion = this.Client.ObjectSearchOperations.SearchForObjectsByString(
							returnTuple.Item1.GetValueForColumn( "SearchKeyWord" ) );

						if( extObjVersion.Length != 0 )
						{
							// Verifies whether the expected object is present in the search list.
							foreach( var testc in extObjVersion )
							{
								// Verifies the properties of the object.
								if( testc.Class.ToString().Equals( returnTuple.Item1.GetValueForColumn( "ClassID" ) ) &&
									testc.Title.ToString().Contains( objectName.Split( '.' )[ 0 ] ) &&
									testc.EscapedTitleWithID.Contains( objectName.Split( '.' )[ 1 ] ) )
								{
									fileExistence++;  // Increments the value if the object is found.
								}
							}
							count++;
						}
						else
						{
							Thread.Sleep( 10000 );
							count++;
						}
					}

					Report.LogInfo( "Step 1: Search by file contents operation for managed object was performed." );
					
					// Verifies the variable count.
					if( fileExistence > 0 )
						Report.LogPass( "Test Passed: " +
							"Search with filecontents operation succeds for managed object " + objectName );
					else
						Assert.Fail( "Test Failed: " +
							"Search with filecontents operation fails for managed object " + objectName );

				}
				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Perform undo checkout operation and verify the status" )]
		[Order( 1 )]
		public void BasicExternalConnectorVerification_ObjCheckedInAfterUndoCheckOut()
		{
			foreach( Tuple<DataRow> returnTuple in
				MyDataSources.FactoryMethod( NUnit.Framework.TestContext.CurrentContext.Test.MethodName.ToString() ) )
			{
				try
				{
					// Create new node in extent report for separate test data.
					Report.LogHeader( testDataCount++ );
					
					// Stores the path of the object.
					if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
						objectPath = returnTuple.Item1.GetValueForColumn( "Path" );
					
					// Get the object.
					ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
					externalObjVer = objectVersion.ObjVer;

					// Stores the object name.
					string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

					// Get the object details.
					MFilesAPI.ObjID mfilesObjID = mfilesApi.GetObjID( externalObjVer );

					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( mfilesObjID );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// Ensure that the object is checked in.
					if( mfilesObjVersion.ObjectCheckedOut )
						Assert.Fail( "Object is not checked in" );

					// Checkout the object.
					mfilesObjVersion = mfilesApi.vaultConnection.ObjectOperations.CheckOut( mfilesObjID );

					// Ensure that the object is checked in.
					if( !mfilesObjVersion.ObjectCheckedOut )
						Assert.Fail( "Object is not checked out." );

					// Undo the check out of the object.
					mfilesObjVersion =
						mfilesApi.vaultConnection.ObjectOperations.UndoCheckout( mfilesObjVersion.ObjVer );

					// Stores the status of the object
					bool objectStatus = mfilesObjVersion.ObjectCheckedOut;
					
					// Verifies whether the object is checked out or not.
					if( !objectStatus )
						Report.LogPass( "Test Passed. " + "Staus of the object " + objectName +
							" after undo checkout operation is CheckedOut:" + objectStatus );
					else
						Assert.Fail( "Test Failed. Object is not checked in after undo checkout operation in done. " +
							"Staus of " + objectName + " is CheckedOut:" + objectStatus );

				}

				catch( Exception exception )
				{
					Console.WriteLine( exception );

					// Verifies the exception message.
					if( exception.Message.Contains( "Test Failed" ) )
						Report.LogFail( exception.Message );
					else
						Report.LogException( exception );
					
					Assert.Fail( exception.Message );
				}

				finally
				{
					// Get the object version.
					MFilesAPI.ObjVer mfilesObjVer = mfilesApi.GetLatestObjVer( externalObjVer );

					// Get the object version data.
					MFilesAPI.ObjectVersion mfilesObjVersion =
							mfilesApi.vaultConnection.ObjectOperations.GetObjectInfo( mfilesObjVer, true );

					// CheckIn the object if it is in checked out state.
					if( mfilesObjVersion.ObjectCheckedOut )
						mfilesApi.vaultConnection.ObjectOperations.CheckIn( mfilesObjVer );
				}
			}

		}

	}
}

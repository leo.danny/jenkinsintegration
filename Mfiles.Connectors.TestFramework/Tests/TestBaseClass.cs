using System.Collections.Generic;
using Mfiles.Connectors.TestFramework.Configuration;
using MFaaP.MFWSClient;
using NUnit.Framework;

namespace Mfiles.Connectors.TestFramework.Tests
{
	/// <summary>
	/// Base class for all integration tests.
	/// </summary>
	[TestFixture]
	[Parallelizable(ParallelScope.None)]
	internal class TestBaseClass
	{
		/// <summary>
		/// The client used for interacting with the M-Files API.
		/// </summary>
		public MFWSClient Client;      
        
        /// <summary>
        /// A collection of methods to help interacting with the M-Files server through the REST API.
        /// </summary>
        public MFHelperMethods _mfHelperMethods;

        ///// <summary>
        ///// Collection of object and view names.
        ///// </summary>
        //public ElementPathCollector ExternalElementCollector;

        /// <summary>
        /// The test configuration.
        /// </summary>
        public TestConfiguration _testConfiguration;

		[SetUp]
		public void BaseSetup()
		{
			// Get the test configuration.
			this._testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

			//// Set collection of object names and view names.
			//this.ExternalElementCollector = new ElementPathCollector( this._testConfiguration );

			// Set up the helper methods for interacting with the M-Files REST API.
			this._mfHelperMethods = this.CreateHelperMethodsForSpecificUser( Users.User1 );

			// Get the M-Files client.
			this.Client = this._mfHelperMethods.Client;
		}

		/// <summary>
		/// Creates an MFHelperMethods instance using the given user for credentials.
		/// </summary>
		/// <param name="user">The user to use for credentials.</param>
		/// <returns>A new instance of MFHelperMethods.</returns>
		protected MFHelperMethods CreateHelperMethodsForSpecificUser( Users user )
		{
			return new MFHelperMethods( this._testConfiguration, Setup.VaultId, user );
		}

		/// <summary>
		/// Returns the object with the given name if it exists in the folder contents.
		/// </summary>
		/// <param name="objectName">The name of the object to find.</param>
		/// <param name="parentFolderContents">The folder contents to search.</param>
		/// <returns>The object version of the requested object.</returns>
		/// <exception cref="KeyNotFoundException">Thrown if the object does not exists in the folder contents.</exception>
		protected ObjectVersion GetObjectFromFolderContents(
			string objectName,
			FolderContentItems parentFolderContents )
		{
			return this._mfHelperMethods.GetObjectFromFolderContents( objectName, parentFolderContents );
		}

		///// <summary>
		///// Gets the object version of a file.
		///// </summary>
		///// <param name="fileKey">The key file type of the file for a certain testing purpose.</param>
		///// <returns>The object version for the file with the file key.</returns>
		//protected ObjectVersion GetObjectVersion( ObjectType fileKey )
		//{
		//	return this._mfHelperMethods.GetObjectVersion( fileKey );
		//}

        /// <summary>
        /// Gets the object version of a file.
        /// </summary>
        /// <param name="objectName">The key file type of the file for a certain testing purpose.</param>
        /// <param name="objPath">The key file type of the file for a certain testing purpose.</param>
        /// <returns>The object version for the file with the file key.</returns>
        public ObjectVersion GetObjectVersion(string objectName, string objPath)
        {
            return this._mfHelperMethods.GetObjectVersion(objectName, objPath);
        }

        /// <summary>
        /// Gets the root folder contents.
        /// </summary>
        /// <returns>The root folder's contents.</returns>
        protected FolderContentItems GetRootFolderContents()
		{
			return this._mfHelperMethods.GetRootFolderContents();
		}

		///// <summary>
		///// Gets the folder content items of the folder with given name in the root.
		///// </summary>
		///// <param name="folderPath">Name of the folder in the root of the external repository.</param>
		///// <returns>The folder content items of the folder with given name.</returns>
		//protected FolderContentItems GetSubfolderContents( RelativeInternalElementPath folderPath )
		//{
		//	return this._mfHelperMethods.GetFolderContentsFromFolderInPath( folderPath.Folders.ToArray() );
		//}

        /// <summary>
        /// Gets the folder content items of the folder with given name in the root.
        /// </summary>
        /// <param name="folderPath">Name of the folder in the root of the external repository.</param>
        /// <returns>The folder content items of the folder with given name.</returns>
        protected FolderContentItems GetSubfolderContents(params string[] folderPath)
        {
            return this._mfHelperMethods.GetFolderContentsFromFolderInPath(folderPath);
        }

		/// <summary>
		/// Gets the folder content items of the folder with given name in the root.
		/// </summary>
		/// <param name="folderPath">Name of the folder in the root of the external repository.</param>
		/// <returns>The folder content items of the folder with given name.</returns>
		protected FolderContentItems GetSubfolderContents( string folderPath )
		{
			if( Setup.connectorName.Equals( "Sharepoint Online" ) )
			{
				folderPath = "Documents/" + folderPath;
			}
			else if( Setup.connectorName.Equals( "OneDriveForBusiness" ) )
			{
				// Appends the root folder with the path.
				folderPath = "Testi1 mfiles/" + folderPath;
			}
			else if( Setup.connectorName.Equals( "SharepointServer2016" ) )
			{
				// Appends the root folder with the path.
				folderPath = "Connector Test/Documents/" + folderPath;
			}

			string[] subfolderPath = folderPath.Split( '/' );
			return this._mfHelperMethods.GetFolderContentsFromFolderInPath( subfolderPath );
		}

		/// <summary>
		/// Gets the ObjId of the object.
		/// </summary>
		/// <param name="externalObjVer">Object version.</param>
		/// <returns>ObjId of the object.</returns>
		public ObjID GetObjID(ObjVer externalObjVer)
        {
            return this._mfHelperMethods.GetObjID(externalObjVer);
        }

		/// <summary>
		/// Gets the object version of an unmanaged object.
		/// </summary>
		/// <param name="objectPath">Object path.</param>
		/// <returns>The object version for the object.</returns>
		public ObjectVersion SelectUnmanagedObject( string objectPath )
		{
			FolderContentItems folderItems = GetSubfolderContents( objectPath );

			return this._mfHelperMethods.SelectUnmanagedObject( folderItems );
		}
	}
}

﻿using Mfiles.Connectors.TestFramework.Configuration;
using Mfiles.Connectors.TestFramework.TestRunSetup;
using MFNUnit.Utils.TestData;
using NUnit.Framework;
using System;
using MFaaP.MFWSClient;
using System.Data;
using MFNUnit.Utils.Logger;

namespace Mfiles.Connectors.TestFramework.Tests
{
	[TestFixture]
	[Order( 3 )]
	internal class ExternalOperationsWithOtherObjectType : TestBaseClass
	{

		/// <summary>
		/// Represents an object version.
		/// </summary>
		protected MFaaP.MFWSClient.ObjVer externalObjVer;
		
		/// <summary>
		/// Variable to store the test data.
		/// </summary>
		private Tuple<DataRow> returnTuple;

		/// <summary>
		/// Represents the object path.
		/// </summary>
		private string objectPath;

		/// <summary>
		/// Instance of the MFilesApiHelperMethods class.
		/// </summary>
		public MFilesApiHelperMethods mfilesApi;

		/// <summary>
		/// Variable to store testdata count and print in extent report.
		/// </summary>
		public int testDataCount;

		/// <summary>
		/// Stores test configuration..
		/// </summary>
		TestConfiguration testConfiguration;

		/// <summary>
		/// Stores the new class id.
		/// </summary>
		public int classID = 0;

		/// <summary>
		/// M-Files server application.
		/// </summary>
		MFilesAPI.MFilesServerApplication mfilesServer;

		/// <summary>
		/// Vault connection.
		/// </summary>
		MFilesAPI.Vault mfilesApiVaultConnection;

		[ OneTimeSetUp]
		public void OneTimeSetUp()
		{
			// Load test configuration.
			testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

			// Vault GUID.
			string compatibleVaultGuid = "{" + Setup.VaultId + "}";

			// Create an instance of the server application.
			mfilesServer = new MFilesAPI.MFilesServerApplication();

			// Connect the server with admin rights.
			mfilesServer.ConnectAdministrative();

			// Load the testconfiguration variables and values.
			ConnectorInstaller connectorConfiguration = new ConnectorInstaller( testConfiguration.ConnectorInstallationConfiguration, 
				testConfiguration.Users.AdminUser.ExternalCredentials,
				mfilesServer );

			// Log in to the vault as an administrator.
			mfilesApiVaultConnection = mfilesServer.LogInToVaultAdministrative( compatibleVaultGuid );
			
			// Creates new object reference for the class MFilesApiHelperMethods.
			mfilesApi = new MFilesApiHelperMethods();

			// Create new object type.
			MFilesAPI.ObjTypeAdmin newObjectType = mfilesApi.CreateNewObjectTypeInAdmin("NewObjectType", mfilesApiVaultConnection );

			// Guid of newly created object type.
			string newObjectTypeGUID = newObjectType.ObjectType.GUID;

			// Create new class and get the class id.
			classID = mfilesApi.CreateNewClass( newObjectType.ObjectType, mfilesApiVaultConnection );

			// Remove the IML configuration from MFAdmin.
			mfilesApiVaultConnection = connectorConfiguration.RemoveExtRepoConfiguration( mfilesApiVaultConnection );

			// Replace the Document class guid with the other object guid in configuration file.
			mfilesApi.ModifyTheObjectTypeInConfiguration( connectorConfiguration, newObjectTypeGUID );

			// Set the new configuration in MFAdmin.
			connectorConfiguration.SetConfiguration( mfilesApiVaultConnection );

			// Authenticate the new configuration.
			Setup.AuthenticateUsers( Setup.VaultId, testConfiguration, mfilesServer );
			
		}


		[SetUp]
		public void SetUp()
		{
			// Store testdata count and print in extent report
			testDataCount = 1;

			// Starts logging the extent report.
			Report.StartTest( Setup.connectorName );

			// Login using specified user.
			mfilesApi.LoginToVault( testConfiguration.Users.AdminUser.MFilesCredentials.Username,
				testConfiguration.Users.AdminUser.MFilesCredentials.Password );

			// Get the testdata for the current test.
			foreach( var data in MyDataSources.FactoryMethod( TestContext.CurrentContext.Test.MethodName.ToString() ) )
				returnTuple = ( Tuple<DataRow> ) data;
		}

		[TearDown]
		public void TearDown()
		{
			// Add the status and information into the extent report.
			Report.Flush();			
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Promoting object which are mapped under NewObjectType." )]
		[Order( 3 )]
		public void ExternalOperationsWithOtherObjectType_PromoteObjectOfNewObjectType()
		{
			try
			{
				// Create new node in extent report for separate test data.
				Report.LogHeader( testDataCount++ );

				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Stores the object name.
				string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension; ;

				Report.LogInfo( "Step 1: Fetched the version of the external object." );

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, classID.ToString() );

				Report.LogInfo( "Step 2: Promoted the external object " + objectName );

				// Get the objectVersion.
				objectVersion = GetObjectVersion( objectName, objectPath );

				// Verifies whether the object is promoted.
				if( objectVersion.Class.ToString().Equals( classID.ToString() ) )
					Report.LogPass( "Test Passed. Promoting the unmanaged object is successfull. " +
						"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );
				else
					Assert.Fail( "Test Failed.Promoting the unmanaged object got failed. " +
						"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );

			}

			catch( Exception exception )
			{
				Console.WriteLine( exception );

				// Verifies the exception message.
				if( exception.Message.Contains( "Test Failed" ) )
					Report.LogFail( exception.Message );
				else
					Report.LogException( exception );

				Assert.Fail( exception.Message );
			}
		}

		[Test]
		[Category( "Smoke" )]
		[Description( "Destroy the metadata of promoted object which are mapped under NewObjectType." )]
		[Order( 3 )]
		public void ExternalOperationsWithOtherObjectType_DemoteObjectOfNewObjectType()
		{
			try
			{
				// Create new node in extent report for separate test data.
				Report.LogHeader( testDataCount++ );

				// Stores the path of the object.
				if( returnTuple.Item1.GetValueForColumn( "Path" ) != null )
					objectPath = returnTuple.Item1.GetValueForColumn( "Path" );

				// Get the object.
				ObjectVersion objectVersion = this.SelectUnmanagedObject( objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Stores the object name.
				string objectName = objectVersion.Title + "." + objectVersion.Files[ 0 ].Extension;

				Report.LogInfo( "Step 1: Fetched the version of the external object." );

				// Promote the object.
				MFilesAPI.ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.PromoteObjects( externalObjVer, classID.ToString() );

				Report.LogInfo( "Step 2: Promoted the external object " + objectName );

				// Get the object.
				objectVersion = GetObjectVersion( objectName, objectPath );
				externalObjVer = objectVersion.ObjVer;

				// Verifies whether the object is promoted.
				if( !objectVersion.Class.ToString().Equals( classID.ToString() ) )
					Assert.Fail( "Test Failed.Promoting the object is failed." );

				// Destroy the metadatacard of the promoted object.
				mfilesApi.DemoteObjects( externalObjVer );

				Report.LogInfo( "Step 3: Destroyed the metadata of promoted object " + objectName );

				// Get the object.
				objectVersion = GetObjectVersion( objectName, objectPath );

				// Verifies whether the object is Demoted.
				if( objectVersion.Class.Equals( -107 ) &&
						objectVersion.IsInternal == false )
					Report.LogPass( "Test Passed.Demoting the promoted object is successfull. " +
						"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );
				else
					Assert.Fail( "Test Failed.Demoting the promoted object was failed. " +
						"Object " + objectName + " is of type Internal:" + objectVersion.IsInternal );

			}

			catch( Exception exception )
			{
				Console.WriteLine( exception );

				// Verifies the exception message.
				if( exception.Message.Contains( "Test Failed" ) )
					Report.LogFail( exception.Message );
				else
					Report.LogException( exception );

				Assert.Fail( exception.Message );
			}
		}
	}
}

﻿using System;
using System.Linq;
using Mfiles.Connectors.TestFramework.Configuration;
using MFilesAPI;

namespace Mfiles.Connectors.TestFramework.TestRunSetup
{
	/// <summary>
	/// Class responsible for authenticating the external users used by the M-Files users.
	/// </summary>
	public class Authenticator
	{
		/// <summary>
		/// The M-Files server.
		/// </summary>
		private MFilesServerApplication _mfServer;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="mfilesServer">The M-Files server.</param>
		public Authenticator(MFilesServerApplication mfilesServer)
		{
			this._mfServer = mfilesServer;
		}
		
		/// <summary>
		/// Authenticates the given list of special users with the external repository.
		/// </summary>
		/// <remarks>Will authenticate the users to all configured connectors in the vault.</remarks>
		/// <param name="vaultId">The id of the vault where the users should be authenticated.</param>
		/// <param name="externalUserCredentials">The credentials to be used when authenticating the users.</param>
		/// <param name="userTypesToAuthenticate">The user types that should be authenticated.</param>
		public void AuthenticateSpecialUsers(
			Guid vaultId,
			UserCredentialConfiguration externalUserCredentials,
			params MFExtensionAuthenticationSpecialUserType[] userTypesToAuthenticate )
		{
			// Log in to the vault as an administrator.
			// The GUID has to be in curly brackets. Otherwise it is seen as an invalid GUID by the server.
			Vault vaultConnection = this._mfServer.LogInToVaultAdministrative( "{" + vaultId + "}" ); 
			
			// Get all the configured connectors.
			ExtensionAuthenticationTargets configuredConnectors =
				vaultConnection.ExtensionAuthenticationOperations.GetExtensionAuthenticationTargets();
			
			// Authenticate all the given user types in all the configured connectors.
			foreach ( ExtensionAuthenticationTarget connector in configuredConnectors )
			{
				// Create the response.
				var response = new ExtensionAuthenticationResponse();

				// Add the name of the configuration.
				response.ConfigurationName = connector.PluginInfos[ 1 ].Name;

				// Add username and password.
				response.AuthenticationData[ "username" ] = externalUserCredentials.Username;
				response.AuthenticationData[ "password" ] = externalUserCredentials.Password;

				// Go through the list of user types and authenticate them.
				foreach ( MFExtensionAuthenticationSpecialUserType userType in userTypesToAuthenticate )
				{
					// Configure the credentials.
					vaultConnection.ExtensionAuthenticationOperations.StoreExtensionAuthenticationSpecialCredentials(
						userType,
						connector.ID,
						response );

					// Authenticate the special user.
					vaultConnection.ExtensionAuthenticationOperations.LogInWithExtensionAuthentication( connector.ID, response );
				}
			}
		}

		/// <summary>
		/// Authenticates all the configured standard users.
		/// </summary>
		/// <param name="vaultId">The id of the vault where the users should be authenticated.</param>
		/// <param name="configuredUsers">The configuration for all the users.</param>
		public void AuthenticateAllStandardUsers(
			Guid vaultId,
			TestUsersConfiguration configuredUsers )
		{
			// Authenticate each of the configured users.
			configuredUsers.AllStandardUserConfigurations.ForEach( c => this.AuthenticateSingleUser( vaultId, c ) );
		}

		/// <summary>
		/// Authenticates the given user with the external repository.
		/// </summary>
		/// <param name="vaultId">The id of the vault where the user should be authenticated.</param>
		/// <param name="user">The configuration M-Files and external credentials for the user.</param>
		private void AuthenticateSingleUser(
			Guid vaultId,
			UserConfiguration user )
		{
			// Log in to the vault as the given user.
			// The GUID has to be in curly brackets. Otherwise it is seen as an invalid GUID by the server.
			Vault vaultConnection = this._mfServer.LogInAsUserToVault(
				"{" + vaultId + "}",
				AuthType: MFAuthType.MFAuthTypeSpecificMFilesUser,
				UserName: user.MFilesCredentials.Username,
				Password: user.MFilesCredentials.Password );
			
			// Get all the configured connectors.
			ExtensionAuthenticationTargets configuredConnectors =
				vaultConnection.ExtensionAuthenticationOperations.GetExtensionAuthenticationTargets();
			
			// Authenticate the user in all the configured connectors.
			foreach ( ExtensionAuthenticationTarget connector in configuredConnectors )
			{
				// Create the response.
				var response = new ExtensionAuthenticationResponse();

				// Add the name of the configuration.
				response.ConfigurationName = connector.PluginInfos[ 1 ].Name;

				// Add username and password.
				response.AuthenticationData[ "username" ] = user.ExternalCredentials.Username;
				response.AuthenticationData[ "password" ] = user.ExternalCredentials.Password;

				// Authenticate the user.
				vaultConnection.ExtensionAuthenticationOperations.LogInWithExtensionAuthentication( connector.ID, response );
			}
		}
	}
}
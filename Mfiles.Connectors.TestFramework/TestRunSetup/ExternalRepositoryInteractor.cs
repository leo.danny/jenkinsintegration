﻿using System;
using Mfiles.Connectors.TestFramework.Configuration;
using Mfiles.Connectors.TestFramework.RepositoryStructure;

namespace Mfiles.Connectors.TestFramework.TestRunSetup
{
    /// <summary>
    /// Class responsible for interacting directly with the external repository without using the M-Files API.
    /// This class should be implemented based on the connector being tested.
    /// </summary>
    /// TODO: Implement this class.
    internal class ExternalRepositoryInteractor
    {
        /// <summary>
        /// Collector used for getting the paths to the test folders and objects.
        /// </summary>
        private ElementPathCollector _elementPathCollector;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="configuration">The configuration for the tests.</param>
        public ExternalRepositoryInteractor(TestConfiguration configuration)
        {
            // Guard against null values.
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            // Save the path collector.
            this._elementPathCollector = new ElementPathCollector(configuration);

            // TODO: Add any extra connector specific details here.
            //throw new NotImplementedException();
        }

        #region Methods to implement

        /// <summary>
        /// Creates an object in the external repository located at the given path. 
        /// </summary>
        /// <param name="relativePath">The relative path to the object.</param>
        /// TODO: Implement this method.
        public void CreateObject(RelativeExternalElementPath relativePath)
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a folder in the external repository located at the given path.
        /// </summary>
        /// <param name="relativePath">The relative path to the folder.</param>
        /// TODO: Implement this method.
        public void CreateFolder(RelativeExternalElementPath relativePath)
        {
            //throw new NotImplementedException();
        }

        #endregion

        #region Methods used by the framework

        /// <summary>
        /// Creates a folder in the external repository.
        /// </summary>
        /// <param name="folderKey">The key of the folder to create.</param>
        public void CreateFolder(FolderType folderKey)
        {
            // Collect the folder path.
            RelativeExternalElementPath path = this._elementPathCollector.GetRelativeExternalPathToFolder(folderKey);

            // Create the folder.
            this.CreateFolder(path);
        }

        /// <summary>
        /// Creates an object of the given object type in the external repository.
        /// </summary>
        /// <param name="objectKey">The key of object to create.</param>
        public void CreateObject(ObjectType objectKey)
        {
            // Collect the path to the object.
            RelativeExternalElementPath path = this._elementPathCollector.GetRelativeExternalPathToObject(objectKey);

            // Create the object.
            this.CreateObject(path);
        }

        #endregion

        #region Helper methods
        // TODO: Add helper methods here.
        #endregion
    }
}
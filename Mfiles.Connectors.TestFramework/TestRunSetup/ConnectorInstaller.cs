﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Mfiles.Connectors.TestFramework.Configuration;
using MFilesAPI;

namespace Mfiles.Connectors.TestFramework.TestRunSetup
{
	/// <summary>
	/// Class for connector installation.
	/// </summary>
	internal class ConnectorInstaller
	{
		/// <summary>
		/// Connector's application Guid.
		/// </summary>
		private string _applicationGuid;

		/// <summary>
		/// Path to the connector's configuration file.
		/// </summary>
		public string _connectorConfigurationPath;

		/// <summary>
		/// Path to the connector package.
		/// </summary>
		private string _connectorFilePath;

		/// <summary>
		/// To decide if connector configuration to be loaded.
		/// </summary>
		private string _connectorConfigRequired;

		/// <summary>
		/// Connector service Guid.
		/// </summary>
		private string _connectorServiceGuid;

		/// <summary>
		/// M-Files server application.
		/// </summary>
		private MFilesServerApplication _mfServer;

		/// <summary>
		/// Password for the external user.
		/// </summary>
		private string _passwordExternal;

		/// <summary>
		/// Username of the external user.
		/// </summary>
		private string _usernameExternal;

		/// <summary>
		/// Name of the vault.
		/// </summary>
		private string _vaultName;

		/// <summary>
		/// Path to the connector license.
		/// </summary>
		private string _licensePath;

		/// <summary>
		/// Constructor of the class.
		/// </summary>
		/// <param name="installationConfig">Configuration for the installation process.</param>
		/// <param name="externalAdminUserCredentials">
		/// The credentials for the external admin user.
		/// </param>
		/// <param name="mfilesServer">The M-Files server.</param>
		public ConnectorInstaller(
			ConnectorInstallationConfiguration installationConfig,
			UserCredentialConfiguration externalAdminUserCredentials,
			MFilesServerApplication mfilesServer )
		{
			this._connectorFilePath = installationConfig.ConnectorPath;
			this._connectorConfigurationPath = installationConfig.ConnectorConfigPath;
			this._connectorConfigRequired = installationConfig.ConnectorConfigurationRequired;

			this._applicationGuid = installationConfig.ApplicationGuid;
			this._connectorServiceGuid = installationConfig.ConnectorServiceGuid;
			this._usernameExternal = externalAdminUserCredentials.Username;
			this._passwordExternal = externalAdminUserCredentials.Password;
			this._vaultName = installationConfig.NameOfVaultToConnectTo;
			this._mfServer = mfilesServer;

			this._licensePath = installationConfig.ConnectorLicensePath;
		}

		/// <summary>
		/// Installs the connector to the vault with the given name.
		/// </summary>
		/// <returns>Vault id for the given vault.</returns>
		public Guid InstallConnector( TestConfiguration testConfiguration )
		{
			// Get access to the vault.
			IVaultOnServer vault = this._mfServer.GetOnlineVaults().GetVaultByName( this._vaultName );

			// Get the id of the vault.
			Guid vaultId = Guid.Parse( vault.GUID );

			// Install the connector if 'Connector Installation Required' is set to 'Yes' in configuration.
			if( testConfiguration.ConnectorInstallationConfiguration.ConnectorInstallationRequired.Equals( "Yes" ) )
			{
				// Install the connector.
				this.InstallConnectorToVault( vaultId );
			}

			// Return vault id.
			return vaultId;
		}

		/// <summary>
		/// Install connector to the vault.
		/// </summary>
		/// <returns>The vault guid.</returns>
		private void InstallConnectorToVault( Guid vaultId )
		{
			// Make the guid useable for server interactions.
			// The GUID has to be in curly brackets. Otherwise it is seen as an invalid GUID by the server.
			string compatibleVaultGuid = "{" + vaultId + "}";

			// Log in to the vault as an administrator.
			Vault vaultConnection = this._mfServer.LogInToVaultAdministrative( compatibleVaultGuid );

			// Install the connector.
			try
			{
				// Try to install the connector.
				vaultConnection.CustomApplicationManagementOperations.InstallCustomApplication( this._connectorFilePath );

				// Restart the vault connection.
				this.RestartVault( compatibleVaultGuid );

			}
			catch( COMException ex )
			{
				// Check which kind of error it is and act accordingly.
				if( ex.Message.StartsWith( "Already exists" ) )
				{
					// The version of the application that is being installed already exists in the vault. 
					// No extra actions are needed.
					Console.WriteLine(
						$"The version of the connector being installed to {this._vaultName} already exists in the vault. It will not be overwritten." );
				}
				else
				{
					// This is some other error. Rethrow it.
					throw;
				}

			}

			// Test the connection to the vault.
			try
			{
				vaultConnection.TestConnectionToVault();
			}
			catch
			{
				// The vault connection test has failed, therefore log in to the vault again.
				vaultConnection = this._mfServer.LogInToVaultAdministrative( compatibleVaultGuid );
			}

			// Install the license for the conenctor.
			this.InstallConnectorLicense( vaultId );

			// Create the initializing configuration if the 'Connector Configuration Required' value is selected as 'Yes'
			if( this._connectorConfigRequired.Equals( "Yes" ) )
				vaultConnection = this.SetConfiguration( vaultConnection );
		}

		/// <summary>
		/// Install connector to the vault.
		/// </summary>
		/// <param name="vaultId">The vault guid.</param>
		private void InstallConnectorLicense( Guid vaultId )
		{
			// Make the guid useable for server interactions.
			// The GUID has to be in curly brackets. Otherwise it is seen as an invalid GUID by the server.
			string compatibleVaultGuid = "{" + vaultId + "}";

			// Log in to the vault as an administrator.
			Vault vaultConnection = this._mfServer.LogInToVaultAdministrative( compatibleVaultGuid );

			try
			{
				// Load the file and store all the words in single line by ignoring the new lines.
				string licenseContent = string.Join( "", File.ReadAllLines( this._licensePath ) );

				// Stores the string used for splitting the license.
				string[] stringSeparators = new string[] { "---" };

				// Split the string and store it in a string array.
				string[] licenseCode = licenseContent.Split( stringSeparators, StringSplitOptions.None );

				// Install the conenctor license.
				vaultConnection.CustomApplicationManagementOperations.InstallCustomApplicationLicense( this._applicationGuid, licenseCode[ licenseCode.Length - 1 ] );
			}

			catch( Exception exception )
			{
				// Throw the error.
				throw exception;
			}
		}



		/// <summary>
		/// Restart the vault connection.
		/// </summary>
		/// <param name="guid">The guid.</param>
		private void RestartVault( string guid )
		{
			// Restart the vault.
			this._mfServer.VaultManagementOperations.TakeVaultOffline( guid, true );
			Console.WriteLine( $"Vault {this._vaultName} taken offline" );
			this._mfServer.VaultManagementOperations.BringVaultOnline( guid );
			Console.WriteLine( $"Vault {this._vaultName} back online" );
		}

		/// <summary>
		/// Set the connector configuration for the vault.
		/// </summary>
		/// <param name="vaultConnection">The vault connection.</param>
		/// <returns>The vault connection.</returns>
		public Vault SetConfiguration( Vault vaultConnection )
		{
			// Create named value type and also named values for initialization.
			var namedValueType = MFNamedValueType.MFSystemAdminConfiguration;
			var decl = new NamedValues();

			// Parse the configuration key name.
			string configKeyName = Guid.NewGuid().ToString();
			configKeyName = configKeyName.Replace( "-", "" ).ToLower();

			// Parse the name space.
			string nameSpace = this._applicationGuid + "." + this._connectorServiceGuid;
			nameSpace = nameSpace.Replace( "-", "" ).ToLower();

			// Initialize configuration.
			decl[ configKeyName ] = "";

			// Check if the configuration is already installed.
			NamedValues namedValues = vaultConnection.NamedValueStorageOperations.GetNamedValues( namedValueType, nameSpace );
			Strings names = namedValues.Names;

			// Set the initializing configuration.
			vaultConnection.NamedValueStorageOperations.SetNamedValues( namedValueType, nameSpace, decl );

			// Load the connector configuration.
			string confText = File.ReadAllText( this._connectorConfigurationPath );

			// Set the connector configuration.
			var config = new NamedValues();
			config[ "active" ] = confText;
			string configurationNameSpace = nameSpace + "." + configKeyName;
			vaultConnection.NamedValueStorageOperations.SetNamedValues( namedValueType, configurationNameSpace, config );

			// Restart the vault connection.
			this.RestartVault( vaultConnection.GetGUID() );

			// Log in to the vault again.
			vaultConnection = this._mfServer.LogInToVaultAdministrative( vaultConnection.GetGUID() );

			// Return the vault connection.
			return vaultConnection;
		}

		/// <summary>
		/// Remove the connector configuration for the vault.
		/// </summary>
		/// <param name="vaultConnection">The vault connection.</param>
		/// <returns>The vault connection.</returns>
		public Vault RemoveExtRepoConfiguration( Vault vaultConnection )
		{
			try
			{

				// Create named value type and also named values for initialization.
				var namedValueType = MFNamedValueType.MFSystemAdminConfiguration;
				var decl = new NamedValues();

				// Parse the name space.
				string nameSpace = this._applicationGuid + "." + this._connectorServiceGuid;
				nameSpace = nameSpace.Replace( "-", "" ).ToLower();

				// Check if the configuration is already installed.
				NamedValues namedValues = vaultConnection.NamedValueStorageOperations.GetNamedValues( namedValueType, nameSpace );
				Strings names = namedValues.Names;

				vaultConnection.NamedValueStorageOperations.RemoveNamedValues( namedValueType, nameSpace, names );

				// Restart the vault connection.
				this.RestartVault( vaultConnection.GetGUID() );

				// Log in to the vault again.
				vaultConnection = this._mfServer.LogInToVaultAdministrative( vaultConnection.GetGUID() );

				return vaultConnection;
			}
			catch(Exception ex)
			{
				// Throws exception.
				throw ex;
			}
		}
	}
}
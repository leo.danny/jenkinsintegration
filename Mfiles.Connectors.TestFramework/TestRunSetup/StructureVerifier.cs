﻿using System;
using System.Collections.Generic;
using Mfiles.Connectors.TestFramework.Configuration;
using Mfiles.Connectors.TestFramework.RepositoryStructure;

namespace Mfiles.Connectors.TestFramework.TestRunSetup
{
	/// <summary>
	/// Class responsible for verifying the structure of the test repository.
	/// </summary>
	public class StructureVerifier
	{
		/// <summary>
		/// Collector used for getting the paths to the test folders and objects.
		/// </summary>
		private ElementPathCollector _elementPathCollector;

		/// <summary>
		/// Helpers methods to interact with the M-Files API.
		/// </summary>
		private MFHelperMethods _helperMethods;

		/// <summary>
		/// Interactor used for interacting with the external repository.
		/// </summary>
		private ExternalRepositoryInteractor _interactor;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="configuration">The test configuration.</param>
		/// <param name="vaultId">The id of the test vault.</param>
		public StructureVerifier( 
			TestConfiguration configuration,
			Guid vaultId )
		{
			// Set up the helper methods for interacting with the M-Files REST API.
			this._helperMethods = new MFHelperMethods( configuration, vaultId, Users.Admin );
			this._elementPathCollector = new ElementPathCollector( configuration );

			// Create the external repository interactor.
			this._interactor = new ExternalRepositoryInteractor( configuration );
		}

		/// <summary>
		/// Verifies the structure of the external test repository and creates missing objects.
		/// </summary>
		public void VerifyTestStructure()
		{
			// Verify the folder structure and create missing folders.
			this.VerifyAndCreateFolderStructure();

			// Verify the object structure and create missing objects.
			this.VerifyAndCreateObjectStructure();
		}

		/// <summary>
		/// Creates the given folder in the external repository if it does not exist.
		/// </summary>
		/// <param name="internalPath">The M-Files path to the object.</param>
		/// <param name="externalPath">The external path to the object.</param>
		/// <param name="verifiedFolders">The set of folders that have been verified to exist.</param>
		private void CreateFolderIfItDoesNotExist(
			RelativeInternalElementPath internalPath,
			RelativeExternalElementPath externalPath,
			HashSet<string> verifiedFolders )
		{
			// Return straight away if the folder has already been verified.
			if ( verifiedFolders.Contains( internalPath.FolderPath ) ) return;

			// Check if the folder exists.
			bool folderExists = this.FolderDoesExist( internalPath );

			// Create the folder if it does not exist.
			if ( !folderExists )
				this._interactor.CreateFolder( externalPath );

			// Add the folder to the set of verified folders.
			verifiedFolders.Add( internalPath.FolderPath );
		}

		/// <summary>
		/// Creates the given object in the external repository if it does not exist.
		/// </summary>
		/// <param name="objectKey">The key for the object.</param>
		private void CreateObjectIfItDoesNotExist( ObjectType objectKey )
		{
			// Make sure the object exists.
			try
			{
				// Try to access the object.
				this._helperMethods.GetObjectVersion( objectKey );
			}
			catch ( KeyNotFoundException )
			{
				// The object was not found. Create it.
				this._interactor.CreateObject( objectKey );
			}
		}

		/// <summary>
		/// Verifies that the folder in the given path does exist.
		/// </summary>
		/// <param name="path">The path with the folder.</param>
		/// <returns>Flag indicating whether the folder exists.</returns>
		private bool FolderDoesExist( RelativeInternalElementPath path )
		{
			try
			{
				// Try to collect the contents from the folder.
				this._helperMethods.GetFolderContentsFromFolderInPath( path.Folders.ToArray() );
			}
			catch ( KeyNotFoundException )
			{
				// The folder was not found.
				return false;
			}

			// No error was thrown so the folder exists.
			return true;
		}

		/// <summary>
		/// Makes sure all needed folders exists. Will try to create them if they do not exist. 
		/// </summary>
		/// <remarks>
		/// Will both verify the folders specified specifically in the configuration and parent folders of test objects.
		/// </remarks>
		private void VerifyAndCreateFolderStructure()
		{
			// Create a cache to keep track of verified folders.
			var verifiedFolders = new HashSet<string>();

			// Make sure all folders exists that are specifically required in tests.
			foreach ( FolderType folderType in Enum.GetValues( typeof( FolderType ) ) )
			{
				// Get the paths of the folder.
				RelativeInternalElementPath internalPath = this._elementPathCollector.GetRelativeInternalPathToFolder( folderType );
				RelativeExternalElementPath externalPath = this._elementPathCollector.GetRelativeExternalPathToFolder( folderType );

				// Create the folder if it does not exist.
				this.CreateFolderIfItDoesNotExist( internalPath, externalPath, verifiedFolders );
			}

			// Make sure folders containing test objects exists.
			foreach ( ObjectType objectType in Enum.GetValues( typeof( ObjectType ) ) )
			{
				// Get the paths of the folder.
				RelativeInternalElementPath internalPath = this._elementPathCollector.GetRelativeInternalPathToObject( objectType );
				RelativeExternalElementPath externalPath = this._elementPathCollector.GetRelativeExternalPathToObject( objectType );

				// Create the folder if it does not exist.
				this.CreateFolderIfItDoesNotExist( internalPath, externalPath, verifiedFolders );
			}
		}

		/// <summary>
		/// Makes sure all needed test objects exists. Will try to create them if they do not exist.
		/// </summary>
		private void VerifyAndCreateObjectStructure()
		{
			// Go through the object types and make sure they exist.
			foreach ( ObjectType objectType in Enum.GetValues( typeof( ObjectType ) ) )
			{
				this.CreateObjectIfItDoesNotExist( objectType );
			}
		}
	}
}
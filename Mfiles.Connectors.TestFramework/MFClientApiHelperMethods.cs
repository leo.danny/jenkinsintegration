﻿using System;
using Mfiles.Connectors.TestFramework.Configuration;
using MFilesAPI;
using System.Collections.Generic;

namespace Mfiles.Connectors.TestFramework.Tests
{
	class MFClientApiHelperMethods
	{
		/// <summary>
		/// The M-Files client.
		/// </summary>
		MFilesClientApplication mfClient;

		/// <summary>
		/// Configuration for the test framework.
		/// </summary>
		private TestConfiguration testConfiguration;

		/// <summary>
		/// Credential configuration for users.
		/// </summary>
		private UserCredentialConfiguration mfilesStandardUserCredentials;

		/// <summary>
		/// Logged in instance of client.
		/// </summary>
		private Vault mfClientVault;

		/// <summary>
		/// Login to MFClient.
		/// </summary>
		/// <param name="userType">UserType for login.</param>
		/// <param name="userName">Name of the user for login.</param>
		/// <param name="passWord">Password assigned for the user.</param>
		/// <returns>Return Logged in client instance.</returns>
		public Vault LoginToMFClient( MFAuthType userType, string userName = null , string passWord = null)
		{
			try
			{
				// Load the test configuration.
				LoadConfiguration();

				// Login to MFClient using mentioned user credentials.
				mfClient = new MFilesClientApplication();

				// If username is null Client is logged in with admin user from test configuration.
				if( userName == "" || userName == null )
					mfClientVault = mfClient.LogInAsUser( Setup.vaultName, userType,
												mfilesStandardUserCredentials.Username, mfilesStandardUserCredentials.Password );
				else
					mfClientVault = mfClient.LogInAsUser( Setup.vaultName, userType,
												mfilesStandardUserCredentials.Username, mfilesStandardUserCredentials.Password );

				// Return Logged in client instance.
				return mfClientVault;
			}
			catch(Exception ex)
			{
				Console.WriteLine( "Error occured while logging into mfclient " + ex.Message );
				throw ex;
			}
		}

		/// <summary>
		/// Load test configuration.
		/// </summary>
		public void LoadConfiguration()
		{
			// Load test configuration.
			testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

			// Stores admin user credentials.
			mfilesStandardUserCredentials = testConfiguration.Users.User2.MFilesCredentials;

		}

		/// <summary>
		/// Get the properties of a built in view.
		/// </summary>
		/// <param name="builtInView">BuiltIn View.</param>
		/// <returns>Return the folder proeprties which has the view.</returns>
		public FolderDefs GetViewProperties( MFBuiltInView builtInView)
		{
			try
			{
				// Get the properties of the view.
				View viewProperties =
					this.mfClientVault.ViewOperations.GetBuiltInView( builtInView );
				
				// Stores the folder location of the view.
				FolderDefs viewFolderPath = new FolderDefs();
				FolderDef viewFolder = new FolderDef();
				viewFolder.SetView( viewProperties.ID );

				// Add the view to the folder definition.
				viewFolderPath.Add( -1, viewFolder );

				// Return the folder proeprties which has the view.
				return viewFolderPath;
			}
			catch(Exception ex)
			{
				Console.WriteLine( "Error occured while fetching the view properties " + ex.Message );
				throw ex;
			}
		}
	}
}

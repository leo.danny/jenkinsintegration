﻿using System;
using Mfiles.Connectors.TestFramework.Configuration;

namespace Mfiles.Connectors.TestFramework.RepositoryStructure
{
	/// <summary>
	/// Enumeration of the different folder types.
	/// </summary>
	internal enum FolderType
	{
		NonRoot
	}

	/// <summary>
	/// Enumeration for the different file types.
	/// </summary>
	internal enum ObjectType
	{
		Checkout,
		DeletedStatus,
		Properties,
		Download,
		Rename,
		Delete
	}

	/// <summary>
	/// Class for collecting the paths of the test objects and folders.
	/// </summary>
	internal class ElementPathCollector
	{
		/// <summary>
		/// The configuration for collecting folders.
		/// </summary>
		private FolderCollectionConfiguration _folderNameCollectionConfiguration;

		/// <summary>
		/// The configuration for collecting objects.
		/// </summary>
		private ObjectCollectionConfiguration _objectNameCollectionConfiguration;

		/// <summary>
		/// Constructor of the class.
		/// </summary>
		/// <param name="testConfiguration">The configuration for the tests.</param>
		public ElementPathCollector( TestConfiguration testConfiguration )
		{
			this._objectNameCollectionConfiguration = testConfiguration.ObjectNameCollectionConfiguration;
			this._folderNameCollectionConfiguration = testConfiguration.FolderNameCollectionConfiguration;
		}

		/// <summary>
		/// Returns the relative external path to the given folder.
		/// </summary>
		/// <param name="folderKey">The key for the folder to return the path for.</param>
		/// <returns>The path for the folder.</returns>
		public RelativeExternalElementPath GetRelativeExternalPathToFolder( FolderType folderKey )
		{
			switch ( folderKey )
			{
				case FolderType.NonRoot:
					return this._folderNameCollectionConfiguration.NonRootFolder.RelativeExternalPathToObject;

				default:
					throw new ArgumentOutOfRangeException( nameof( folderKey ), folderKey, null );
			}
		}

		/// <summary>
		/// Returns the relative external path to the given object.
		/// </summary>
		/// <param name="objectKey">The key for the object to return the path for.</param>
		/// <returns>The path for the object.</returns>
		public RelativeExternalElementPath GetRelativeExternalPathToObject( ObjectType objectKey )
		{
			switch ( objectKey )
			{
				case ObjectType.Checkout:
					return this._objectNameCollectionConfiguration.CheckOutObject.RelativeExternalPathToObject;

				case ObjectType.DeletedStatus:
					return this._objectNameCollectionConfiguration.DeletionStatusObject.RelativeExternalPathToObject;

				case ObjectType.Properties:
					return this._objectNameCollectionConfiguration.PropertiesObject.RelativeExternalPathToObject;

				case ObjectType.Download:
					return this._objectNameCollectionConfiguration.DownloadObject.RelativeExternalPathToObject;

				case ObjectType.Rename:
					return this._objectNameCollectionConfiguration.RenameObject.RelativeExternalPathToObject;

				case ObjectType.Delete:
					return this._objectNameCollectionConfiguration.DeleteObject.RelativeExternalPathToObject;

				default:
					throw new ArgumentOutOfRangeException( nameof( objectKey ), objectKey, null );
			}
		}

		/// <summary>
		/// Returns the relative internal path of the given folder.
		/// </summary>
		/// <param name="folderKey">The key of the folder to return.</param>
		/// <returns>The path to the folder.</returns>
		/// <exception cref="ArgumentOutOfRangeException">Thrown if an unknown key is given.</exception>
		public RelativeInternalElementPath GetRelativeInternalPathToFolder( FolderType folderKey )
		{
			switch ( folderKey )
			{
				case FolderType.NonRoot:
					return this._folderNameCollectionConfiguration.NonRootFolder.RelativeInternalPathToObject;

				default:
					throw new ArgumentOutOfRangeException( nameof( folderKey ), folderKey, null );
			}
		}

		/// <summary>
		/// Returns the relative internal path of the given object.
		/// </summary>
		/// <param name="objectKey">The key of the object to return.</param>
		/// <returns>The path to the object.</returns>
		/// <exception cref="ArgumentOutOfRangeException">Thrown if an unknown key is given.</exception>
		public RelativeInternalElementPath GetRelativeInternalPathToObject( ObjectType objectKey )
		{
			switch ( objectKey )
			{
				case ObjectType.Checkout:
					return this._objectNameCollectionConfiguration.CheckOutObject.RelativeInternalPathToObject;

				case ObjectType.DeletedStatus:
					return this._objectNameCollectionConfiguration.DeletionStatusObject.RelativeInternalPathToObject;

				case ObjectType.Properties:
					return this._objectNameCollectionConfiguration.PropertiesObject.RelativeInternalPathToObject;

				case ObjectType.Download:
					return this._objectNameCollectionConfiguration.DownloadObject.RelativeInternalPathToObject;

				case ObjectType.Rename:
					return this._objectNameCollectionConfiguration.RenameObject.RelativeInternalPathToObject;

				case ObjectType.Delete:
					return this._objectNameCollectionConfiguration.DeleteObject.RelativeInternalPathToObject;

				default:
					throw new ArgumentOutOfRangeException( nameof( objectKey ), objectKey, null );
			}
		}
	}
}
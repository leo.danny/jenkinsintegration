﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mfiles.Connectors.TestFramework.RepositoryStructure
{
	/// <summary>
	/// Container for element paths.
	/// </summary>
	public abstract class RelativeElementPath
	{
		/// <summary>
		/// The delimiter used in the paths.
		/// Has to be implemented in classes inheriting this class.
		/// </summary>
		/// TODO: Get this from configuration.
		protected abstract string PathDelimiter { get; }

		/// <summary>
		/// The name of the element the path is leading to.
		/// </summary>
		public string ElementName { get; }

		/// <summary>
		/// The name of the folders in the path to the element.
		/// If the element itself is a folder it is included in this list.
		/// </summary>
		public List<string> Folders { get; }

		/// <summary>
		/// The path to the element.
		/// </summary>
		public string Path { get; }

		/// <summary>
		/// Returns the path to the last folder in the path.
		/// If the element itself is a folder it is include in this path.
		/// </summary>
		public string FolderPath => string.Join( this.PathDelimiter, this.Folders );

		/// <summary>
		/// Flag indicating whether this is a path to a folder.
		/// </summary>
		public bool IsFolder { get; }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="path">The path to the element.</param>
		/// <param name="isFolder">Flag indicating whether this is a path to a folder. Default = false.</param>
		protected RelativeElementPath(
			string path,
			bool isFolder = false )
		{
			// Make sure the path delimiter is set.
			if ( this.PathDelimiter == null )
			{
				throw new ArgumentNullException(
					nameof( this.PathDelimiter ),
					"The path delimiter needs to be implemented"
				);
			}

			// Make sure the path does not start with a path delimiter.
			if ( path.StartsWith( this.PathDelimiter ) )
				throw new ArgumentException( $"The path cannot start with the character '{this.PathDelimiter}'" );

			// Get all the elements in the path.
			string[] pathElements = path.Split( new[] { this.PathDelimiter }, StringSplitOptions.None );

			// Set the name of the object.
			this.ElementName = pathElements.Last();

			// Set the is folder flag.
			this.IsFolder = isFolder;

			// Create the list of folders in the path.
			// If this is a path to a folder then all elements are added. Otherwise the last element is kept out.
			this.Folders = isFolder
				? pathElements.ToList()
				: pathElements.Take( pathElements.Length - 1 ).ToList();

			// Set the path.
			this.Path = path;
		}
	}
}
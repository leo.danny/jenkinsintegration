﻿namespace Mfiles.Connectors.TestFramework.RepositoryStructure
{
	/// <summary>
	/// Container for paths used in the M-Files API.
	/// </summary>
	public class RelativeInternalElementPath : RelativeElementPath
	{
		/// <summary>
		/// The delimiter used in the paths.
		/// </summary>
		protected override string PathDelimiter { get; } = "/";

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="path">The path to the element.</param>
		/// <param name="isFolder">Flag indicating whether this is a path to a folder. Default = false.</param>
		public RelativeInternalElementPath( string path, bool isFolder = false ) : base( path, isFolder ) { }
	}
}
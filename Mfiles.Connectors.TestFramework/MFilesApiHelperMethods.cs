﻿using System;
using Mfiles.Connectors.TestFramework.Configuration;
using Mfiles.Connectors.TestFramework.TestRunSetup;
using MFilesAPI;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Mfiles.Connectors.TestFramework.Tests
{
	class MFilesApiHelperMethods
	{

		/// <summary>
		/// The M-Files server.
		/// </summary>
		MFilesServerApplication mfServer;

		/// <summary>
		/// Configuration of the M-Files server.
		/// </summary>
		private ServerConfiguration serverConfiguration;

		/// <summary>
		/// Credential configuration for users.
		/// </summary>
		private UserCredentialConfiguration mfilesAdminCredentials;

		/// <summary>
		/// Configuration for the test framework.
		/// </summary>
		private TestConfiguration testConfiguration;

		// Collection of VaultOnServer objects.
		private IVaultOnServer vault;

		// Represents the document vault.
		public Vault vaultConnection;

		// Specifies the object version.
		private ObjVer objVer;

		// Represents object version data.
		private ObjectVersion objectVersion;

		// Represents an object ID.
		private ObjID objectInfo;

		// The Vault ID.
		public string vaultId;

		// The vault name.
		private String vaultName;

		/// <summary>
		/// Creates an instance of the class by authenticating to the server.
		/// </summary>
		public MFilesApiHelperMethods()
		{
			// Load test configuration.
			this.LoadConfiguration();

			// Connects to the server.
			this.AuthenticatingServer();
		}

		/// <summary>
		/// Promote the unmanaged object by changing the class value.
		/// </summary>
		/// <param name="externalObjVer">MFWS Object version of an object.</param>
		/// <param name="classID">ID of the class assigned while promoting.</param>
		public ObjectVersionAndProperties PromoteObjects( MFaaP.MFWSClient.ObjVer externalObjVer,
			string classID )
		{

			try
			{
				// Get the identifier of the object.
				objectInfo = this.GetObjID( externalObjVer );

				// Get the latest version of the object.
				objVer = GetLatestObjVer( objectInfo );

				// Stores the property need to be assigned to promote the objects.
				PropertyValue objectProperty = new PropertyValue();
				objectProperty.PropertyDef = 100;
				objectProperty.TypedValue.SetValue( MFDataType.MFDatatypeLookup, classID );

				// Add the values to the object fopr promoting.
				PropertyValues setProperty = new PropertyValues();
				setProperty.Add( 1, objectProperty );

				// Promte the objects with the given property definition.
				ObjectVersionAndProperties objVersionAndProperties =
					vaultConnection.ExternalObjectOperations.PromoteObject( objVer,
									setProperty, MFACLEnforcingMode.MFACLEnforcingModeAutomatic );

				return objVersionAndProperties;


			}
			catch( Exception exception )
			{
				// Prints the exception occured in console.
				Console.WriteLine( "Error occured while promoting the object." + exception.Message );

				// Throws the occured exception.
				throw exception;
			}
		}

		/// <summary>
		/// Promote the unmanaged object by changing the class value.
		/// </summary>
		/// <param name="mfilesObjVer">MFiles API Object version of an object.</param>
		/// <param name="classID">ID of the class assigned while promoting.</param>
		public void PromoteObjects( ObjVer mfilesObjVer,
			string classID )
		{

			try
			{
				// Stores the property need to be assigned to promote the objects.
				PropertyValue objectProperty = new PropertyValue();
				objectProperty.PropertyDef = 100;
				objectProperty.TypedValue.SetValue( MFDataType.MFDatatypeLookup, classID );

				// Add the values to the object fopr promoting.
				PropertyValues setProperty = new PropertyValues();
				setProperty.Add( 1, objectProperty );

				// Promte the objects with the given property definition.
				vaultConnection.ExternalObjectOperations.PromoteObject( mfilesObjVer,
									setProperty, MFACLEnforcingMode.MFACLEnforcingModeAutomatic );

			}
			catch( Exception exception )
			{
				// Prints the exception occured in console.
				Console.WriteLine( "Error occured while promoting the object." + exception.Message );

				// Throws the occured exception.
				throw exception;
			}
		}

		/// <summary>
		/// Destroy the metadata of promoted object.
		/// </summary>
		/// <param name="externalObjVer">Object version of an object.</param>
		public void DemoteObjects( MFaaP.MFWSClient.ObjVer externalObjVer )
		{
			try
			{
				// Get the identifier of the object.
				objectInfo = this.GetObjID( externalObjVer );

				// Set the object information for demoting.
				ObjIDs setObjectInfo = new ObjIDs();
				setObjectInfo.Add( -1, objectInfo );

				// Destroy the metadata of promoted objects.
				vaultConnection.ExternalObjectOperations.DemoteObjects( setObjectInfo );

			}
			catch( Exception exception )
			{
				// Prints the exception occured in console.
				Console.WriteLine( "Error occured while demoting the object." + exception.Message );

				// Throws the occured exception.
				throw exception;
			}
		}

		/// <summary>
		/// Load test configuration.
		/// </summary>
		public void LoadConfiguration()
		{
			// Load test configuration.
			testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

			// Set the vault name.
			vaultName = testConfiguration.ConnectorInstallationConfiguration.NameOfVaultToConnectTo;

			// Collect needed configurations.
			serverConfiguration = testConfiguration.ServerConfiguration;

			// Stores admin user credentials.
			mfilesAdminCredentials = testConfiguration.Users.AdminUser.MFilesCredentials;

		}

		/// <summary>
		/// Connects to the server.
		/// </summary>
		public void AuthenticatingServer()
		{
			// Collect info for logging.
			string fullServerAddress =
				$"{serverConfiguration.ServerNetworkAddress}:{serverConfiguration.ServerEndpoint}";

			// Connect to the server.
			Console.WriteLine( $"Connecting to the server at {fullServerAddress}" );
			mfServer = new MFilesServerApplication();
			mfServer.ConnectAdministrative(
				AuthType: MFilesAPI.MFAuthType.MFAuthTypeSpecificMFilesUser,
				UserName: mfilesAdminCredentials.Username,
				Password: mfilesAdminCredentials.Password,
				NetworkAddress: serverConfiguration.ServerNetworkAddress,
				Endpoint: serverConfiguration.ServerEndpoint );

		}

		/// <summary>
		/// Login to the vault from server.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="passWord">Password assigned to the user.</param>
		public void LoginToVault( string userName,
			string passWord )
		{
			// Get access to the vault.
			vault = mfServer.GetOnlineVaults().GetVaultByName( vaultName );

			// Get the id of the vault.
			vaultId = Guid.Parse( vault.GUID ).ToString();

			// LogIn to the vault with the given user.
			vaultConnection = mfServer.LogInAsUserToVault(
				"{" + vaultId + "}",
				AuthType: MFAuthType.MFAuthTypeSpecificMFilesUser,
				UserName: userName,
				Password: passWord );
		}

		/// <summary>
		/// Gets the latest object version.
		/// </summary>
		/// <param name="objectInfo">Information about the object.</param>
		/// <returns>The object version for the object with the given object.</returns>
		public ObjVer GetLatestObjVer( ObjID objectInfo )
		{
			// Get the object.
			objVer = vaultConnection.ObjectOperations.GetLatestObjVer( objectInfo, false, true );

			// Return the object version of an object.
			return objVer;
		}

		/// <summary>
		/// Gets the latest object version.
		/// </summary>
		/// <param name="externalObjVer">Information about the object.</param>
		/// <returns>The object version for the object with the given object.</returns>
		public ObjVer GetLatestObjVer( MFaaP.MFWSClient.ObjVer externalObjVer )
		{
			// Get the object details.
			objectInfo = this.GetObjID( externalObjVer );

			// Get the object.
			objVer = vaultConnection.ObjectOperations.GetLatestObjVer( objectInfo, false, true );

			// Return the object version of an object.
			return objVer;
		}

		/// <summary>
		/// Convert the given object to PDF.
		/// </summary>
		/// <param name="externalObjVer">Information about the object.</param>
		/// <param name="fileID">FileFD the object.</param>
		/// <returns>The object version for the object with the given object.</returns>
		public ObjectVersion ConvertToPDF( MFaaP.MFWSClient.ObjVer externalObjVer, int fileID )
		{
			try
			{
				// Identifier of an object..
				objectInfo = this.GetObjID( externalObjVer );

				// Checkout the object.
				objectVersion = this.CheckOutObject( objectInfo );

				// Gets information on the specified object version..
				objectVersion = this.GetObjectDetails( objectVersion.ObjVer, true, true );

				// Convert the object to PDF format.
				objectVersion = vaultConnection.ObjectFileOperations.ConvertToPDF( objectVersion.ObjVer, fileID );

				// CheckIn the object.
				objectVersion = this.CheckInObject( objectVersion.ObjVer );

				// Return the object.
				return objectVersion;
			}
			catch( Exception exception )
			{
				// Prints the exception occured in console.
				Console.WriteLine( "Error occured while converting object into PDF" + exception.Message );

				// Throws the occured exception.
				throw exception;
			}
		}

		/// <summary>
		/// Get the object details.
		/// </summary>
		/// <param name="externalObjVer">Information about the object.</param>
		/// <returns>The information about the object.</returns>
		public ObjID GetObjID( MFaaP.MFWSClient.ObjVer externalObjVer )
		{
			// Initaites the ObjID class to store the information about the object.
			objectInfo = new ObjID();

			// Assign the infromation about the object.
			if( !externalObjVer.IsInternal )
			{
				// External Object.

				// Stores the information about the object.
				objectInfo.ExternalRepositoryName = externalObjVer.ExternalRepositoryName.ToString();
				objectInfo.ExternalRepositoryObjectID = externalObjVer.ExternalRepositoryObjectID.ToString();
			}

			// Stores the information about the object.
			objectInfo.ID = externalObjVer.ID;
			objectInfo.Type = externalObjVer.Type;

			// Return object details.
			return objectInfo;
		}

		/// <summary>
		/// CheckOut the object.
		/// </summary>
		/// <param name="objectInfo">Information about the object.</param>
		/// <returns>The object version for the object with the given object.</returns>
		public ObjectVersion CheckOutObject( ObjID objectInfo )
		{
			// Checkout the given object.
			this.objectVersion = this.vaultConnection.ObjectOperations.CheckOut( objectInfo );

			// Return the object.
			return objectVersion;
		}

		/// <summary>
		/// CheckIn the object.
		/// </summary>
		/// <param name="objVer">Object version of the object.</param>
		/// <returns>The object version for the object with the given object.</returns>
		public ObjectVersion CheckInObject( ObjVer objVer )
		{
			// Checkin the given object.
			this.objectVersion = this.vaultConnection.ObjectOperations.CheckIn( objVer );

			// Return the object.
			return objectVersion;
		}

		/// <summary>
		/// Gets information on the specified object version..
		/// </summary>
		/// <param name="objVer">Object version of the object.</param>
		/// <param name="latestVersion">Boolean value indicating latest version or not.</param>
		/// <param name="updateFromServer">Boolean value indicating updated version from server.</param>
		/// <returns>The object version for the object with the given object.</returns>
		public ObjectVersion GetObjectDetails( ObjVer objVer,
			bool latestVersion,
			bool updateFromServer )
		{
			// Get object details.
			objectVersion = this.vaultConnection.ObjectOperations.GetObjectInfo( objVer, true, true );

			// Return the object.
			return objectVersion;
		}

		/// <summary>
		/// Change the permissions of the object.
		/// </summary>
		/// <param name="externalObjVer">Information about the object.</param>
		public void EditPermissions( MFaaP.MFWSClient.ObjVer externalObjVer, int namedACLID )
		{
			// Identifier of an object..
			objectInfo = this.GetObjID( externalObjVer );

			// Get the latest object version.
			objVer = GetLatestObjVer( objectInfo );

			// Change the object permissions.
			objectVersion =
				this.vaultConnection.ObjectOperations.ChangePermissionsToNamedACL( objVer, namedACLID, true );
		}

		/// <summary>
		/// Change the permissions of the object.
		/// </summary>
		/// <param name="externalObjVer">Information about the object version.</param>
		/// <returns>Permission of the object.</returns>
		public ObjectVersionPermissions GetObjectPermission( MFaaP.MFWSClient.ObjVer externalObjVer )
		{
			// Identifier of an object..
			objectInfo = this.GetObjID( externalObjVer );

			// Get the latest object version.
			objVer = GetLatestObjVer( objectInfo );

			// Get the object permission.
			ObjectVersionPermissions perm = this.vaultConnection.ObjectOperations.GetObjectPermissions( objVer );

			// Return object permission.
			return perm;
		}

		/// <summary>
		/// Getting the Object History Count
		/// </summary>
		/// <param name="externalObjVer">Passing the object version</param>
		/// <returns>The Object History Count.</returns>
		public int GetObjectHistoryCount( MFaaP.MFWSClient.ObjVer externalObjVer )
		{
			// Stores the information about the object.
			ObjID obectInfo = this.GetObjID( externalObjVer );

			//Get the Object History information.
			var ObjectHistory = vaultConnection.ObjectOperations.GetHistory( obectInfo );

			//Get the Object History Count.
			int ObjHistoryCount = ObjectHistory.Count;

			//Return the Object History Count.
			return ObjHistoryCount;
		}

		/// <summary>
		/// Getting the file version details
		/// </summary>
		/// <param name="objectVersion">Passing the object version</param>
		/// <returns>A single file version details.</returns>
		public FileVer GetFileVer( MFaaP.MFWSClient.ObjectVersion objectVersion )
		{
			// Object to store the details of the file version.
			FileVer fileVersion = new MFilesAPI.FileVer();
			fileVersion.ExternalRepositoryFileID = objectVersion.Files.ToArray()[ 0 ].ExternalRepositoryFileID;
			fileVersion.ID = objectVersion.Files.ToArray()[ 0 ].ID;
			fileVersion.Version = objectVersion.Files.ToArray()[ 0 ].Version;

			// A single file version details.
			return fileVersion;
		}


		/// <summary>
		/// Add users to the vault with given permissions.
		/// </summary>
		/// <param name="userConfiguration">The configuration of the users.</param>
		/// <param name="userName">User to be added.</param>
		/// <param name="windowsUser">User is Admin/Normal user.</param>
		public void AddUserToVault( string userName,
				bool windowsUser )
		{
			try
			{
				// Object to store the login session of the vault.
				Vault userLoginSession;

				// Stores the existence of the user account.
				int accountExist = 0;

				// Object to store the properties of the user account.
				UserAccount userProperties = new UserAccount();

				TestUsersConfiguration userConfiguration = testConfiguration.Users;

				// Assigns the permission for the user.
				if( userName == userConfiguration.AdminUser.MFilesCredentials.Username || windowsUser )
					// Admin user.
					userProperties.VaultRoles = MFUserAccountVaultRole.MFUserAccountVaultRoleFullControl;
				else
					// Normal user.
					userProperties.VaultRoles = MFUserAccountVaultRole.MFUserAccountVaultRoleDefaultRoles;

				// Properties of the user account.
				userProperties.LoginName = userName;
				userProperties.InternalUser = true;
				userProperties.Enabled = true;

				// See if the vault exists.
				if( this.mfServer.GetOnlineVaults().GetVaultIndexByName( this.vaultName ) != -1 )
				{
					// Login to the vault.
					userLoginSession = this.mfServer.LogInToVaultAdministrative(
						  this.mfServer.GetOnlineVaults().GetVaultByName( this.vaultName ).GUID );

					// Get the user account in the vault.
					UserAccounts userAccounts = userLoginSession.UserOperations.GetUserAccounts();

					// Verify the existence of the user account.
					foreach( UserAccount account in userAccounts )
					{
						// Modify the user account properties if it already exists.
						if( account.LoginName == userName )
						{
							// Assigns the user ID.
							userProperties.ID = account.ID;

							// Modify the user account properties.
							userLoginSession.UserOperations.ModifyUserAccount( userProperties );

							// Increments the variable and exists the loop.
							accountExist++;
							break;
						}
					}

					// Create new user account in vault if it doesnot exists.
					if( accountExist == 0 )
						userLoginSession.UserOperations.AddUserAccount( userProperties );
				}
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Assign the permissions and role of the user and add to the server.
		/// </summary>
		/// <param name="userCredentials">User credentials.</param>
		/// <param name="AdminUser">User is Admin/Normal user.</param>
		/// <param name="licenseType">License type of the user.</param>
		public void AddLoginAccount( UserCredentialConfiguration userCredentials,
			bool AdminUser, MFLicenseType licenseType )
		{
			try
			{
				// Existence of the account.
				bool accountExists = false;

				// Stores the instance of the MFilesServerApplication.
				var serverConnection = new MFilesServerApplication();

				// Object to store the login account information.
				LoginAccount userInformation = new LoginAccount();

				// Verify the user needs admin role.
				if( AdminUser )
				{
					// Admin User.
					userInformation.ServerRoles = MFLoginServerRole.MFLoginServerRoleSystemAdministrator;
				}
				else
				{
					// Normal User.
					userInformation.ServerRoles = MFLoginServerRole.MFLoginServerRoleNone;
				}

				// Collects the information about the users.
				userInformation.UserName = userCredentials.Username;
				userInformation.LicenseType = licenseType;
				userInformation.AccountType = MFLoginAccountType.MFLoginAccountTypeMFiles;
				userInformation.Enabled = true;

				// Get all the existing login accounts from the server.
				LoginAccounts loginAccounts = this.GetAllLoginAccounts( mfServer, testConfiguration.Users );

				// Verify whether the account already exists or not.
				foreach( LoginAccount acount in loginAccounts )
				{
					// Compares the user name.
					if( acount.UserName.Equals( userCredentials.Username ) )
					{
						// Set the flag if the user exists.
						accountExists = true;
						break;
					}
				}

				// Add or update the login account based on the existence.
				if( accountExists )
				{
					// Account exists.

					// Modify the user roles and assign the password.
					mfServer.LoginAccountOperations.ModifyLoginAccount( userInformation );
					mfServer.LoginAccountOperations.UpdateLoginPassword(
						userCredentials.Username, userCredentials.Password );
				}
				else
				{
					// Account does not exists.

					// Add new login account.
					mfServer.LoginAccountOperations.AddLoginAccount( userInformation, userCredentials.Password );
				}

			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Get all the existing login account from the server.
		/// </summary>
		/// <param name="serverConnection">Instance of the server connection.</param>
		/// <param name="userConfiguration">The configuration of the users.</param>
		/// <returns>The available login accounts.</returns>
		public LoginAccounts GetAllLoginAccounts( MFilesServerApplication mfServer,
			TestUsersConfiguration userConfiguration )
		{
			try
			{
				// Get all the available login accounts from the server.
				LoginAccounts loginAccounts = mfServer.LoginAccountOperations.GetLoginAccounts();

				// Return the login accounts.
				return loginAccounts;
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Add the comment to external objects.
		/// </summary>
		/// <param name="mfilesApi">Instance of the logged in user.</param>
		/// <param name="objVer">The object.</param>
		/// <param name="dataType">Data type of the comment.</param>
		/// <param name="commentValue">Comment to be inserted.</param>
		/// <returns>The version properties of the object.</returns>
		public ObjectVersionAndProperties SetCommentToExternalObject(
			MFilesApiHelperMethods mfilesApi,
			ObjVer objVer,
			MFDataType dataType,
			string commentValue )
		{
			try
			{
				// Set the type and value of the comment.
				TypedValue setCommentValue = new TypedValue();
				setCommentValue.SetValue( dataType, commentValue );

				// Set the typed value in comment property definition.
				PropertyValue propertyValue = new PropertyValue();
				propertyValue.PropertyDef = 33;
				propertyValue.TypedValue = setCommentValue;

				// Add the comment to the external object.
				ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.vaultConnection.ObjectPropertyOperations.SetVersionComment( objVer, propertyValue );

				// Return the version property of the object.
				return objVersionAndProperties;
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Add the property to external objects metadata.
		/// </summary>
		/// <param name="mfilesApi">Instance of the logged in user.</param>
		/// <param name="objVer">The object.</param>
		/// <param name="dataType">Data type of the comment.</param>
		/// <param name="propertyDefID">Property definition ID.</param>
		/// <param name="value">Value to be updated.</param>
		/// <returns>The version properties of the object.</returns>
		public ObjectVersionAndProperties SetPropertyToExternalObject(
			MFilesApiHelperMethods mfilesApi,
			ObjVer objVer,
			MFDataType dataType,
			int propertyDefID,
			string value )
		{
			try
			{
				// Set the type and value of the property.
				TypedValue setPropertyValue = new TypedValue();
				setPropertyValue.SetValue( dataType, value );

				// Set the typed value in property definition.
				PropertyValue propertyValue = new PropertyValue();
				propertyValue.PropertyDef = propertyDefID;
				propertyValue.TypedValue = setPropertyValue;

				// Add the property to the external object.
				ObjectVersionAndProperties objVersionAndProperties =
					mfilesApi.vaultConnection.ObjectPropertyOperations.SetProperty( objVer, propertyValue );

				// Return the version property of the object.
				return objVersionAndProperties;
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Creates new object type in MFAdmin.
		/// </summary>
		/// <param name="objectTypeName">Title of new Object Type.</param>
		/// <param name="mfilesApiVaultConnection">Instance of the logged in Vault.</param>
		/// <returns>The instance of new object type.</returns>
		public ObjTypeAdmin CreateNewObjectTypeInAdmin( 
			string objectTypeName, 
			Vault mfilesApiVaultConnection )
		{
			try
			{
				// New instance for creating new object type.
				ObjTypeAdmin newObjectType = new ObjTypeAdmin();
				
				// Properties of new object type.
				ObjType newObjectTypeInfo = new ObjType();
				newObjectTypeInfo.NameSingular = objectTypeName;
				newObjectTypeInfo.NamePlural = objectTypeName + "s";
				newObjectTypeInfo.RealObjectType = true;
				newObjectTypeInfo.AllowAdding = true;
				newObjectTypeInfo.CanHaveFiles = true;
				newObjectType.ObjectType = newObjectTypeInfo;

				// Create new object type in MFAdmin.
				newObjectType = mfilesApiVaultConnection.ObjectTypeOperations.AddObjectTypeAdmin( newObjectType );
				
				// Return the instance of new object type.
				return newObjectType;
			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Creates new Class in MFAdmin.
		/// </summary>
		/// <param name="objectType">Object Type for which the class has to be created.</param>
		/// <param name="mfilesApiVaultConnection">Instance of the logged in Vault.</param>
		/// <returns>The ID of new class.</returns>
		public int CreateNewClass( 
			ObjType objectType, 
			Vault mfilesApiVaultConnection )
		{
			try
			{
				// New instance for creating new class.
				ObjectClassAdmin newClass = new ObjectClassAdmin();

				// Properties of new class.
				newClass.Name = objectType.NameSingular;
				newClass.ObjectType = objectType.ID;

				// Create new class in MFAdmin.
				newClass = mfilesApiVaultConnection.ClassOperations.AddObjectClassAdmin( newClass );

				// Returns the ID of new class.
				return newClass.ID;

			}
			catch( Exception ex )
			{
				// Throws exception.
				throw ex;
			}
		}

		/// <summary>
		/// Replace the Document class guid with the other object guid in configuration file.
		/// </summary>
		/// <param name="connectorConfiguration">Instance of ConnectorInstaller Class.</param>
		/// <param name="newObjectTypeGUID">GUID of new object type.</param>
		public void ModifyTheObjectTypeInConfiguration(
			ConnectorInstaller connectorConfiguration, 
			string newObjectTypeGUID )
		{
			try
			{
				// Read the configuration content.
				string confText = File.ReadAllText( connectorConfiguration._connectorConfigurationPath );

				// Parse the content in JSON format.
				JObject rss = JObject.Parse( confText );

				// Select the child node which has the Document class GUID.
				JToken furnitures = rss.SelectToken( "declaration" ).SelectToken( "mappingOptions" ).SelectToken( "objectTypes" );

				// Replace the Document class GUID with the New class GUId.
				furnitures[ 0 ][ "objectType" ] = newObjectTypeGUID;

				// Set the new configuration path.
				connectorConfiguration._connectorConfigurationPath = connectorConfiguration._connectorConfigurationPath.Replace( ".json", "1.json" );

				// Save the configuration file.
				File.WriteAllText( connectorConfiguration._connectorConfigurationPath, rss.ToString() );
			}
			catch(Exception ex)
			{
				// Throws exception.
				throw ex;
			}
		}
	}
}

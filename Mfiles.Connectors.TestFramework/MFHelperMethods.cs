﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mfiles.Connectors.TestFramework.Configuration;
using MFaaP.MFWSClient;
using Mfiles.Connectors.TestFramework.Tests;
using NUnit.Framework;

namespace Mfiles.Connectors.TestFramework
{
    /// <summary>
    /// A collection of helper methods to interact with the M-Files API.
    /// </summary>
    internal class MFHelperMethods
    {
        /// <summary>
        /// Client used to interact with M-Files through the REST API.
        /// </summary>
        public MFWSClient Client { get; }

        ///// <summary>
        ///// The collector used for getting the names of objects.
        ///// </summary>
        //private ElementPathCollector _elementPathCollector;

        /// <summary>
        /// The folder content item of the external repository root folder.
        /// </summary>
        private FolderContentItem _rootFolderItem;

        // Represents an object ID.
        private ObjID objectInfo;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="configuration">The configuration of the tests.</param>
        /// <param name="vaultId">The id of the vault to connect to.</param>
        public MFHelperMethods(
            TestConfiguration configuration,
            Guid vaultId,
            Users user)
        {
            // Create the client.
            UserConfiguration userConfiguration = configuration.Users.GetConfigurationForUser(user);
            string serverUrl = configuration.ServerConfiguration.BaseUrl;
            this.Client = this.SetUpClient(userConfiguration, serverUrl, vaultId);

            // Make sure we have an active connector session.
            this.ActivateConnectorSession(configuration.RepositoryName);

            //// Instantiate the object name collector.
            //this._elementPathCollector = new ElementPathCollector(configuration);

            // Get the root folder item.
            // Set up folder content item for the root of the selected external repository.
            this._rootFolderItem = this.GetExternalRepositoryRoot(configuration.RepositoryName);
        }

        /// <summary>
        /// Returns the folder contents from the folder at the end of the given path.
        /// If the path is empty the contents from the root folder is returned.
        /// </summary>
        /// <remarks>
        /// The contents are returned by traversing the path as a user would do it.
        /// Thus using paths with many elements will be expensive. 
        /// </remarks>
        /// <param name="pathToFolderFromRoot">The path to the folder from the root. Given as the names of the folders.</param>
        /// <returns>The contents of the last folder in the path.</returns>
        public FolderContentItems GetFolderContentsFromFolderInPath(params string[] pathToFolderFromRoot)
        {
            // Collect the contents of the external root folder.
            FolderContentItems folderContents = this.GetRootFolderContents();

            // Create the M-Files path needed for collecting the folder contents from sub folders.
            var pathToCurrentFolder = new List<FolderContentItem> { this._rootFolderItem };

            // Traverse the folder tree to collect the folder contents.
            foreach (string folderName in pathToFolderFromRoot)
            {
                // Get the sub folder.
                FolderContentItem subFolder = this.GetSubFolderFromFolderContents(folderName, folderContents);

                // Add the subfolder to the M-Files path.
                pathToCurrentFolder.Add(subFolder);

                // Collect the contents of the folder.
                folderContents = this.Client.ExternalViewOperations.GetFolderContents(pathToCurrentFolder.ToArray());
            }

            // Return the folder contents.
            return folderContents;
        }

		/// <summary>
		/// Returns the object with the given name if it exists in the folder contents.
		/// </summary>
		/// <param name="objectName">The name of the object to find.</param>
		/// <param name="parentFolderContents">The folder contents to search.</param>
		/// <returns>The object version of the requested object.</returns>
		/// <exception cref="KeyNotFoundException">Thrown if the object does not exists in the folder contents.</exception>
		public ObjectVersion GetObjectFromFolderContents(
			string objectName,
			FolderContentItems parentFolderContents )
		{
			// Collect the object version.
			ObjectVersion objectVersion;

			// Stores the extension of the file.
			string extn;

			// Stores the title of the object.
			string objectTitle;

			// Verfiy for the connector used.
			if( Setup.connectorName == "Documentum" )
			{
				// Documentum connector.
				
				// Stores the title of the object.
				objectTitle = objectName.Split( '.' )[ 0 ];

				// Stores the extension of the file.
				extn = objectName.Split('.')[1];

				// Collect the object version with partial name.
				objectVersion = (
					from item in parentFolderContents.Items
					where item.ObjectVersion != null
					where item.ObjectVersion.Title.Remove( item.ObjectVersion.Title.Length - 19 ) == objectTitle
					where item.ObjectVersion.Files[0].Extension.Equals( extn )
					select item.ObjectVersion ).FirstOrDefault();

				// if objectVersion is null for partial name then it verfies with full name.
				if( objectVersion == null )
				{
					// Collect the object version.
					objectVersion = (
						from item in parentFolderContents.Items
						where item.ObjectVersion != null
						where item.ObjectVersion.Title == objectTitle
						where item.ObjectVersion.Files[ 0 ].Extension.Equals( extn )
						select item.ObjectVersion ).FirstOrDefault();
				}

				// If object version is null, throw an error.
				if( objectVersion == null )
					throw new KeyNotFoundException( $"No object with the name {objectName} exists." );
			}

			else
			{
				// Other than Documentum connector.
				
				// Stores the extension of the file.
				extn = objectName.Split( '.' )[ 1 ];

				// Stores the title of the object.
				objectTitle = objectName.Split( '.' )[ 0 ];

				// Collect the object version.
				objectVersion = (
				   from item in parentFolderContents.Items
				   where item.ObjectVersion != null
				   where item.ObjectVersion.Title == objectTitle
				   where item.ObjectVersion.Files[ 0 ].Extension.Equals( extn )
				   select item.ObjectVersion ).FirstOrDefault();

				// If object version is null, throw an error.
				if( objectVersion == null )
					throw new KeyNotFoundException( $"No object with the name {objectName} exists." );
			}

			// Return object version.
			return objectVersion;
		}		

        ///// <summary>
        ///// Gets the object version of the given object.
        ///// </summary>
        ///// <param name="objectKey">The key for the object to get the version for.</param>
        ///// <returns>The object version for the object with the given object.</returns>
        //public ObjectVersion GetObjectVersion(ObjectType objectKey)
        //{
        //    // Get the relative path to the object.
        //    RelativeElementPath path = this._elementPathCollector.GetRelativeInternalPathToObject(objectKey);

        //    // Get the object.
        //    ObjectVersion objectVersion = this.GetObjectVersionUsingPath(path);

        //    // Return the found object version.
        //    return objectVersion;
        //}

        /// <summary>
        /// Gets the object version of the given object.
        /// </summary>
        /// <param name="objectName">The key for the object to get the version for.</param>
        /// <param name="objPath">The key for the object to get the version for.</param>
        /// <returns>The object version for the object with the given object.</returns>
        public ObjectVersion GetObjectVersion(string objectName, string objPath)
        {

            // Get the object.
            ObjectVersion objectVersion = this.GetObjectVersionUsingPath( objPath, objectName );

            // Return the found object version.
            return objectVersion;
        }

        /// <summary>
        /// Gets the root folder contents.
        /// </summary>
        /// <returns>The root folder's contents.</returns>
        public FolderContentItems GetRootFolderContents()
        {
            return this.Client.ViewOperations.GetFolderContents(this._rootFolderItem);
        }

        /// <summary>
        /// Activate the connector session.
        /// </summary>
        /// <param name="externalRepositoryName">The name of the external repository.</param>
        private void ActivateConnectorSession(string externalRepositoryName)
        {
            // Make a call to the server to make sure we have an active session.
            try
            {
                // Make any call to the client.
                this.Client.ExternalViewOperations.GetExternalRepositoryRoot(externalRepositoryName);
            }
            catch (HttpException exception)
            {
                // If there was no active session, catch the error but ignore it.
                // The session is now restored and ready to be used.
                Console.WriteLine("No valid session existed. One has been created now.");
            }
        }

        /// <summary>
        /// Collects the root folder item for the external repository.
        /// </summary>
        /// <param name="externalRepositoryName">The name of the external repository.</param>
        /// <returns>The root folder item for the external repository.</returns>
        private FolderContentItem GetExternalRepositoryRoot(string externalRepositoryName)
        {
            return this.Client.ExternalViewOperations.GetExternalRepositoryRoot(externalRepositoryName);
        }

        ///// <summary>
        ///// Returns the object version identified by the given path.
        ///// </summary>
        ///// <param name="pathToObject">The path to the object.</param>
        ///// <returns>The object version for the object.</returns>
        //private ObjectVersion GetObjectVersionUsingPath(RelativeElementPath pathToObject)
        //{
        //    // Collect the contents of the parent folder.
        //    FolderContentItems parentFolderContents =
        //        this.GetFolderContentsFromFolderInPath(pathToObject.Folders.ToArray());

        //    // Extract the object from the contents.
        //    ObjectVersion objectVersion =
        //        this.GetObjectFromFolderContents(pathToObject.ElementName, parentFolderContents);

        //    // Return the object.
        //    return objectVersion;
        //}

        /// <summary>
        /// Returns the object version identified by the given path.
        /// </summary>
        /// <param name="objectName">The path to the object.</param>
        /// <param name="objPath">The path to the object.</param>
        /// <returns>The object version for the object.</returns>
        private ObjectVersion GetObjectVersionUsingPath(  string objPath, string objectName = null)
        {
			// Verifies the connecotr used.
			if(Setup.connectorName.Equals("Sharepoint Online"))
			{
				// Sharepoint connector.

				// Appends the Documents root folder with the path.
				objPath = "Documents/" + objPath;
			}
			else if( Setup.connectorName.Equals( "OneDriveForBusiness" ) )
			{
				// One Drive connector.

				// Appends the root folder with the path.
				objPath = "Testi1 mfiles/" + objPath;
			}
			else if( Setup.connectorName.Equals( "SharepointServer2016" ) )
			{
				// Appends the root folder with the path.
				objPath = "Connector Test/Documents/" + objPath;
			}

			// Stores the path in array with subfolders.
			string[] objectPath = objPath.Split( '/' );

			// Collect the contents of the parent folder.
			FolderContentItems parentFolderContents =
                this.GetFolderContentsFromFolderInPath(objectPath);

            // Extract the object from the contents.
            ObjectVersion objectVersion =
                this.GetObjectFromFolderContents(objectName, parentFolderContents);

            // Return the object.
            return objectVersion;
        }

        /// <summary>
        /// Collects the subfolder with the given name from the parent folder contents.
        /// </summary>
        /// <param name="subFolderName">The name of the subfolder to return.</param>
        /// <param name="parentFolderContents">The contents of the parent folder.</param>
        /// <returns>The requested folder.</returns>
        private FolderContentItem GetSubFolderFromFolderContents(
            string subFolderName,
            FolderContentItems parentFolderContents)
        {
            // Get the first external view with the correct name.
            FolderContentItem subfolder = parentFolderContents.Items
                .Where(i => i.ExternalView != null)
                .FirstOrDefault(i => i.ExternalView.DisplayName == subFolderName);

            // Throw an error if we didn't find the sub folder.
            if (subfolder == null)
                throw new KeyNotFoundException($"No subfolder with the name {subFolderName} was found.");

            // Return the sub folder.
            return subfolder;
        }

        /// <summary>
        /// Set up a client and a vault access.
        /// </summary>
        /// <param name="user">The user that should loc in to the server.</param>
        /// <param name="baseUrl">The base url of the server.</param>
        /// <param name="vaultId">The id of the vault the client should be connected to.</param>
        /// <returns>The client.</returns>
        private MFWSClient SetUpClient(
            UserConfiguration user,
            string baseUrl,
            Guid vaultId)
        {
            // Connect to the vault.
            var client = new MFWSClient(baseUrl);
            client.AddDefaultHeader("X-Extensions", "MFWA,IML");

            // Get authentication to the selected vault and return the client.
            client.AuthenticateUsingCredentials(
                vaultId,
                user.MFilesCredentials.Username,
                user.MFilesCredentials.Password);
            return client;
        }

        /// <summary>
        /// Get the object details.
        /// </summary>
        /// <param name="externalObjVer">Information about the object.</param>
        /// <returns>The information about the object.</returns>
        public ObjID GetObjID(ObjVer externalObjVer)
        {
            // Initaites the ObjID class to store the information about the object.
            objectInfo = new ObjID();

            // Stores the information about the object.
            objectInfo.ID = externalObjVer.ID;
            objectInfo.Type = externalObjVer.Type;

            // Return object details.
            return objectInfo;
        }

		/// <summary>
		///  Returns the object version of an unmananged object.
		/// </summary>
		/// <param name="folderItems">All the objects in the mentioned path.</param>
		/// <returns>The object version for the object.</returns>
		public ObjectVersion SelectUnmanagedObject( FolderContentItems folderItems )
		{
			try
			{
				ObjectVersion objectVersion = null;

				// Verify the folder is not empty.
				if( folderItems.Items.Count == 0 )
				{
					Assert.Fail( "The sub folder is empty." );
				}

				// Verify and pick an unmanaged object.
				foreach( FolderContentItem item in folderItems.Items )
				{
					if( item.ObjectVersion.Class == -107 &&
						item.ObjectVersion.ObjectCheckedOut == false )
					{
						// Get the object.
						objectVersion = item.ObjectVersion;
						break;
					}
				}

				// Return the objectversion.
				return objectVersion;
			}
			catch( Exception ex )
			{
				throw ex;
			}
		}
	}
}
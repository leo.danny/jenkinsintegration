﻿using System;
using System.IO;
using System.Xml;
using System.Data.OleDb;
using MFNUnit.Utils.TestData;
using Mfiles.Connectors.TestFramework.Tests;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace Mfiles.Connectors.TestFramework.Configuration
{
	/// <summary>
	/// This class consists of methods to update the performance results in Excel and XML file.
	/// </summary>
	public class GeneratingPerformanceResults
    {
        /// <summary>
        /// This method will update the execution time for each test.
        /// </summary>
        /// <param name="valueToBeUpdated">Value which has to be updated in the respective column.</param>
        public void UpdateResult(string valueToBeUpdated)
        {
            // Stores the excel path.
            string excelPath =
				Directory.GetParent(
			NUnit.Framework.TestContext.CurrentContext.TestDirectory ).Parent.FullName + "\\PerformanceReport\\PerformanceTest.xlsx";

            // Sheet Name to be updated.
            string sheetName = "PerformanceTestingResult";

            // Current test name.
            string testName = NUnit.Framework.TestContext.CurrentContext.Test.Name.Split('_')[1];

            // Column which has to be updated.
            string columnToBeUpdated = Setup.connectorName;

            ExcelDataWriter excelDataWriter = new ExcelDataWriter();

            // Get the instance of the oledb connection.
            OleDbConnection excelConnection = excelDataWriter.CreateExcelConnection(excelPath);

            // Open the excel connection.
            excelConnection.Open();

            // Update the value in the document.
            excelDataWriter.ExecuteQueryToUpdate(excelConnection, sheetName, testName, columnToBeUpdated, valueToBeUpdated);

            // Close the excel connection.
            excelConnection.Close();
        }

		/// <summary>
		/// This method will update the execution time for each performance test.
		/// </summary>
		/// <param name="connectorName">Name of the connector.</param>
		/// <param name="performanceTime">Performance time taken by the API method.</param>
		public static void updateXMLDocument(
			string connectorName,
			string performanceTime)
		{
			try
			{
				// Removes the empty spaces from the connector name.
				connectorName = connectorName.Replace( " ", "" );

				// Stores the current test name.
				string testName = NUnit.Framework.TestContext.CurrentContext.Test.MethodName.Replace( 
					NUnit.Framework.TestContext.CurrentContext.Test.MethodName.Split( '_' )[ 0 ] + "_", "" );

				// Stores the performance time based on the Connector for different operations.
				CreateTheXMLAndUpdate( connectorName, testName, performanceTime, "ConnectorPerformance" );

				// Stores the performance time based on the Operations for each connector.
				CreateTheXMLAndUpdate( testName, connectorName, performanceTime, "OperationPerformance" );
			}
			catch( Exception exception )
			{
				Console.WriteLine( "Error while updating the xml file " + exception.Message );
			}

		}

		/// <summary>
		/// This method will update the execution time for each performance test in SQL Server.
		/// </summary>
		/// <param name="connectorName">Name of the connector.</param>
		/// <param name="performanceTime">Performance time taken by the API method.</param>
		public static void updateSQLServer(
			string connectorName,
			string performanceTime )
		{
			try
			{
				// Instantiate the TestConfiguration class to load the value of SQL server credentials.
				TestConfiguration testConfiguration = ConfigurationLoader.LoadAndPopulateTestConfiguration();

				
				// Stores the name of DB, TableName and Column Name in which Connectorname is stores.
				string databaseName = "ConnectorPerformanceResults";
				string tableName = "TestResults";
				string connectorColumnName = "ConnectorName";

				// Stores the credentials of SQL Server.
				string sqlServerName = testConfiguration.ReportsConfiguration.SQLServer;
				string sqlUserName = testConfiguration.ReportsConfiguration.SQLUsername;
				string sqlPassword = testConfiguration.ReportsConfiguration.SQLPassword;

				// Removes the empty spaces from the connector name.
				connectorName = connectorName.Replace( " ", "" );

				// Stores the current test name.
				string testName = NUnit.Framework.TestContext.CurrentContext.Test.MethodName.Split( '_' )[ 1 ];

				// Update the performance results in SQL Server.
				SQLDataWriter.UpdatePerformanceResultToSQL( databaseName, tableName, connectorColumnName, connectorName, 
															testName, Int32.Parse( performanceTime ), "MFilesVersion", 
															Setup.mFilesVersion, sqlServerName, sqlUserName, sqlPassword );
			}
			catch( Exception exception )
			{
				Console.WriteLine( "Error while updating the SQL Server " + exception.Message );
			}

		}

		/// <summary>
		/// This method will create the XML file if it does not exists or It will update to the existing file.
		/// </summary>
		/// <param name="parentTagName">Parent tag name.</param>
		/// <param name="childTagName">Child tag name.</param>
		/// <param name="performanceTime">Performance time which to be updated.</param>
		/// <param name="rootTag">Root tag name.</param>
		public static void CreateTheXMLAndUpdate(
			string parentTagName,
			string childTagName,
			string performanceTime,
			string rootTagName)
		{
			try
			{
				// Version tag name.
				string versionTag = "M-FilesVersion";

				string projectDirectory = Directory.GetParent( NUnit.Framework.TestContext.CurrentContext.TestDirectory ).Parent.FullName;
				
				// Location where the Performance results will be generated.
				var performanceResultDirectory = projectDirectory + "\\Reports\\PerformanceReport";

				// Deletes the report folder if it exists.
				if( !Directory.Exists( performanceResultDirectory ) )
				{
					// Creates report folder.
					System.IO.Directory.CreateDirectory( performanceResultDirectory );
				}

				// Stores the path of xml sheet.
				var xmlPath = projectDirectory + "\\Reports\\PerformanceReport\\" + Setup.mFilesVersion + "_" + rootTagName + ".xml";

				// Check the existence of the xml file.
				if( !File.Exists( xmlPath ) )
					// Creates the xml file with the root tag.
					File.WriteAllText( xmlPath, "<" + rootTagName + ">" + Environment.NewLine + "</" + rootTagName + ">" );

				// Load the xml file.
				XmlDocument xmlDoc = LoadXMLDocument( xmlPath );

				// Update the performance results in XML file.
				XMLDataWriter.UpdatingPerformanceResultInXML( rootTagName, parentTagName, childTagName, xmlDoc, performanceTime, Setup.mFilesVersion, versionTag );				

				// Save the changes in xml file.
				xmlDoc.Save( @xmlPath );
			}
			catch(Exception ex)
			{
				Console.WriteLine( "Error while updating the performance time in xml file: " + ex.Message );
			}
		}

		/// <summary>
		/// This method will load the xml file.
		/// </summary>
		/// <param name="xmlPath">Path of the xml file.</param>
		/// <returns>The loaded instance of the xml file.</returns>
		public static XmlDocument LoadXMLDocument( string xmlPath)
		{
			try
			{
				// New instance of XmlDocument to load, update and save the xml file.
				XmlDocument xmlDoc = new XmlDocument();

				// Loads the XML content.
				xmlDoc.Load( @xmlPath );

				// return the instance of loaded XML file.
				return xmlDoc;
			}
			catch( Exception exception )
			{
				Console.WriteLine( "Error while loading the xml file " + exception.Message );
				throw exception;
			}
		}

		/// <summary>
		/// This method will extract the information from xml file and create a chart view.
		/// </summary>
		public static void CreatePerformanceTestChartView( )
		{
			try
			{
				// URL of the parent tag.
				string parentTagURL = "ConnectorPerformance";

				// Stores the xml sheet path name.
				var xmlPath = Directory.GetParent(
						NUnit.Framework.TestContext.CurrentContext.TestDirectory ).Parent.FullName + "\\Reports\\PerformanceReport\\" + Setup.mFilesVersion + "_" + parentTagURL + ".xml";

				// Load the xml file.
				XmlDocument xmlDoc = LoadXMLDocument( xmlPath );

				// Stores the instance of parent tag.
				XmlElement parentTag = ( XmlElement ) xmlDoc.SelectSingleNode( parentTagURL );

				// Stores the number of child tag available;
				var childTag = parentTag.ChildNodes.Count;

				// Verify the xml file is not empty.
				if( childTag == 0 )
				{
					Console.WriteLine( "There is no information to be logged." );
					return;
				}

				// Array to store the connector name tags.
				string[] connectorTag = new string[ childTag ];

				// Stores available connector name in an array.
				for( int childTagCount = 0; childTagCount < childTag; childTagCount++ )
				{
					connectorTag[ childTagCount ] = parentTag.ChildNodes[ childTagCount ].Name.ToString();
				}

				// Create an instance of the chart.
				var performanceChart = new Chart();

				// Assign the size of the chart.
				performanceChart.Size = new Size( 1000, 500 );

				// Set the properties of the ChartArea.
				var chartArea = new ChartArea();
				chartArea.AxisY.LineColor = Color.White;
				chartArea.AxisX.LineColor = Color.LightGray;
				chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
				chartArea.AxisX.MajorGrid.Enabled = false;
				chartArea.AxisY.MajorTickMark.Enabled = false;
				chartArea.AxisX.MajorTickMark.Enabled = false;
				chartArea.AxisY.Title = "In MilliSeconds";
				chartArea.AxisY.TitleFont = new Font( "Calibri (Body)", 10 );
				chartArea.AxisX.LabelStyle.Font = new Font( "Calibri (Body)", 8 );
				chartArea.AxisY.LabelStyle.Font = new Font( "Calibri (Body)", 8 );
				
				performanceChart.ChartAreas.Add( chartArea );
				
				// Fetch the value of each method attribute under each connector and assign the x and y axis of Chart.
				for( int childTagCount = 0; childTagCount < childTag; childTagCount++ )
				{
					// Number of nodes under each connector tag.
					var nodeCountOfEachConnector = parentTag.ChildNodes[ childTagCount ].ChildNodes[0].ChildNodes.Count;
					
					// Method name available in each connector tag.
					string[] methodTag = new string[ nodeCountOfEachConnector ];

					// Value of each method tag.
					int[] performanceValue = new int[ nodeCountOfEachConnector ];

					// Stores the value of method attributes.
					for( int methodTagCount = 0; methodTagCount < nodeCountOfEachConnector; methodTagCount++ )
					{
						// Stores the method name.
						methodTag[ methodTagCount ] =
							parentTag.ChildNodes[ childTagCount ].ChildNodes[ 0 ].ChildNodes[ methodTagCount ].Name;

						// Stores the value of method tag.
						performanceValue[ methodTagCount ] =
							Int32.Parse( parentTag.ChildNodes[ childTagCount ].ChildNodes[ 0 ].ChildNodes[ methodTagCount ].InnerText );
					}

					// Create a new instance of Series class to store the data in chart.
					var series = new Series();

					// Set the proeprties of the Chart.
					series.Name = connectorTag[ childTagCount ];
					series.ChartType = SeriesChartType.Column;
					series.XValueType = ChartValueType.String;
				
					// Add the data in series instance to the chart.
					performanceChart.Series.Add( series );
					performanceChart.ChartAreas[ 0 ].AxisX.LabelStyle.Angle = -45;
					performanceChart.ChartAreas[ 0 ].AxisX.Interval = 1;

					// Bind the datapoints to the chart.
					performanceChart.Series[ connectorTag[ childTagCount ] ].Points.DataBindXY( methodTag, performanceValue );
				}
				
				// Create new legend entry to differentiate the connectors used.
				performanceChart.Legends.Add( new Legend( "Legend" ) );

				// Set the properties of the legend entry.
				performanceChart.Legends[ "Legend" ].LegendStyle = LegendStyle.Row;
				performanceChart.Legends[ "Legend" ].Docking = Docking.Bottom;
				performanceChart.Legends[ "Legend" ].Alignment = StringAlignment.Center;

				// Save the chart as an image.
				performanceChart.SaveImage( xmlPath.Replace( ".xml", ".Png" ), ChartImageFormat.Png );

			}
			catch( Exception exception )
			{
				Console.WriteLine( "Error while logging the performance result in graphical view " + exception.Message );
				throw exception;
			}
		}
	}
}

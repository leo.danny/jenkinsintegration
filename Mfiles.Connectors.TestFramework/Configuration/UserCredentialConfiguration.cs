﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration {
	/// <summary>
	/// Credential configuration for users.
	/// </summary>
	[DataContract]
	public class UserCredentialConfiguration
	{
		/// <summary>
		/// The user's username.
		/// </summary>
		[DataMember( Name = "Username", IsRequired = true )]
		public string Username { get; set; }

		/// <summary>
		/// The user's password.
		/// </summary>
		[DataMember( Name = "Password", IsRequired = true )]
		public string Password { get; set; }
	}
}
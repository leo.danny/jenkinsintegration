﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration
{
	/// <summary>
	/// Configuration for the folder collection.
	/// </summary>
	[DataContract]
	public class FolderCollectionConfiguration
	{
		/// <summary>
		/// The configuration for the non root folder.
		/// </summary>
		[DataMember( Name = "Non Root Folder", IsRequired = true )]
		public ExternalFolderConfiguration NonRootFolder { get; set; }
	}
}
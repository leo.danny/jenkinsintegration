﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration {
	/// <summary>
	/// Configuration of the internal and external credentials for a specific user.
	/// </summary>
	[DataContract]
	public class UserConfiguration
	{
		/// <summary>
		/// The credentials for the M-Files user.
		/// </summary>
		[DataMember( Name = "M-Files Credentials", IsRequired = true )]
		public UserCredentialConfiguration MFilesCredentials { get; set; }

		/// <summary>
		/// The credentials for the external user the M-Files user will authenticate as.
		/// </summary>
		[DataMember( Name = "External Credentials", IsRequired = true )]
		public UserCredentialConfiguration ExternalCredentials { get; set; }
	}
}
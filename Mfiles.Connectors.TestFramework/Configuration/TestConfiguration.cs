﻿using System.Runtime.Serialization;
       
namespace Mfiles.Connectors.TestFramework.Configuration
{
	/// <summary>
	/// Configuration for the test framework.
	/// </summary>
	[DataContract]
	public class TestConfiguration
	{
		/// <summary>
		/// The name of the external repository.
		/// </summary>
		/// <remarks>Has to be set by extracting the name from the connector configuration.</remarks>
		public string RepositoryName { get; set; }
		
		/// <summary>
		/// The configuration for the users used in the tests.
		/// </summary>
		[DataMember(Name = "Test Users", IsRequired = true)]
		public TestUsersConfiguration Users { get; set; }

		/// <summary>
		/// The configuration for the M-Files server to connect to.
		/// </summary>
		[DataMember(Name = "Server Configuration", IsRequired = true)]
		public ServerConfiguration ServerConfiguration { get; set; }
		
		/// <summary>
		/// The configuration for installing the connector.
		/// </summary>
		[DataMember(Name = "Connector Installation", IsRequired = true)]
		public ConnectorInstallationConfiguration ConnectorInstallationConfiguration { get; set; }

		/// <summary>
		/// The configuration for SQL Server.
		/// </summary>
		[DataMember( Name = "Reports", IsRequired = true )]
		public ReportsConfiguration ReportsConfiguration { get; set; }

	}
}
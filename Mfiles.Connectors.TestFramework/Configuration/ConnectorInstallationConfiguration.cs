﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration {
	/// <summary>
	/// Configuration for the installation of the connector. 
	/// </summary>
	[DataContract]
	public class ConnectorInstallationConfiguration
	{
		/// <summary>
		/// Name of the vault to connect to on the M-Files server.
		/// </summary>
		[DataMember( Name = "Vault Name", IsRequired = true )]
		public string NameOfVaultToConnectTo { get; set; }
		
		/// <summary>
		/// Application Guid from the connector's appdef.xml file.
		/// </summary>
		[DataMember( Name = "Application Guid", IsRequired = true )]
		public string ApplicationGuid { get; set; }

		/// <summary>
		/// Connector service Guid from "RepositoryConnectorService.Guid".
		/// </summary>
		[DataMember( Name = "Connector Service Guid", IsRequired = true )]
		public string ConnectorServiceGuid { get; set; }
		
		/// <summary>
		/// The path to the connector package.
		/// </summary>
		[DataMember( Name = "Connector Path", IsRequired = true )]
		public string ConnectorPath { get; set; }

        /// <summary>
		/// The Value in 'Connector Installation Required' decides if connector needs to be installed before running tests
		/// </summary>
		[DataMember(Name = "Connector Installation Required", IsRequired = true)]
        public string ConnectorInstallationRequired { get; set; }

        /// <summary>
        /// The path to the connector's configuration file.
        /// </summary>
        [DataMember( Name = "Connector Configuration Path", IsRequired = true )]
		public string ConnectorConfigPath { get; set; }

        /// <summary>
		/// The Value in 'Connector Configuration Required' decides if configuration to be loaded before running tests
		/// </summary>
		[DataMember(Name = "Connector Configuration Required", IsRequired = true)]
        public string ConnectorConfigurationRequired { get; set; }

        /// <summary>
		/// The Name of the connector
		/// </summary>
		[DataMember(Name = "Connector Name", IsRequired = true)]
        public string ConnectorName { get; set; }

        /// <summary>
		/// The Value in 'Connector Authentication Required' decides if Common and Index user authentication is required before running tests
		/// </summary>
		[DataMember(Name = "Connector Authentication Required", IsRequired = true)]
        public string ConnectorAuthenticationRequired { get; set; }

        /// <summary>
        /// The Value in 'Vault Restore Required' decides if restoring test vault back up is needed before running tests
        /// </summary>
        [DataMember(Name = "Vault Restore Required", IsRequired = true)]
        public string VaultRestoreRequired { get; set; }

        /// <summary>
        /// Path to the folder where the M-Files vaults are stored.
        /// </summary>
        [DataMember( Name = "Vault Folder Path", IsRequired = true )]
		public string MFilesVaultFolder { get; set; }

		/// <summary>
		/// Path to the backup file of the vault.
		/// </summary>
		[DataMember( Name = "Vault Backup File Path", IsRequired = true )]
		public string VaultBackupFilePath { get; set; }

        /// <summary>
        /// Path to the license file of the connector.
        /// </summary>
        [DataMember(Name = "Connector License Path", IsRequired = true)]
        public string ConnectorLicensePath { get; set; }

		/// <summary>
		/// The Value in 'Create New Vault' decides if creating new test vault is needed before running tests.
		/// </summary>
		[DataMember( Name = "Create New Vault", IsRequired = true )]
		public string CreateNewVault { get; set; }
	}
}
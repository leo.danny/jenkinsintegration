﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration {
	/// <summary>
	/// Configuration of the M-Files server.
	/// </summary>
	[DataContract]
	public class ServerConfiguration
	{
		/// <summary>
		/// The network address of the M-Files server to connect to.
		/// </summary>
		[DataMember( Name = "Server Network Address", IsRequired = true)]
		public string ServerNetworkAddress { get; set; }

		/// <summary>
		/// The endpoint (port number) where the server is active.
		/// Only needs to be set if the endpoint is different from the default value (2266).
		/// </summary>
		[DataMember( Name = "Server Endpoint", IsRequired = false)]
		public string ServerEndpoint { get; set; } = "2266";
		
		/// <summary>
		/// Returns the base url of the server.
		/// </summary>
		public string BaseUrl => $"http://{this.ServerNetworkAddress}";
	}
}
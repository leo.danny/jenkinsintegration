﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration {
	/// <summary>
	/// Configuration of the SQL server.
	/// </summary>
	[DataContract]
	public class ReportsConfiguration
	{
		/// <summary>
		/// The name of the SQL Server.
		/// </summary>
		[DataMember( Name = "SQL Server", IsRequired = false)]
		public string SQLServer { get; set; }

		/// <summary>
		/// User name to access the SQL Server,
		/// </summary>
		[DataMember( Name = "SQL Username", IsRequired = false )]
		public string SQLUsername { get; set; }

		/// <summary>
		/// Password of the SQL Server user.
		/// </summary>
		[DataMember( Name = "SQL Password", IsRequired = false )]
		public string SQLPassword { get; set; }
	}
}
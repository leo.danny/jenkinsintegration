﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration
{
	/// <summary>
	/// Configuration for the collection of test objects.
	/// </summary>
	[DataContract]
	public class ObjectCollectionConfiguration
	{
		/// <summary>
		/// The configuration for the checkout object.
		/// </summary>
		[DataMember( Name = "Checkout Object", IsRequired = true )]
		public ExternalObjectConfiguration CheckOutObject { get; set; }

		/// <summary>
		/// The configuration for the deletion status object.
		/// </summary>
		[DataMember( Name = "Deletion Status Object", IsRequired = true )]
		public ExternalObjectConfiguration DeletionStatusObject { get; set; }

		/// <summary>
		/// The configuration for the properties object.
		/// </summary>
		[DataMember( Name = "Properties Object", IsRequired = true )]
		public ExternalObjectConfiguration PropertiesObject { get; set; }

		/// <summary>
		/// The configuration for the download object.
		/// </summary>
		[DataMember( Name = "Download Object", IsRequired = true )]
		public ExternalObjectConfiguration DownloadObject { get; set; }

		/// <summary>
		/// The configuration for the rename object.
		/// </summary>
		[DataMember( Name = "Rename Object", IsRequired = true )]
		public ExternalObjectConfiguration RenameObject { get; set; }

		/// <summary>
		/// The configuration for the deletion object.
		/// </summary>
		[DataMember( Name = "Delete Object", IsRequired = true )]
		public ExternalObjectConfiguration DeleteObject { get; set; }
	}
}
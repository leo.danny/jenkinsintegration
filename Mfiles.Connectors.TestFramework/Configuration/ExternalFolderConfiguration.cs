﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration {
	/// <summary>
	/// Configuration for interacting with the external objects.
	/// </summary>
	/// <remarks>
	/// The only difference between this configuration and 
	/// <see cref="ExternalObjectConfiguration"/> is the value of the IsFolder flag.
	/// </remarks>
	[DataContract]
	public class ExternalFolderConfiguration : ExternalObjectConfiguration
	{
		/// <summary>
		/// Flag indicating whether this is a path to a folder or an object.
		/// </summary>
		protected override bool IsFolder { get; } = true;
	}
}
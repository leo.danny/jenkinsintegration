﻿using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration
{
	/// <summary>
	/// Configuration for interacting with the external repository.
	/// The properties in this class are dependent on the specific connector being tested.
	/// </summary>
	/// TODO: Implement this based on the connector.
	[DataContract]
	public class ExternalRepositoryConfiguration
	{

	}
}
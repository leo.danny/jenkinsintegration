﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Mfiles.Connectors.TestFramework.Configuration {

	/// <summary>
	/// The users supported by the test framework.
	/// </summary>
	public enum Users
	{
		Admin,
		User1,
		User2
	}
	
	/// <summary>
	/// Configuration for the users used in the tests. 
	/// </summary>
	[DataContract]
	public class TestUsersConfiguration
	{
		/// <summary>
		/// The configuration for the admin user.
		/// </summary>
		[DataMember(Name = "Admin User", IsRequired = true)]
		public UserConfiguration AdminUser { get; set; }
		
		/// <summary>
		/// The configuration for the normal user used for all the tests.
		/// </summary>
		[DataMember(Name = "User 1", IsRequired = true)]
		public UserConfiguration User1 { get; set; }
		
		/// <summary>
		/// The configuration for the user used in tests where multiple users are needed.
		/// </summary>
		[DataMember(Name = "User 2", IsRequired = true)]
		public UserConfiguration User2 { get; set; }
		
		/// <summary>
		/// Returns a list of all the configured users.
		/// </summary>
		public List<UserConfiguration> AllStandardUserConfigurations => new List<UserConfiguration>
		{
			this.User1,
			this.User2
		};

		/// <summary>
		/// Returns the configuration for the given user.
		/// </summary>
		/// <param name="user">The user to return the configuration for.</param>
		/// <returns>The configuration for the user.</returns>
		public UserConfiguration GetConfigurationForUser( Users user )
		{
			switch ( user )
			{
				case Users.Admin:
					return this.AdminUser;
				case Users.User1:
					return this.User1;
				case Users.User2:
					return this.User2;
				default:
					throw new ArgumentOutOfRangeException( nameof( user ), user, null );
			}
		}
	}
}
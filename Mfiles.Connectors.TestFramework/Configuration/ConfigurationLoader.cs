﻿using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Mfiles.Connectors.TestFramework.Configuration
{
	/// <summary>
	/// Class responsible for loading the configuration for the test framework.
	/// </summary>
	public static class ConfigurationLoader
	{
		/// <summary>
		/// The name of the configuration file.
		/// </summary>
		private static string TestConfigurationFileName = "testconf.json";

		/// <summary>
		/// Loads the test configuration and populates fields not given directly in the configuration file.
		/// </summary>
		/// <returns>The test configuration with all fields properly populated.</returns>
		public static TestConfiguration LoadAndPopulateTestConfiguration()
		{
			// Load the test configuration from the configuration file.
			TestConfiguration configuration = ConfigurationLoader.LoadTestConfiguration();
						
			// Get the repository display name of the connector from the connector configuration.
			string repositoryDisplayName = ConfigurationLoader.ExtractRepositoryDisplayName( configuration );
			configuration.RepositoryName = repositoryDisplayName;
			
			// Return the configuration.
			return configuration;
		}

		/// <summary>
		/// Loads the test configuration from the working directory.
		/// </summary>
		/// <returns>The configuration class.</returns>
		private static TestConfiguration LoadTestConfiguration()
		{
			// Get the parameter value.
			string testconfPath = TestContext.Parameters.Get( "testconfPath" );

			// Get the file path for the configuration file from the working directory.
			string directoryPath = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location );
			string filePath;

			// Stores the configuration file path.
			if( testconfPath != null)
				filePath = testconfPath;
			else 
				filePath = $@"{directoryPath}\{ConfigurationLoader.TestConfigurationFileName}";			

			// Parse the JSON file into a class.
			using ( StreamReader file = File.OpenText( filePath ) )
			{
				// Load the file.
				string fileContent = file.ReadToEnd();
				
				// Replace all single back slashes with double back slashes to make the text ready for parsing.
				fileContent = fileContent.Replace( @"\", @"\\" );
				
				// Parse the configuration and return it.
				return JsonConvert.DeserializeObject<TestConfiguration>( fileContent );
			}
		}

		/// <summary>
		/// Loads the connector configuration to extract the repository display name.
		/// </summary>
		/// <param name="testConfiguration">
		/// Configuration from where to get the location of the connector configuration.
		/// </param>
		/// <returns>The display name for the external repository.</returns>
		private static string ExtractRepositoryDisplayName( TestConfiguration testConfiguration )
		{
			// Get the path to the connector configuration.
			string pathToConnectorConfiguration = testConfiguration.ConnectorInstallationConfiguration.ConnectorConfigPath;
			
			// Load the configuration.
			JObject configuration;
			using ( StreamReader file = File.OpenText( pathToConnectorConfiguration ) )
			{
				string fileContent = file.ReadToEnd();
				configuration = JsonConvert.DeserializeObject<JObject>( fileContent );
			}
			
			// Extract and return the display name of the repository.
			return (string) configuration[ "declaration" ][ "displayName" ];
		}
	}
}
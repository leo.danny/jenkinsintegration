﻿using System.Runtime.Serialization;
using Mfiles.Connectors.TestFramework.RepositoryStructure;
using Mfiles.Connectors.TestFramework.TestRunSetup;

namespace Mfiles.Connectors.TestFramework.Configuration
{
	/// <summary>
	/// Configuration for interacting with the external objects.
	/// </summary>
	[DataContract]
	public class ExternalObjectConfiguration
	{
		/// <summary>
		/// The relative path from the external root folder to the element as used in the M-Files GUI.
		/// </summary>
		/// <remarks>
		/// Do not start with a "/". 
		/// Each element in the path is a folder except the last one which has to be the name of the object.
		/// </remarks>
		/// <example> 
		/// The relative view path "Documents/MyFile" corresponds to the following full path in the M-Files GUI:
		/// "[VaultRoot]/[ExternalRepositoryRoot]/Documents/MyFile".
		/// For an object stored in an external repository called SharePoint this would correspond to
		/// "MyVault/SharePoint/Documents/MyFile".
		/// </example>
		[DataMember( Name = "Relative View Path to Element", IsRequired = true )]
		private string RelativeInternalPathToObjectAsString { get; set; }

		/// <summary>
		/// The relative path from the external root folder to the element as used in the M-Files GUI.
		/// </summary>
		public RelativeInternalElementPath RelativeInternalPathToObject { get; private set; }

		/// <summary>
		/// The path to the element as used in the external repository. 
		/// If not set this value will be the same as the <see cref="RelativeInternalPathToObjectAsString"/>.
		/// </summary>
		/// <remarks>
		/// This is only used by the implemented <see cref="ExternalRepositoryInteractor"/> 
		/// so the format of this path is up to the implementor.
		/// </remarks>
		[DataMember( Name = "Relative External Path to Element", IsRequired = false )]
		private string RelativeExternalPathToObjectAsString { get; set; }

		/// <summary>
		/// The path to the element as used in the external repository.
		/// </summary>
		public RelativeExternalElementPath RelativeExternalPathToObject { get; private set; }

		/// <summary>
		/// Flag indicating whether this is a path to a folder or an object.
		/// </summary>
		protected virtual bool IsFolder { get; } = false;

		/// <summary>
		/// The method that is run when the contract has been deserialized.
		/// </summary>
		/// <param name="c">The steaming context.</param>
		[OnDeserialized]
		private void OnDeserialized( StreamingContext c )
		{
			// Set the relative external path to be the same as the relative view path
			// if the relative external path is not set in the configuration.
			this.RelativeExternalPathToObjectAsString =
				this.RelativeExternalPathToObjectAsString ?? this.RelativeInternalPathToObjectAsString;

			// Create the path objects.
			this.RelativeInternalPathToObject =
				new RelativeInternalElementPath( this.RelativeInternalPathToObjectAsString, this.IsFolder );
			this.RelativeExternalPathToObject =
				new RelativeExternalElementPath( this.RelativeExternalPathToObjectAsString, this.IsFolder );
		}
	}
}